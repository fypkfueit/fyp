<?php

use Picqer\Barcode\BarcodeGeneratorHTML;
use Picqer\Barcode\BarcodeGeneratorPNG;
use \Pusher\Pusher;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


function currentDate()
{
    return date('Y-m-d H:i:s');
}


function print_rm($data)
{
    echo '<pre>';
    print_r($data);
    exit;
}

function dump($data)
{
    echo '<pre>';
    print_r($data);
    exit;
}


function sms($_param){
    
        $_url = 'https://apps.gateway.sa/vendorsms/pushsms.aspx?user=exclusavecard&password=ChangeLater12!&sid=EXCLUSAVE&fl=0';
        $postData = '';
        //create name value pairs seperated by &
        foreach($_param as $k => $v) 
        { 
          $postData .= $k . '='.$v.'&'; 
        }
        rtrim($postData, '&');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_POST, count($_param));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    

        $output=curl_exec($ch);

        curl_close($ch);

        return $output;
    }

function getMonths($language)
{

    if ($language == 'EN') {

        return array('Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    } elseif ($language == 'AR') {
        return array('?????', '??????', '????', '?????', '????', '?????', '????', '?????', '??????', '??????', '??????', '??????');
    }
}

function showDateFromString($val,$d_formate = 'Y-m-d'){
    if($val == ''){
        return '';
    }
    if(strlen($val) == 13){
        $val = $val / 1000;

    }


    return date($d_formate,$val);
}

function get_images($id, $type = 'product', $multiple = true)
{
    $CI = &get_Instance();
    $CI->load->model('Site_images_model');
    $images = $CI->Site_images_model->getMultipleRows(array('FileID' => $id, 'ImageType' => strtolower($type)));
    if ($images) {
        if ($multiple) {
            return $images;
        } else {
            return $images[0]->ImageName;
        }
    } else {
        return false;
    }
}

function getCategoryTree($level = 0, $prefix = '', $language)
{
    $CI = &get_Instance();
    $rows = $CI->db
        ->select('categories.IsActive,categories.CategoryID,categories.SortOrder,categories_text.Title')
        ->join('categories_text', 'categories.CategoryID = categories_text.CategoryID')
        ->join('system_languages', 'system_languages.SystemLanguageID = categories_text.SystemLanguageID')
        ->where('categories.ParentID', $level)
        ->where('system_languages.ShortCode', $language)
        ->order_by('categories.SortOrder', 'asc')
        ->get('categories')
        ->result();

    $category = '';
    if (count($rows) > 0) {
        foreach ($rows as $row) {
            if (checkUserRightAccess(43, $CI->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(43, $CI->session->userdata['admin']['UserID'], 'CanDelete')) {
                $action = '';
                if (checkUserRightAccess(43, $CI->session->userdata['admin']['UserID'], 'CanEdit')) {
                    $action .= '<a href="' . base_url('cms/category/edit/' . $row->CategoryID) . '"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons">edit</i>
                                                            <div class="ripple-container"></div>
                                                        </a>';
                }

                if (checkUserRightAccess(43, $CI->session->userdata['admin']['UserID'], 'CanDelete')) {
                    $action .= ' <a href="javascript:void(0);"
                                                           onclick="deleteRecord("' . $row->CategoryID . '","cms/category/action","")"
                                                           class="btn btn-simple btn-danger btn-icon remove"><i
                                                                    class="material-icons">close</i>
                                                            <div class="ripple-container"></div>
                                                        </a>';
                }
            }

            $category .= '<tr><td>' . $prefix . ' ' . $row->Title . '</td><td>' . $row->SortOrder . '</td><td>' . ($row->IsActive ? lang('yes') : lang('no')) . '</td><td>' . $action . '</td></tr>';
            // Append subcategories
            $category .= getCategoryTree($row->CategoryID, $prefix . '-------', $language);
        }
    }
    return $category;
}


function checkRightAccess($module_id, $role_id, $can)
{
    $CI = &get_Instance();
    $CI->load->model('Module_rights_model');

    $fetch_by = array();
    $fetch_by['ModuleID'] = $module_id;
    $fetch_by['RoleID'] = $role_id;
    $fetch_by[$can] = 1;

    $result = $CI->Module_rights_model->getWithMultipleFields($fetch_by);
    if ($result) {
        return true;
    } else {
        return false;
    }
}


function NullToEmpty($data)
{
    $returnArr = array();
    if (isset($data[0])) // checking if array is a multi-dimensional one, if so then checking for each row
    {
        $i = 0;
        foreach ($data as $row) {
            if (is_object($row)) {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i]->$key = "";
                    } else {
                        $returnArr[$i]->$key = $value;
                    }
                }
            } else {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i][$key] = "";
                    } else {
                        $returnArr[$i][$key] = $value;
                    }
                }
            }
            $i++;
        }
    } else {
        if (is_object($data)) {
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr->$key = "";
                } else {
                    $returnArr->$key = $value;
                }
            }
        } else {
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr[$key] = "";
                } else {
                    $returnArr[$key] = $value;
                }
            }
        }
    }
    return $returnArr;
}

function checkUserRightAccess($module_id, $user_id, $can)
{
    $CI = &get_Instance();
    $CI->load->model('Modules_users_rights_model');

    $fetch_by = array();
    $fetch_by['ModuleID'] = $module_id;
    $fetch_by['UserID'] = $user_id;
    $fetch_by[$can] = 1;

    $result = $CI->Modules_users_rights_model->getWithMultipleFields($fetch_by);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function getSystemLanguages()
{
    $CI = &get_Instance();
    $CI->load->model('System_language_model');
    $languages = $CI->System_language_model->getAllLanguages();
    return $languages;
}

function getDefaultLanguage()
{
    $CI = &get_Instance();
    $CI->load->Model('System_language_model');
    $fetch_by = array();
    $fetch_by['IsDefault'] = 1;
    $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
    return $result;
}

function DomainsTitle($domain_id = array(),$names = true){
    $CI = &get_Instance();
    $CI->load->Model('Domain_model');
   
    $result = $CI->Domain_model->getMultipleRowsWhereIn('DomainID',$domain_id,true);
    if($names){
        if($result){
            $names = array_column($result, 'Title');
            return implode(',',$names);
        }else{
            return '';
        }

    }
    return $result;
}

function categoryName($category_id, $language = 'EN')
{
    $CI = &get_Instance();
    $CI->load->Model('Category_model');

    $result = $CI->Category_model->getJoinedData(false, 'CategoryID', "categories.CategoryID = " . $category_id . " AND system_languages.ShortCode = '" . $language . "'");
    if ($result) {
        return $result[0]->Title;
    } else {
        return '';
    }
    //return $result;
}

function storeName($StoreID, $language = 'EN')
{
    $CI = &get_Instance();
    $CI->load->Model('Store_model');

    $result = $CI->Store_model->getJoinedData(false, 'StoreID', "stores.StoreID = " . $StoreID . " AND system_languages.ShortCode = '" . $language . "'");
    if ($result) {
        return $result[0]->Title;
    } else {
        return '';
    }
    //return $result;
}


function subCategories($category_id, $language)
{
    $CI = &get_Instance();
    $CI->load->Model('Category_model');

    $result = $CI->Category_model->getJoinedData(false, 'CategoryID', "categories.ParentID = " . $category_id . " AND system_languages.ShortCode = '" . $language . "'");
    //echo $CI->db->last_query();
    if ($result) {
        return $result;
    } else {
        return '';
    }
    //return $result;
}


function getAllActiveModules($role_id, $system_language_id, $where,$is_array = false)
{
    $CI = &get_Instance();
    $CI->load->Model('Module_rights_model');
    $result = $CI->Module_rights_model->getModulesWithRights($role_id, $system_language_id, $where,$is_array);
    return $result;
}


function getActiveUserModule($user_id, $system_language_id, $where)
{
    $CI = &get_Instance();
    $CI->load->Model('Modules_users_rights_model');
    $result = $CI->Modules_users_rights_model->getModulesWithRights($user_id, $system_language_id, $where);
    return $result;
}

function countStoreUser($store_id)
{
    $CI = &get_Instance();
    $CI->load->Model('User_model');
    $fetch_by = array();
    $fetch_by['StoreID'] = $store_id;
    $result = $CI->User_model->getMultipleRows($fetch_by);
    if ($result) {
        return count($result);
    } else {
        return '0';
    }

}

function checkAdminSession()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('admin')) {
        return true;

    } else {
        redirect($CI->config->item('base_url') . 'cms');
    }
}


function getCardID($coupon_id){
    $CI = &get_Instance();
    $CI->load->Model('Coupon_model');
   
    $result = $CI->Coupon_model->get($coupon_id,true,'CouponID');
    if ($result) {
        return $result['CardID'];
    } else {
        return '0';
    }
}

function sendEmail($data = array())
{
    $CI = &get_Instance();
    
//print_rm($data);
    
    $CI->load->library('email');
   // $CI->load->library('email');
    $CI->email->from('noreply@kfueit.com', site_title());
    $CI->email->to($data['to']);
    $CI->email->subject($data['subject']);
    $CI->email->message($data['message']);
    $CI->email->set_mailtype('html');
    if ($CI->email->send()) {
        //print_rm($CI->email->print_debugger(), true);
        return true;
    } else {
        return false;
    }
}

function sendSmsOfficial() // This is official function kept here so we can use it anywhere we want. It has all the functionality unifonic supports
{
    require FCPATH . '/vendor/Unifonic/Autoload.php';
    $client = new \Unifonic\API\Client();
    try {
        $response = $client->Messages->Send('+923368809300', 'test unifonic for bilal', 'MP-App'); // send regular massage
        dump($response);
        //$response = $client->Account->GetBalance();
        //$response = $client->Account->getSenderIDStatus('Arabic');
        //$response = $client->Account->getSenderIDs();
        //$response = $client->Account->GetAppDefaultSenderID();
        //$response = $client->Messages->Send('recipient','messageBody','senderName');
        //$response = $client->Messages->SendBulkMessages('96650*******,9665*******','Hello','UNIFONIC');
        //$response = $client->Messages->GetMessageIDStatus('9459*******');
        //$response = $client->Messages->GetMessagesReport();
        //$response = $client->Messages->GetMessagesDetails();
        //$response = $client->Messages->GetScheduled();
        echo '<pre>';
        print_r($response);
    } catch (Exception $e) {
        echo $e->getCode();
    }
}

// This function is made so that if sms is not sent still proceed with order placement
/*function sendSms($mobile_no, $msg, $debug = false) // Provide mobile no with country code, AppsID is configured in vendor/Unifonic/config.php
{
    // return true;
    require_once FCPATH . '/vendor/Unifonic/Autoload.php';
    $client = new \Unifonic\API\Client();
    $msg .= "\nFor further details please contact support.";
    $get_balance_response = $client->Account->GetBalance();
    // dump($get_balance_response);
    $balance = (isset ($get_balance_response->Balance) ? (int)$get_balance_response->Balance : 0);
    if ($balance > 1) { // for now we have checked if balance is greater than 1 SAR then call sms functionality
        try {
            $response = $client->Messages->Send($mobile_no, $msg, 'MP-App');
            if ($debug) // If this is true and message sent in try block then it will dump response here
            {
                dump($response);
            }
            if (isset($response->Status) && ($response->Status == 'Queued' || $response->Status == 'Sent' || $response->Status == 'Delivered')) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $error = $e->getCode();
            if ($debug) // If this is true and message failed to sent in try block then it will echo error message here
            {
                echo $error;
            }
            return false;
        }
    } else {
        return false;
    }
}*/


function sendSms($mobile_no, $msg, $debug = false) // Provide mobile no with country code, AppsID is configured in vendor/Unifonic/config.php
{
    // return true;
    require_once FCPATH . '/vendor/Unifonic/Autoload.php';
    $client = new \Unifonic\API\Client();
    try {
        $msg = $msg . "\nFrom: Exclusavecard.";
        $response = $client->Messages->Send($mobile_no, $msg, 'Exclusavecard');
        if ($debug) // If this is true and message sent in try block then it will dump response here
        {
            dump($response);
        }
        if (isset($response->Status) && ($response->Status == 'Queued' || $response->Status == 'Sent' || $response->Status == 'Delivered')) {
            return true;
        } else {
            return false;
        }
    } catch (Exception $e) {
        $error = $e->getCode();
        if ($debug) // If this is true and message failed to sent in try block then it will echo error message here
        {
            echo $error;
        }
        return false;
    }
}

function RandomString($digit = 4)
{
    $characters = '123456789123456789123456789123456789123456789';
    $randstring = '';
    for ($i = 0; $i < $digit; $i++) {
        $randstring .= $characters[rand(0, 40)];
    }
    return $randstring;
}

function generatePIN($digits = 4)
{
    $i = 0; //counter
    $pin = ""; //our default pin is blank.
    while ($i < $digits) {
        //generate a random number between 0 and 9.
        $pin .= mt_rand(0, 9);
        $i++;
    }
    return $pin;
}

function log_notification($data)
{
    $CI = &get_Instance();
    $CI->load->model('User_notification_model');
    $result = $CI->User_notification_model->save($data);
    if ($result > 0) {
        return true;
    } else {
        return false;
    }
}

function get_notifications()
{
    $CI = &get_Instance();
    $CI->load->model('Notification_model');
    $fetch_by['IsRead'] = 0;
    $fetch_by['NotificationTo'] = $CI->session->userdata['admin']['UserID'];
    
    $result = $CI->Notification_model->getMultipleRowsWithSort($fetch_by, 'notifications.CreatedAt', 'DESC');
    $retArr['result'] = $result;
    $retArr['result_count'] = $result ? count($result) : 0;
    return $retArr;
}

function sendPushNotificationToAndroid($title, $message, $registatoin_ids, $data = array())
{
    //$android_fcm_key = 'AAAAWMZ6qcY:APA91bHbo5FjHeboiRX4H3XpVPk3ObjrUpqmwF7VmmukI1IlianIZpbfE4COCib9g92zaC92S2XgXLTqVDLkf6RVtBSFiROp7g9eiaVaPERBA5tXdFuBJx7Ae1KanfVXeCCgEFrn63sz';
    $android_fcm_key = 'AAAAyf0eLXQ:APA91bE5fEuMofLEEHIhO_hRAhKaZEQC0tQyW6paRIq5V3rFkULxZ1Pnl36Mjfc7qb9T6qtNR7khoJKraSbqoM9Ga6A3HXtT2SyfSHQxlCqSi7sRRfLqEDkHWvlq6wNsbb0R7qV3RHmN';

    $sendData['title'] = $title;
    $sendData['body'] = $message;
    $url = 'https://fcm.googleapis.com/fcm/send';
    $fields = array(
        "registration_ids" => $registatoin_ids,
        "content_available" => true,
        "priority" => "high",
        "notification" => array
        (
            "title" => $title,
            "body" => $message,
            "sound" => "default"
        ),
        "data" => array
        (
            "body" => $message,
            "notificationKey" => $registatoin_ids,
            "priority" => "high",
            "sound" => "default",
            "notification_data" => $data
        ),
    );

    $headers = array(
        'Authorization:key=' . $android_fcm_key,
        'Content-Type: application/json'
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);

    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
   // print_rm($result);
    curl_close($ch);
    return $result;
}

function sendPushNotificationToIOS($title, $message, $register_keys, $data = array())
{
    $ios_fcm_key = 'AAAAyf0eLXQ:APA91bE5fEuMofLEEHIhO_hRAhKaZEQC0tQyW6paRIq5V3rFkULxZ1Pnl36Mjfc7qb9T6qtNR7khoJKraSbqoM9Ga6A3HXtT2SyfSHQxlCqSi7sRRfLqEDkHWvlq6wNsbb0R7qV3RHmN';

    $fields = array(
        //"to" => $gcm_ios_mobile_reg_key,
        "registration_ids" => $register_keys, //1000 per request logic is pending
        "content_available" => true,
        "priority" => "high",
        "notification" => array(
            "body" => strip_tags($message),
            "title" => $title,
            "sound" => "default"
        ),
        "data" => array
        (
            "body" => $message,
            "notificationKey" => $register_keys,
            "priority" => "high",
            "sound" => "default",
            "notification_data" => $data
        ),
    );


    $url = 'https://fcm.googleapis.com/fcm/send'; //note: its different than android.


    $headers = array(
        'Authorization: key=' . $ios_fcm_key,
        'Content-Type: application/json'
    );


    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        // echo 'abc';
        die('Curl failed: ' . curl_error($ch));
    }
    // echo 'cdf';
    //print_r($result);exit();
    // Close connection
    curl_close($ch);
    return $result;
}

function sendNotification($title, $message, $data, $user_id)
{
    $CI = &get_Instance();
    $CI->load->model('User_model');
    $res = 'Device token not found!';
    $user = $CI->User_model->get($user_id, true, 'UserID');
    if ($user['DeviceToken'] != '') {
        $token = array($user['DeviceToken']);
        if (strtolower($user['DeviceType']) == 'android') {
            $res = sendPushNotificationToAndroid($title, $message, $token, $data);
        } elseif (strtolower($user['DeviceType']) == 'ios') {
            $res = sendPushNotificationToIOS($title, $message, $token, $data);
        }
    }
    return $res;
}

function pusher($data, $channel, $event, $debug = false)
{
    require FCPATH . '/vendor/autoload.php';

    $app_key = '3f749e3c939bc5802992';
    $app_secret = 'c7e81df3d54460369963';
    $app_id = '827806';
    $app_cluster = 'ap2';

    $options = array(
        'cluster' => $app_cluster,
        'useTLS' => true
    );

    $pusher = new \Pusher\Pusher(
        $app_key,
        $app_secret,
        $app_id,
        $options
    );

    $response = $pusher->trigger($channel, $event, $data);
    if ($debug) {
        echo '<pre>';
        print_r($response);
        exit();
    }
    return true;
}

function convertTimestampToLocalDatetime($timestamp)
{
    // first converting timestamp to GMT date time
    $datetime = gmdate("Y-m-d H:i:s", $timestamp);

    // setting default timezone
    if (isset($_COOKIE['system_timezone'])) {
        $current_timezone = $_COOKIE['system_timezone'];
    } else {
        $current_timezone = 'Asia/Riyadh';
    }

    // creating date time object from the date time coming in UTC format
    $utc_date = DateTime::createFromFormat(
        'Y-m-d H:i:s',
        $datetime,
        new DateTimeZone('UTC')
    );
    $acst_date = clone $utc_date;

    // setting timezone to local timezone for UTC time coming above
    $acst_date->setTimeZone(new DateTimeZone($current_timezone));

    // formatting datetime
    $original_time = $utc_date->format('Y-m-d H:i:s');
    $converted_local_time = $acst_date->format('Y-m-d H:i:s');
    return $converted_local_time;
}

function getFormattedDateTime($timestamp, $format)
{
    $timestamp = trim($timestamp);
    return date($format, strtotime(convertTimestampToLocalDatetime($timestamp)));
}

function site_settings()
{
    $CI = &get_Instance();
    $CI->load->model('Site_setting_model');
    return $CI->Site_setting_model->get(1, false, 'SiteSettingID');
}

function site_title()
{
    $CI = &get_Instance();
    $CI->load->model('Site_setting_model');
    return $site_setting = $CI->Site_setting_model->get(1, false, 'SiteSettingID')->SiteName;
}

function site_logo()
{
    $CI = &get_Instance();
    $CI->load->model('Site_setting_model');
    $site_setting = $CI->Site_setting_model->get(1, false, 'SiteSettingID');
    return base_url($site_setting->SiteImage);
}

function my_site_url()
{
    $input = base_url();

// in case scheme relative URI is passed, e.g., //www.google.com/
    $input = trim($input, '/');

// If scheme not included, prepend it
    if (!preg_match('#^http(s)?://#', $input)) {
        $input = 'http://' . $input;
    }

    $urlParts = parse_url($input);

// remove www
    $domain = preg_replace('/^www\./', '', $urlParts['host']);

    return $domain;

// output: google.co.uk
}

function generateBarcode($text, $file_name, $type = 'image')
{
    require FCPATH . '/vendor/autoload.php';
    $time = time();
    $file_path = "uploads/barcode/$file_name.png";
    // $generator = new Picqer\Barcode\BarcodeGeneratorSVG();
    // $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
    // $generator = new Picqer\Barcode\BarcodeGeneratorJPG();
    // $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
    // return $generator->getBarcode('081231723897', $generator::TYPE_CODE_128);
    if ($type == 'image') {
        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
        $barcode = $generator->getBarcode($text, $generator::TYPE_CODE_128);
        file_put_contents($file_path, $barcode);
        return $file_path;
    } elseif ($type == 'html') {
        $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
        return $generator->getBarcode($text, $generator::TYPE_CODE_128);
    }
}

function generate_pdf($html, $filename = 'invoice')
{
    require FCPATH . '/vendor/autoload.php';
    $filename = $filename . "-" . time() . ".pdf";
    $mpdf = new \Mpdf\Mpdf();
    $mpdf->SetTitle('Booking Receipt');
    $mpdf->WriteHTML($html);
    $mpdf->Output($filename, 'I');
}

function booking_html($booking_info)
{
    $html = '<table cellspacing="0" width="100%" style="width:100%">';
    $html .= '<tr><td>OrderNumber </td><td>' . $booking_info['OrderNumber'] . '</td></tr>';
    $html .= '<tr><td>Status </td><td>' . $booking_info['BookingStatusEn'] . '</td></tr>';
    $html .= '<tr><td>Address </td><td>' . $booking_info['Address'] . '</td></tr>';
    $html .= '<tr><td>ServiceCost </td><td>' . $booking_info['ServiceCost'] . '</td></tr>';
    $html .= '<tr><td>CategoryTitle </td><td>' . $booking_info['CategoryTitle'] . '</td></tr>';
    $html .= '</table>';
    return $html;
}

function emailTemplate($msg)
{
    $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MPKSA</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}

#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>

<body>

<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<p style="font-family:sans-serif;font-size:14px;">' . $msg . '</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Exclusave Card</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">www.exclusavecard.com</a>
</span>
</td>
</tr>
</table>

</td>
</tr>
</table>

</body>
</html>';

    return $html;
}

function custom_encode($str)
{
    return substr(json_encode($str), 1, -1);
}

function custom_decode($str)
{
    return json_decode(sprintf('"%s"', $str));
}

function getArabicCityName($city_name)
{
    $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=$city_name&key=AIzaSyA7vMfxgK_KPChKiJkCwnjn7m_J5h7hNlM&language=ar";
    $ch = curl_init();
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json',
    );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
    $result = curl_exec($ch);
    curl_close($ch);
    if ($result === FALSE) {
        return $city_name;
    } else {
        $result = json_decode($result, true);
        // dump($result);
        if (isset($result['status']) && $result['status'] == 'OK') {
            if (isset($result['results'][0]['name']) && $result['results'][0]['name'] != '') {
                return $result['results'][0]['name'];
            } else {
                return $city_name;
            }
        } else {
            return $city_name;
        }
    }

}

function getRecords($city_name)
{
    $url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyCYI6hdC9nNUMT01vo5mEiJxlHa5kt0q_k&input=$city_name&country=sa&language=ar";
    $ch = curl_init();
    $headers = array(
        'Accept: application/json',
        'Content-Type: application/json',
    );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
    $result = curl_exec($ch);
    curl_close($ch);
    if ($result === FALSE) {
        return $city_name;
    } else {
        $result = json_decode($result, true);
        dump($result);
        if (isset($result['status']) && $result['status'] == 'OK') {
            if (isset($result['results'][0]['name']) && $result['results'][0]['name'] != '') {
                return $result['results'][0]['name'];
            } else {
                return $city_name;
            }
        } else {
            return $city_name;
        }
    }

}

function get_unread_bookings()
{
    $CI = &get_Instance();
    $CI->load->model('Booking_model');
    $fetch_by['IsRead'] = 0;
    if ($CI->session->userdata['admin']['StoreID'] > 0)
    {
        $fetch_by['StoreID'] = $CI->session->userdata['admin']['StoreID'];
    }
    $result = $CI->Booking_model->getMultipleRowsWithSort($fetch_by, 'bookings.BookingID', 'DESC');
    $retArr['result'] = $result;
    $retArr['result_count'] = $result ? count($result) : 0;
    return $retArr;
}


function get_unread_transactions()
{
    $CI = &get_Instance();
    $CI->load->model('Store_model');
   // $fetch_by['TransactionIsRead'] = 0;
     $where = 'transactions.TransactionIsRead = 0';
    if ($CI->session->userdata['admin']['StoreID'] > 0)
    {
         $where .= ' AND transactions.StoreID = ' . $this->session->userdata['admin']['StoreID'];
    }
    //$result = $CI->Transaction_model->getMultipleRowsWithSort($fetch_by, 'transactions.TransactionID', 'DESC');
    $result = $CI->Store_model->getAllTransaction('EN', $where);
    $retArr['result'] = $result;
    $retArr['result_count'] = $result ? count($result) : 0;
    return $retArr;
}

function get_unread_complaints()
{
    $CI = &get_Instance();
    $CI->load->model('Store_model');
   // $fetch_by['TransactionIsRead'] = 0;
     $where = 'ratings.RatingIsRead = 0';
    if ($CI->session->userdata['admin']['StoreID'] > 0)
    {
         $where .= ' AND ratings.StoreID = ' . $this->session->userdata['admin']['StoreID'];
    }
    //$result = $CI->Transaction_model->getMultipleRowsWithSort($fetch_by, 'transactions.TransactionID', 'DESC');
    $result = $CI->Store_model->getAllRatingsWithOutTransactions('EN', $where);
    $retArr['result'] = $result;
    $retArr['result_count'] = $result ? count($result) : 0;
    return $retArr;
}
function get_cities($language)
{
    $CI = &get_Instance();
    $CI->load->model('City_model');
    $cities = $CI->City_model->getAllJoinedData(false, 'CityID', $language, "cities.IsActive = 1", 'ASC', 'SortOrder');
    return $cities;
}

function getOrderItems($order_id)
{
    $CI = &get_Instance();
    $CI->load->model('Order_item_model');
    $items = $CI->Order_item_model->getOrdersItems($order_id);
    return $items;
}

function getTotalProduct($user_id)
{
    $CI = &get_Instance();
    $CI->load->model('Temp_order_model');
    $result = $CI->Temp_order_model->getTotalProduct($user_id);
    return $result[0]->CartProductsCount > 0 ? $result[0]->CartQuantityCount : 0;
}

function getLanguage()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('lang')) {
        $language = $CI->session->userdata('lang');
    } else {
        $result = getDefaultLanguage();
        if ($result) {
            $language = $result->ShortCode;
        } else {
            $language = 'EN';
        }
    }

    return $language;
}

function getTaxShipmentCharges($type = 'Shipment', $OnlyDefault = false) // type == Shipment, Tax
{
    $CI = &get_Instance();
    $CI->load->model('Tax_shipment_charges_model');
    $lang = getLanguage();
    $tax_shipment = new Tax_shipment_charges_model();
    if ($OnlyDefault) {
        $where = "tax_shipment_charges.ChargesType = '" . $type . "' AND tax_shipment_charges.IsDefault = 1";
        $defaultCharge = $tax_shipment->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return (isset($defaultCharge[0]) ? $defaultCharge[0] : false);
    } else {
        $where = "tax_shipment_charges.ChargesType = '" . $type . "' AND tax_shipment_charges.IsActive = 1";
        $charges = $tax_shipment->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return $charges;
    }
}

function getSelectedShippingMethodDetail($ShipmentMethodID, $lang)
{
    $CI = &get_Instance();
    $CI->load->model('Tax_shipment_charges_model');
    if ($ShipmentMethodID > 0) {
        $where = "tax_shipment_charges.TaxShipmentChargesID = " . $ShipmentMethodID;
        $shipment_method = $CI->Tax_shipment_charges_model->getAllJoinedData(false, 'TaxShipmentChargesID', $lang, $where);
        return (isset($shipment_method[0]) ? $shipment_method[0] : false);
    } else {
        $shipment_method = false;
    }
    return $shipment_method;
}

function productAverageRating($ProductID)
{
    $CI = &get_Instance();
    $CI->load->model('Product_rating_model');
    $rating = $CI->Product_rating_model->getAverageRating($ProductID);
    return round($rating->average_rating, 1);
}

function productRatings($ProductID)
{
    $CI = &get_Instance();
    $CI->load->model('Product_rating_model');
    $total_ratings_count = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID));
    if ($total_ratings_count > 0) {
        $rating_count_1 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 1));
        $rating_count_2 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 2));
        $rating_count_3 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 3));
        $rating_count_4 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 4));
        $rating_count_5 = $CI->Product_rating_model->getRowsCount(array('ProductID' => $ProductID, 'Rating' => 5));
        $response_arr['rating_1'] = ($rating_count_1 / $total_ratings_count) * 100;
        $response_arr['rating_2'] = ($rating_count_2 / $total_ratings_count) * 100;
        $response_arr['rating_3'] = ($rating_count_3 / $total_ratings_count) * 100;
        $response_arr['rating_4'] = ($rating_count_4 / $total_ratings_count) * 100;
        $response_arr['rating_5'] = ($rating_count_5 / $total_ratings_count) * 100;
        $response_arr['total_ratings_count'] = $total_ratings_count;
    } else {
        $response_arr['rating_1'] = 0;
        $response_arr['rating_2'] = 0;
        $response_arr['rating_3'] = 0;
        $response_arr['rating_4'] = 0;
        $response_arr['rating_5'] = 0;
        $response_arr['total_ratings_count'] = 0;
    }
    return $response_arr;
}

function uploadImage($file_key, $path)
{
    $extensions_alowed = array("jpeg", "jpg", "png", "JPEG", "JPG", "PNG");
    $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name']);
    $file_tmp = $_FILES[$file_key]["tmp_name"];
    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
    if (true || in_array($ext, $extensions_alowed)) {
        move_uploaded_file($file_tmp, $path . $file_name);
        return $path . $file_name;
    } else {
        return '';
    }
}

function uploadVideo($file_key, $path)
{
    $extensions_alowed = array("mp4", "MP4", "3gp", "3GP");
    $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name']);
    $file_tmp = $_FILES[$file_key]["tmp_name"];
    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
    if (in_array($ext, $extensions_alowed)) {
        move_uploaded_file($file_tmp, $path . $file_name);
        return $path . $file_name;
    } else {
        return '';
    }
}

function uploadMultipleImages($file_key, $path, $id = false, $type = false)
{
    $data = array();
    $extensions_alowed = array("jpeg", "jpg", "png", "JPEG", "JPG", "PNG");
    foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
        $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
        $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
        if (in_array($ext, $extensions_alowed)) {
            move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);
            $this->load->model('Site_images_model');
            $data['FileID'] = $id;
            $data['ImageType'] = $type;
            $data['ImageName'] = $path . $file_name;
            $this->Site_images_model->save($data);
        }
    }
}

function uploadFileFromBase64($base64encoded_string, $path, $extension = 'png')
{
    $filename = rand(9999, 99999999999) . date('Ymdhsi');
    $upload_path = $path . $filename . "." . $extension;
    file_put_contents($upload_path, base64_decode($base64encoded_string));
    return $upload_path;
}

function get_order_invoice($order_id, $type = 'invoice')
{
    $CI = &get_Instance();
    $CI->load->model('Order_model');
    $CI->load->model('User_address_model');
    $order_details = $CI->Order_model->getOrders("orders.OrderID = $order_id");
    $data['order'] = $order_details[0];
    $data['payment_address'] = $CI->User_address_model->getAddresses("user_address.AddressID = " . $order_details[0]->AddressIDForPaymentCollection)[0];
    $data['type'] = $type;
    $order_html = $CI->load->view('emails/order_confirmation', $data, true);
    return $order_html;
}

function email_format($content)
{
    $CI = &get_Instance();
    $data['content'] = $content;
    $html = $CI->load->view('emails/general_email', $data, true);
    return $html;
}

function get_email_template($template_id, $lang = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('Email_template_model');
    $where = "email_templates.Email_templateID = " . $template_id . " AND system_languages.ShortCode = '" . $lang . "'";
    $template = $CI->Email_template_model->getJoinedData(false, 'Email_templateID', $where);
    return $template[0];
}

function get_unread_orders()
{
    $CI = &get_Instance();
    $CI->load->model('Order_model');
    $result = $CI->Order_model->getMultipleRowsWithSort(array('IsRead' => 0), 'orders.OrderID', 'DESC');
    $retArr['result'] = $result;
    $retArr['result_count'] = $result ? count($result) : 0;
    return $retArr;
}

function getCategories($language)
{
    $CI = &get_Instance();
    $CI->load->model('Category_model');
    return $CI->Category_model->getAllJoinedData(false, 'CategoryID', $language, 'categories.ParentID = 0 AND categories.IsActive = 1');
}

function getAddressDetail($AddressID)
{
    $CI = &get_Instance();
    $CI->load->model('User_address_model');
    $address = $CI->User_address_model->getAddresses("user_address.AddressID = " . $AddressID);
    return $address[0];
}

function getUserOffers($language)
{
    $CI = &get_Instance();
    $CI->load->model('Offer_user_notification_model');
    if ($CI->session->userdata('user')) {
        $UserID = $CI->session->userdata['user']->UserID;
        $user_offers = $CI->Offer_user_notification_model->getUserOffers($UserID, $language);
        return $user_offers;
    } else {
        return false;
    }
}

function IsProductUnderOffer($product_id)
{
    // this function is not final yet
    $CI = &get_Instance();
    $CI->load->model('Offer_user_notification_model');
    $html = '';
    if ($CI->session->userdata('user')) {
        $UserID = $CI->session->userdata['user']->UserID;
        $user_offers = $CI->Offer_user_notification_model->getAllUserOffers($UserID);
        if ($user_offers and count($user_offers) > 0) {
            foreach ($user_offers as $user_offer) {
                $ProductIDs = explode(',', $user_offer->ProductID);
                if (in_array($product_id, $ProductIDs)) {
                    $html = '<h4 class="offered_product" title="' . $user_offer->Description . '">' . $user_offer->Title . '</h4>';
                    // $offer_title = $user_offer->Title;
                    // $offer_description = $user_offer->Description;
                    break;
                }
            }
        }
    }
    return $html;
}

function IsProductPurchased($UserID, $ProductID)
{
    $CI = &get_Instance();
    $CI->load->model('Order_item_model');
    $items = $CI->Order_item_model->getOrdersItemsWhere($UserID, $ProductID);
    if (count($items) > 0) {
        return true;
    } else {
        return false;
    }
}

function compress($source, $destination, $quality = 10)
{
    ini_set('memory_limit', '-1');
    $info = getimagesize($source);

    if ($info['mime'] == 'image/jpeg')
        $image = imagecreatefromjpeg($source);

    elseif ($info['mime'] == 'image/gif')
        $image = imagecreatefromgif($source);

    elseif ($info['mime'] == 'image/png')
        $image = imagecreatefrompng($source);

    imagejpeg($image, $destination, $quality);

    return $destination;
}

function boothAverageRating($BoothID)
{
    // needs to work on this
    return 3.5;
    /*$CI = &get_Instance();
    $CI->load->model('Product_rating_model');
    $rating = $CI->Product_rating_model->getAverageRating($ProductID);
    return round($rating->average_rating, 1);*/
}

function mask_number($number, $maskingCharacter = '*')
{
    return $number;

    if ($number != '') {
        return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4);
    } else {
        return 'N/A';
    }
}

function convertEmptyToNA($data)
{
    if (is_array($data)) // checking if array is a multi-dimensional one, if so then checking for each row
    {
        foreach ($data as $key => $value) {
            if ($value == '') {
                $data[$key] = "N/A";
            }
        }
    } elseif (is_object($data)) {
        foreach ($data as $key => $value) {
            if ($value == '') {
                $data->$key = "N/A";
            }
        }
    }
    return $data;
}

function getCurrencySymbol($currencyCode, $locale = 'en_US')
{
    $formatter = new \NumberFormatter($locale . '@currency=' . $currencyCode, \NumberFormatter::CURRENCY);
    return $formatter->getSymbol(\NumberFormatter::CURRENCY_SYMBOL);
}

function text_shorter($text, $chars_limit = 40)
{
    // Check if length is larger than the character limit
    if (strlen($text) > $chars_limit)
    {
        // If so, cut the string at the character limit
        $new_text = substr($text, 0, $chars_limit);
        // Trim off white space
        $new_text = trim($new_text);
        // Add at end of text ...
        return $new_text . "...";
    }
    // If not just return the text as is
    else
    {
        return $text;
    }
}

function getLatestUsers($language = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('User_model');
    $latest_booths = $CI->User_model->getUsers("users.FirstName != '' AND users.RoleID = 2", $language, 'users.UpdatedAt', 'DESC');
    $latest_users = $CI->User_model->getUsers("users.FirstName != '' AND users.RoleID = 2", $language, 'users.UpdatedAt', 'DESC');
    $retArr['booths'] = count($latest_booths) > 0 ? $latest_booths : array();
    $retArr['users'] = count($latest_users) > 0 ? $latest_users : array();
    return $retArr;
}

function productsPosted($language = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('Product_model');
    $CI->load->model('Product_like_model');
    $most_selling_products = $CI->Product_model->getMostSellingProducts($language, 'DESC', 10, 0);
    $least_selling_products = $CI->Product_model->getMostSellingProducts($language, 'ASC', 10, 0);
    $most_liked_products = $CI->Product_like_model->getMostLikedProducts($language, 'DESC', 10, 0);
    $retArr['most_selling_products'] = $most_selling_products;
    $retArr['least_selling_products'] = $least_selling_products;
    $retArr['most_liked_products'] = $most_liked_products;
    return $retArr;
}

function getProductImages($ProductID)
{
    $CI = &get_Instance();
    $CI->load->model('Product_image_model');
    $images = $CI->Product_image_model->getMultipleRows(array('ProductID' => $ProductID));
    return $images;
}

function random_password($length = 8, $only_caps = false,$only_digits = false) {
    if ($only_caps)
    {
        $input = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    } else if($only_digits){
         $input = '123456789123456789123456789';
    }else {
        $input = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    $input_length = strlen($input);
    $random_string = '';
    for($i = 0; $i < $length; $i++) {
        $random_character = $input[mt_rand(0, $input_length - 1)];
        $random_string .= $random_character;
    }

    return $random_string;
}

function verifyPaytabsPayment($TransactionID)
{
    $fields = array(
        'merchant_email' => 'dev@zynq.net',
        'secret_key' => 'c7YRQHdLzzMN3TyMS6VmhZQEvgUMjj1vq0mgHz2h9P4cIDZBJUY1TOFp4AboTdTkdgBVQDcZshcbUbME5KFZzloEWA2PNV91cKX2',
        'transaction_id' => $TransactionID
    );
    $url = 'https://www.paytabs.com/apiv2/verify_payment_transaction';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
    return json_decode($result);
}

function frontend_assets()
{
    return base_url('assets/frontend/');
}

function getUserInfo($UserID, $NameOnly = false)
{
    $CI = &get_Instance();
    $CI->load->Model('User_model');
    $fetch_by['UserID'] = $UserID;
    $result = $CI->User_model->getWithMultipleFields($fetch_by);
    if ($NameOnly) {
        return $result->FirstName.' '.$result->MiddleName.' '.$result->LastName;
    } else {
        return $result;
    }

}

function getRoleInfo($RoleID, $NameOnly = false)
{
    $CI = &get_Instance();
    $CI->load->Model('Role_text_model');
    $fetch_by['RoleID'] = $RoleID;
    $fetch_by['SystemLanguageID'] = 1;
    $result = $CI->Role_text_model->getWithMultipleFields($fetch_by);
    if ($NameOnly) {
        return $result->Title;
    } else {
        return $result;
    }

}

function getCoupons($CouponIDs, $NameOnly = false)
{
    $CI = &get_Instance();
    $CI->load->Model('Coupon_model');
    
    $result = $CI->Coupon_model->getCoupons($CouponIDs);
    if ($NameOnly) {
        $names = '';
        if(!empty($result)){
            $names = array_column($result, 'CouponCode');
            $names = implode(',',$names);
        }

        return $names;
    } else {
        return $result;
    }

}

function getStoreInfo($StoreID, $NameOnly = false,$lang = 'EN')
{
    $CI = &get_Instance();
    $CI->load->Model('Store_model');
   
    $result = $CI->Store_model->getJoinedData(false, 'StoreID', "stores.StoreID = " . $StoreID . " AND system_languages.ShortCode = '" . $lang . "'")[0];
    if ($NameOnly) {
        return $result->Title;
    } else {
        return $result;
    }

}

function getPageData($page_id, $lang = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('Page_model');
    $result = $CI->Page_model->getJoinedData(false, 'PageID', "pages.PageID = " . $page_id . " AND system_languages.ShortCode = '" . $lang . "'")[0];
    return $result;
}

function getFaqsInCategory($faq_category_id, $lang = 'EN')
{
    $CI = &get_Instance();
    $CI->load->model('Faq_model');
    $result = $CI->Faq_model->getAllJoinedData(false, 'FaqID', $lang, "faqs.ParentID = $faq_category_id AND faqs.IsActive = 1");
    return $result;
}