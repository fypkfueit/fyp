<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

ERROR - 2019-12-13 05:15:03 --> Query error: Expression #75 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'fyp.cities_text.Title' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by - Invalid query: SELECT `bookings`.*, `bookings`.`IsFamilyMember` as `BookingByFamilyMember`, `users`.*, `cities_text`.`Title` as `UserCity`, `stores`.`ParentID`, `stores_text`.`Title`, `stores_text`.`MainBranchTitle`, `cardtypes_text`.`Title` as `CardTypeTitle`
FROM `bookings`
JOIN `users` ON `bookings`.`UserID` = `users`.`UserID`
LEFT JOIN `stores` ON `bookings`.`StoreID` = `stores`.`StoreID`
LEFT JOIN `stores_text` ON `stores`.`StoreID` = `stores_text`.`StoreID`
LEFT JOIN `cards` ON `users`.`CardID` = `cards`.`CardID`
LEFT JOIN `cardgroups` ON `cards`.`CardgroupID` = `cardgroups`.`CardgroupID`
LEFT JOIN `cardtypes` ON `cardgroups`.`CardtypeID` = `cardtypes`.`CardtypeID`
LEFT JOIN `cardtypes_text` ON `cardtypes`.`CardtypeID` = `cardtypes_text`.`CardtypeID`
LEFT JOIN `cities` ON `users`.`CityID` = `cities`.`CityID`
LEFT JOIN `cities_text` ON `cities`.`CityID` = `cities_text`.`CityID`
LEFT JOIN `system_languages` as `slct` ON `slct`.`SystemLanguageID` = `cities_text`.`SystemLanguageID`
WHERE `slct`.`ShortCode` = 'EN'
GROUP BY `bookings`.`BookingID`
ORDER BY `bookings`.`BookingDate` DESC
