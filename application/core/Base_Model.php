<?php

class Base_Model extends CI_Model
{

    public $table;

    public function __construct($table = "")
    {
        parent::__construct();

        if (!empty ($table)) {
            $this->table = $table;

            $fields = $this->db->list_fields($table);

            foreach ($fields as $field) {
                $this->$field = NULL;
            }
        }

    }

    /**
     * Inserts a row into the table and returns the row id
     * @param data Array
     * @return insert_id int
     */
    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function insert_batch($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

    public function update_batch($data, $update_by)
    {
        return $this->db->update_batch($this->table, $data, $update_by);

    }

    /**
     *
     * @param array $data
     * @param array $search
     */
    public function update($data, $search)
    {
        $this->db->update($this->table, $data, $search);
        $this->db->last_query();
    }

    /**
     * Get an instance of the model and initialize the properties with table row for supplied ID.
     * @param unknown $id
     * @return Base_Model|boolean
     */
    public function get($id, $as_array = false, $field_name = false)
    {
        if ($field_name) {
            $result = $this->db->get_where($this->table, array($field_name => $id));

        } else {
            $result = $this->db->get_where($this->table, array('id' => $id));

        }
        if ($result->num_rows() > 0) {
            $row = $result->row_array();

            if ($as_array) {
                return $row;
            }

            foreach ($row as $col => $val) {
                $this->$col = $val;
            }

            return $this;
        } else {
            return false;
        }
    }


    public function getAllJoinedData($as_array = false, $join_field, $system_language_code = false, $where = false, $sort = 'ASC', $sort_field = 'SortOrder', $custom_sort = false)// please for now not usable function in text model
    {

        $first_field = $this->table . '.' . $join_field;
        $second_field = $this->table . '_text.' . $join_field;
        $this->db->select($this->table . '.*, ' . $this->table . '_text.*');
        $this->db->join($this->table . '_text', $first_field . ' = ' . $second_field);
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = ' . $this->table . '_text.SystemLanguageID');


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where($this->table . '.Hide', '0');
        if ($custom_sort) {
            $this->db->order_by($sort_field, $sort);
        } else {
            $this->db->order_by($this->table . '.' . $sort_field, $sort);
        }
        $result = $this->db->get($this->table);
        //echo $this->db->last_query();exit();
        if ($as_array) {

            $data = $result->result_array();
        } else {
            $data = $result->result();
        }


        return $data;

    }


    public function getJoinedDataWithOutText($as_array=false,$join_field,$second_table,$where = false)// please for now not usable function in text model 
    {
        
        $first_field = $this->table.'.'.$join_field;
        $second_field = $second_table.'.'.$join_field;
        $this->db->select($this->table.'.*, ' . $second_table . '.*');
        $this->db->join($second_table,$first_field.' = '.$second_field );
        
        if($where)
        {
            $this->db->where($where);
        }
        
        //$this->db->order_by($this->table.'_cod.Code','asc');
        $result = $this->db->get($this->table);
        //echo $this->db->last_query();exit();
        if($as_array)
        {
            // return $result->result_array();
            return $data =  $result->result_array();
        }
        
        $data = $result->result();

        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/

        /*foreach ($data as $row) {
            if($as_array) {
                if($row['Code'] == ''){
                    $row['Code'] = $this->GetDefaultLangData($join_field , $row[$join_field]);
                }
            } else {
                if($row->Code == ''){
                    $row->Code = $this->GetDefaultLangData($join_field , $row->$join_field);
                }
            }
        
        }*/
        
        return $data;
        //return $result->result();
    }


    public function getJoinedData($as_array = false, $join_field, $where = false, $sort = 'ASC', $sort_field = 'SortOrder', $custom_sort = false)// please for now not usable function in text model
    {

        $first_field = $this->table . '.' . $join_field;
        $second_field = $this->table . '_text.' . $join_field;
        $this->db->select($this->table . '.*, ' . $this->table . '_text.*');
        $this->db->join($this->table . '_text', $first_field . ' = ' . $second_field);
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = ' . $this->table . '_text.SystemLanguageID', 'Left');

        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('system_languages.Hide', 0);

        if ($custom_sort) {
            $this->db->order_by($sort_field, $sort);
            // $this->db->order_by($this->table . '.' . $sort_field, $sort);
        } else {
            $this->db->order_by('system_languages.IsDefault', 'DESC');
        }

        $result = $this->db->get($this->table);

        if ($as_array) {
            //return $result->result_array();
            $data = $result->result_array();
        } else {
            $data = $result->result();
        }


        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/


        return $data;
        //return $result->result();
    }

    public function getMultipleRows($fields, $as_array = false, $idOrderBy = 'asc', $limit = false, $start = 0)
    {


        if ($idOrderBy == 'desc')
            $this->db->order_by('id', 'desc');

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $result = $this->db->get_where($this->table, $fields);

        //$this->db->last_query(); exit();

        if ($result->num_rows() > 0) {


            if ($as_array) {
                return $result->result_array();
            }

            return $result->result();
        } else {
            return false;
        }
    }


    public function getMultipleRowsWhereIn($fields,$valuesArr ,$as_array = false, $idOrderBy = 'asc', $limit = false, $start = 0)
    {


        if ($idOrderBy == 'desc')
            $this->db->order_by('id', 'desc');

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $this->db->where_in($fields, $valuesArr);

        $result = $this->db->get($this->table);

        //$this->db->last_query(); exit();

        if ($result->num_rows() > 0) {


            if ($as_array) {
                return $result->result_array();
            }

            return $result->result();
        } else {
            return false;
        }
    }

    public function getMultipleRowsWithSort($fields, $order_by = false, $order_as = false, $limit = false, $start = 0)
    {
        if ($order_by && $order_as)
            $this->db->order_by($order_by, $order_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get_where($this->table, $fields);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
    }

    public function getWithMultipleFields($fields, $as_array = false)
    {


        $result = $this->db->get_where($this->table, $fields);

        if ($result->num_rows() > 0) {
            $row = $result->row_array();

            if ($as_array) {
                return $row;
            }

            foreach ($row as $col => $val) {
                $this->$col = $val;
            }


            return $this;
        } else {
            return false;
        }

    }

    /**
     * Gets all the records from the table.
     */
    public function getAll($as_array = false)
    {
        $result = $this->db->get($this->table);

        if ($as_array) {
            return $result->result_array();
        }

        return $result->result();
    }

    public function deleteIn($key, $valuesArr)
    {

        $this->db->where_in($key, $valuesArr);
        $this->db->delete($this->table);

    }

    public function delete($search)
    {
        $this->db->delete($this->table, $search);

    }


    public function getFields()
    {
        $fields = $this->db->list_fields($this->table);
        return $fields;
    }


    public function getLastRow($primery_key = 'id')
    {
        return $this->db->select('SortOrder')->order_by($primery_key, "desc")->limit(1)->get($this->table)->row_array();
    }

    public function getJoinedDataWithOtherTable($as_array = false, $join_field, $second_table, $where = false)// please for now not usable function in text model
    {

        $first_field = $this->table . '.' . $join_field;
        $second_field = $second_table . '.' . $join_field;
        $this->db->select($this->table . '.*, ' . $second_table . '.*');
        $this->db->join($second_table, $first_field . ' = ' . $second_field);

        if ($where) {
            $this->db->where($where);
        }

        //$this->db->order_by($this->table.'_cod.Code','asc');
        $result = $this->db->get($this->table);
        //echo $this->db->last_query();exit();
        if ($as_array) {
            // return $result->result_array();
            return $data = $result->result_array();
        }

        $data = $result->result();

        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/

        /*foreach ($data as $row) {
            if($as_array) {
                if($row['Code'] == ''){
                    $row['Code'] = $this->GetDefaultLangData($join_field , $row[$join_field]);
                }
            } else {
                if($row->Code == ''){
                    $row->Code = $this->GetDefaultLangData($join_field , $row->$join_field);
                }
            }

        }*/

        return $data;
        //return $result->result();
    }

    public function getLeftJoinedDataWithOtherTable($as_array = false, $join_field, $second_table, $where = false)// please for now not usable function in text model
    {

        $first_field = $this->table . '.' . $join_field;
        $second_field = $second_table . '.' . $join_field;
        $this->db->select($second_table . '.*, ' . $this->table . '.*');
        $this->db->join($second_table, $first_field . ' = ' . $second_field, 'left');

        if ($where) {
            $this->db->where($where);
        }

        //$this->db->order_by($this->table.'_cod.Code','asc');
        $result = $this->db->get($this->table);
        //echo $this->db->last_query();exit();
        if ($as_array) {
            // return $result->result_array();
            return $data = $result->result_array();
        }

        $data = $result->result();

        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/

        /*foreach ($data as $row) {
            if($as_array) {
                if($row['Code'] == ''){
                    $row['Code'] = $this->GetDefaultLangData($join_field , $row[$join_field]);
                }
            } else {
                if($row->Code == ''){
                    $row->Code = $this->GetDefaultLangData($join_field , $row->$join_field);
                }
            }

        }*/

        return $data;
        //return $result->result();
    }

    public function getJoinedDataWithOtherTableWithLimit($as_array = false, $join_field, $second_table, $where = false)// please for now not usable function in text model
    {

        $first_field = $this->table . '.' . $join_field;
        $second_field = $second_table . '.' . $join_field;
        $this->db->select($this->table . '.*, ' . $second_table . '.*');
        $this->db->join($second_table, $first_field . ' = ' . $second_field);

        if ($where) {
            $this->db->where($where);
        }

        //$this->db->order_by($this->table.'_cod.Code','asc');
        $result = $this->db->get($this->table);
        //echo $this->db->last_query();exit();
        if ($as_array) {
            // return $result->result_array();
            return $data = $result->result_array();
        }

        $data = $result->result();

        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/

        /*foreach ($data as $row) {
            if($as_array) {
                if($row['Code'] == ''){
                    $row['Code'] = $this->GetDefaultLangData($join_field , $row[$join_field]);
                }
            } else {
                if($row->Code == ''){
                    $row->Code = $this->GetDefaultLangData($join_field , $row->$join_field);
                }
            }

        }*/

        return $data;
        //return $result->result();
    }

    function getRowsCount($search)
    {

        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($search);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function updateWhere($data, $search)
    {
        $this->db->update($this->table, $data, $search);
        if ($this->db->affected_rows() == '1') {
            return true;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

}