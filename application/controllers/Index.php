<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        
        
        $this->load->model('User_model');
        
        $this->data['language'] = $this->language;
    }

    public function test_notification()
    {
        $result = sendNotification("Test", "Test Notification", array(), 3);
        dump($result);
    }

    public function index()
    {
        redirect(base_url('cms/account/login'));
    }

    public function home()
    {
        $this->data['student_featured_promotions'] = $this->Promotion_model->getFeaturedPromotions("promotions.StudentIndividual = 'Yes'");
        $this->data['corporate_featured_promotions'] = $this->Promotion_model->getFeaturedPromotions("promotions.CorporateIndividual = 'Yes'");
        $this->data['news_events'] = $this->News_model->getAllJoinedData(false, 'NewsID', $this->language, "news.IsActive = 1");
        $this->data['home_slides'] = $this->Home_slide_model->getAllJoinedData(false, 'HomeSlideID', $this->language, "home_slides.IsActive = 1");
        $this->data['view'] = 'index';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function cards()
    {
        $this->data['view'] = 'cards';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function blogs()
    {
        $this->data['blogs'] = $this->Blog_model->getAllJoinedData(false, 'BlogID', $this->language, "blogs.IsActive = 1");
        $this->data['view'] = 'blogs';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function blog_detail()
    {
        $this->data['blog'] = $this->Blog_model->getAllJoinedData(false, 'BlogID', $this->language, "blogs.BlogID = " . $_GET['id'])[0];
        $this->data['view'] = 'blog_detail';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function addBlog()
    {
        $post_data = $this->input->post();
        $save_parent_data = array();
        $save_child_data = array();
        $getSortValue = $this->Blog_model->getLastRow('BlogID');
        $sort = 0;
        if (!empty($getSortValue)) {
            $sort = $getSortValue['SortOrder'] + 1;
        }
        if (isset($_FILES['Image']["name"]) && $_FILES['Image']["name"] != '') {
            $save_parent_data['Image'] = uploadImage('Image', 'uploads/images/');
        }
        $save_parent_data['Name'] = $post_data['Name'];
        $save_parent_data['Email'] = $post_data['Email'];
        $save_parent_data['Mobile'] = $post_data['Mobile'];
        $save_parent_data['Company'] = $post_data['Company'];
        $save_parent_data['Position'] = $post_data['Position'];
        $save_parent_data['AddedFromFrontend'] = 1;
        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = 0;
        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = 0;
        $insert_id = $this->Blog_model->save($save_parent_data);
        if ($insert_id > 0) {
            // $default_lang = getDefaultLanguage();
            $system_languages = getSystemLanguages();
            foreach ($system_languages as $system_language) {
                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['Description'] = $post_data['Description'];
                $save_child_data['BlogID'] = $insert_id;
                $save_child_data['SystemLanguageID'] = $system_language->SystemLanguageID;
                $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = 0;
                $this->Blog_text_model->save($save_child_data);
            }
            $response['message'] = lang('save_successfully');
            $response['reload'] = true;
            echo json_encode($response);
            exit;
        } else {
            $response['message'] = lang('some_thing_went_wrong');
            echo json_encode($response);
            exit;
        }
    }

    public function faq()
    {
        $this->data['faqs_categories'] = $this->Faq_model->getAllJoinedData(false, 'FaqID', $this->language, "faqs.ParentID = 0 AND faqs.IsActive = 1");
        $this->data['view'] = 'faq';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function advertise_with_us()
    {
        $this->data['view'] = 'partner_with_us';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function about_us()
    {
        $this->data['view'] = 'about_us';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function news_events()
    {
        $this->data['news_events'] = $this->News_model->getAllJoinedData(false, 'NewsID', $this->language, "news.IsActive = 1");
        $this->data['view'] = 'news_events';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function news_detail()
    {
        $this->data['news_event'] = $this->News_model->getAllJoinedData(false, 'NewsID', $this->language, "news.NewsID = " . $_GET['id'])[0];
        $this->data['view'] = 'news_detail';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function exclusave_map()
    {
        $this->data['stores'] = $this->Store_model->getAllStores($this->language);
        $this->data['view'] = 'exclusave_map';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function wishlist()
    {
        $this->data['view'] = 'wishlist';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function career()
    {
        $this->data['careers'] = $this->Career_model->getAllJoinedData(false, 'CareerID', $this->language, "careers.IsActive = 1");
        $this->data['view'] = 'career';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function careerApply()
    {
        $post_data = $this->input->post();
        if (isset($_FILES['CV']["name"]) && $_FILES['CV']["name"] != '') {
            $post_data['CV'] = uploadImage('CV', 'uploads/cv/');
        }
        $post_data['CreatedAt'] = date('Y-m-d H:i:s');
        $insert_id = $this->Career_application_model->save($post_data);
        if ($insert_id > 0) {
            $response['message'] = lang('save_successfully');
            $response['reload'] = true;
            echo json_encode($response);
            exit;
        } else {
            $response['message'] = lang('some_thing_went_wrong');
            echo json_encode($response);
            exit;
        }
    }

    public function contact_us()
    {
        $this->data['view'] = 'contact_us';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function partnerWithUsRequest()
    {
        $post_data = $this->input->post();
        $post_data['CreatedAt'] = date('Y-m-d H:i:s');
        $inser_id = $this->Partner_request_model->save($post_data);
        if ($inser_id > 0) {
            $response['message'] = "Your request is saved successfully.";
            $response['reset'] = true;
            echo json_encode($response);
            exit();
        } else {
            $response['message'] = "Something went wrong. Please try again later.";
            echo json_encode($response);
            exit();
        }
    }

    public function forgotPassword()
    {
        if (isset($_GET['verification_token']) && $_GET['verification_token'] != '') {
            $UserID = base64_decode($_GET['verification_token']);
            $user_info = $this->User_model->get($UserID,true, 'UserID');
            if ($user_info) {
                $this->data['UserID'] = $UserID;

               // $this->data['view'] = 'forgot_password';
                //$this->load->view('frontend/layouts/default', $this->data);
                $this->load->view('frontend/forgot_password', $this->data);
            } else {
                echo "Sorry! We couldn't verify you as your verification process failed.";
                exit();
            }
        } else {
            echo "Sorry! We couldn't verify you as your verification process failed.";
            exit();
        }
    }

    public function changePassword()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $password = str_replace(' ', '', $post_data['new_password']);
            $password_length = strlen($password);
            if ($post_data['new_password'] !== $post_data['confirm_password']) {
                $response['status'] = false;
                $response['message'] = "Password and confirm password doesn't match!";
                echo json_encode($response);
                exit();
            } elseif ($password_length < 6) {
                $response['status'] = false;
                $response['message'] = "Password field must have atleast 6 characters!";
                echo json_encode($response);
                exit();
            } elseif ($post_data['new_password'] == $post_data['confirm_password'] && $password_length >= 6) {
                $UserID = base64_decode($post_data['UserID']);
                $user = $this->User_model->get($UserID, true, 'UserID');
                if ($user) {
                    $update['Password'] = md5($post_data['new_password']);
                    $update['OnlineStatus'] = 'Offline';
                    $update_by['UserID'] = $UserID;
                    $this->User_model->update($update, $update_by);
                    $response['status'] = true;
                    $response['message'] = "Your password is updated successfully. You can use this password for login.";
                    echo json_encode($response);
                    exit();
                } else {
                    $response['status'] = false;
                    $response['message'] = "Something went wrong. Please try again later.";
                    echo json_encode($response);
                    exit();
                }
            } else {
                $response['status'] = false;
                $response['message'] = "Something went wrong. Please try again later.";
                echo json_encode($response);
                exit();
            }
        } else {
            $response['status'] = false;
            $response['message'] = "Something went wrong. Please try again later.";
            echo json_encode($response);
            exit();
        }
    }

    /*public function merchant_promocodes($UserID, $CardID)
    {
        $UserCouponColor = 'Orange'; // This is default color set
        if (isset($UserID)) {
            $user = $this->User_model->getUsers("users.UserID = " . $UserID, $this->language)[0];
            if ($user->CouponID > 0) {
                $CouponColor = $this->Coupon_model->get($user->CouponID, false, 'CouponID');
                if ($CouponColor->PackageColorID == 2) {
                    $UserCouponColor = 'Gold';
                } elseif ($CouponColor->PackageColorID == 1) {
                    $UserCouponColor = 'Orange';
                }
            }
        }

        $sponsored_stores = array();
        $today = date('Y-m-d');
        // $where = "stores.ParentID = 0 AND store_cards.CardID = $CardID AND stores.IsSponsored = 1 AND stores.SponsorshipExpiresAt >= '$today'";
        $where = "stores.ParentID = 0 AND store_cards.CardID = $CardID";
        $where .= " AND promotions.EndDate >= '" . date('Y-m-d') . "'";
        $stores = $this->Store_model->getAllStoresWithPromotions($where, $this->language, 'ASC', 0, 0, true);
        if ($stores) {
            $i = 0;
            foreach ($stores as $store) {
                $sponsored_stores[$i] = $store;
                if ($UserCouponColor == 'Gold') {
                    $sponsored_stores[$i]->CategoryImage = $store->GoldImage;
                } elseif ($UserCouponColor == 'Orange') {
                    $sponsored_stores[$i]->CategoryImage = $store->OrangeImage;
                }
                $i++;
            }
        }
        $this->data['sponsored_stores'] = $sponsored_stores;
        // $this->load->view('frontend/sponsored_stores', $this->data);

        $this->data['view'] = 'sponsored_stores';
        $this->load->view('frontend/layouts/default', $this->data);
    }*/

    public function merchant_promocodes()
    {
        $post_data = $this->input->get(); // Email, Password (md5 encrypted)
        $sponsored_stores = array();
        if (!empty($post_data)) {
            $UserCouponColor = 'Orange'; // This is default color set
            $where = "(users.Email = '" . $post_data['Email'] . "' OR users.Mobile = '" . $post_data['Email'] . "') AND users.Password  = '" . $post_data['Password'] . "'";
            $user = $this->User_model->getUserInfo($where);
            if (empty($user)) {
                redirect(base_url());
            }
            if ($user['CouponID'] > 0) {
                $CouponColor = $this->Coupon_model->get($user['CouponID'], false, 'CouponID');
                if ($CouponColor->PackageColorID == 2) {
                    $UserCouponColor = 'Gold';
                } elseif ($CouponColor->PackageColorID == 1) {
                    $UserCouponColor = 'Orange';
                }
            }

            $where = "stores.ParentID = 0 AND stores.StoreType != 'Social Media' AND store_cards.CardID = " . $user['CardID'] . " AND promotions.EndDate >= '" . date('Y-m-d') . "'";
            $stores = $this->Store_model->getAllStoresWithPromotions($where, $this->language, 'ASC', 0, 0, true);
            if ($stores) {
                $i = 0;
                foreach ($stores as $store) {
                    $sponsored_stores[$i] = $store;
                    if ($UserCouponColor == 'Gold') {
                        $sponsored_stores[$i]->CategoryImage = $store->GoldImage;
                    } elseif ($UserCouponColor == 'Orange') {
                        $sponsored_stores[$i]->CategoryImage = $store->OrangeImage;
                    }
                    $i++;
                }
            }
        }

        $this->data['sponsored_stores'] = $sponsored_stores;
        $this->data['view'] = 'sponsored_stores';
        $this->load->view('frontend/layouts/default', $this->data);
    }

}