<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class User extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            'Role_model',
            'User_model',
            'Modules_users_rights_model',
            'Module_model',
            'City_model',
            'District_model'
            


        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        // $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';


    }



    public function index()
    {
        $parent = $this->data['Parent_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
       // $this->data['admins'] = $this->$parent->getAdminUsers();
        //$this->data['store_users'] = $this->$parent->getStoreUsers();
       // $this->data['app_users_not_verified'] = array();//$this->$parent->getUsers("users.RoleID = 2 AND IsVerified = 0", $this->language);
        
       // $this->data['app_users_verified'] = array();//$this->$parent->getUsers("users.RoleID = 2 AND IsVerified = 1", $this->language);
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function ajax_users($role_id = 1)
    {
        $post_data = $this->input->get();
        $col_no = $post_data['order'][0]['column'];
        $sort_by = $post_data['columns'][$col_no]['data'];
        $sort_as = $post_data['order'][0]['dir'];
        $offset = $this->input->get('start');
        $limit = $this->input->get('length');
        $search_text = $post_data['search']['value'];
        $parent = $this->data['Parent_model'];
        $where = 'users.RoleID = '.$role_id;
        

        $this->$parent->limit = $limit;
        $this->$parent->offset = $offset;
        $results = $this->$parent->getAdminUsers($where, $sort_as, $sort_by, $search_text);
        $results_count = $this->$parent->getAdminUsersCount($where, $sort_as, $sort_by, $search_text);
        // echo $this->db->last_query();die();
        $data = array(
            'draw' => $this->input->get('draw'),
            "recordsTotal" => ($results_count),
            "recordsFiltered" => ($results_count),
        );
        $data['data'] = array();
        $i = $offset ? $offset + 1 : 1;
        foreach ($results as $result) {
            

            $actions_html = '';
            if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete') || checkRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanView')) { 

                $actions_html .= '<a href="'.base_url('cms/user/changePassword/' . base64_encode($result->UserID)).'" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Change Password">lock</i><div class="ripple-container"></div></a>';
                if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit')) {
                    $actions_html .= '<a href="'.base_url('cms/user/edit/' . $result->UserID).'"
                                                               class="btn btn-simple btn-warning btn-icon edit"><i
                                                                        class="material-icons" title="Edit">edit</i>
                                                                <div class="ripple-container"></div>
                                                            </a>';
                }

                if(checkRightAccess(23,$this->session->userdata['admin']['RoleID'],'CanDelete')){
                    $actions_html .= '<a href="javascript:void(0);"
                                                               onclick="deleteRecord(\''.$result->UserID.'\',\'cms/user/action\',\'\')"
                                                               class="btn btn-simple btn-danger btn-icon remove"><i
                                                                        class="material-icons" title="Delete">close</i>
                                                                <div class="ripple-container"></div>
                                                            </a>';
                }

                

            }

            $data['data'][] = array(
                'User Name' => $result->FirstName.' '.$result->MiddleName.' '.$result->LastName,
                'Email' => $result->Email,
                'Mobile' => $result->Mobile,
                'Publish' => ($result->IsActive ? lang('yes') : lang('no')),
                'Action' => $actions_html
            );
            $i++;
        }
        echo json_encode($data);
        exit();
    }





    public function add()
    {
        if (!checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';
        $this->data['roles'] = $this->Role_model->getAllJoinedData(false, 'RoleID', $this->language, 'roles.IsActive = 1');
        if (!$this->data['roles']) {
            $this->session->set_flashdata('message', lang('please_add_role_first'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        //$this->data['cities'] = $this->City_model->getAllJoinedData(false, 'CityID', $this->language, 'cities.IsActive = 1');


        $this->load->view('backend/layouts/default', $this->data);
    }

    public function view($id = '')
    {
        if (!checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanView')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $id = base64_decode($id);
        $parent = $this->data['Parent_model'];
        $this->data['user'] = $this->$parent->getUsers($this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id);
        if (empty($this->data['user'])) {
            $this->session->set_flashdata('message', lang('some_thing_went_wrong'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['user'] = $this->data['user'][0];
        // dump($this->data['user']);
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/view';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->get($id, false, 'UserID');


        if (!$this->data['result']) {
            $this->session->set_flashdata('message', lang('some_thing_went_wrong'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }


        $this->data['roles'] = $this->Role_model->getAllJoinedData(false, 'RoleID', $this->language, 'roles.IsActive = 1');
        if (!$this->data['roles']) {
            $this->session->set_flashdata('message', lang('please_add_role_first'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        //$this->data['cities'] = $this->City_model->getAllJoinedData(false, 'CityID', $this->language, 'cities.IsActive = 1');
       

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';

        $this->load->view('backend/layouts/default', $this->data);

    }


    public function rights($user_id = '')
    {

        if (!checkRightAccess(23, $this->session->userdata['admin']['checkRightAccess'], 'CanEdit') && !checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        if ($user_id == '') {
            $user_id = $this->session->userdata['admin']['UserID'];
        }
        $parent = $this->data['Parent_model'];
        // $child = $this->data['Child_model'];
        $this->data['UserID'] = $user_id;
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/rights';
        $this->data['results'] = $this->Modules_users_rights_model->getModulesWithRights($user_id, $this->language);


        if (empty($this->data['results'])) {

            $this->session->set_flashdata('message', lang('please_add_module_first'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['users'] = $this->User_model->getAll(false);

        if (empty($this->data['users'])) {

            $this->session->set_flashdata('message', lang('please_add_user_first'));
            redirect(base_url('cms/user'));
        }


        $this->load->view('backend/layouts/default', $this->data);


    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                //$this->validate();
                $this->delete();
                break;

            case 'save_rights':
                $this->saveRights();
                break;
            case 'update_password':
                $this->UpdatePassword();
             break;

        }
    }

    private function UpdatePassword()
    {
       
        $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[ConfirmPassword]');
        $this->form_validation->set_rules('ConfirmPassword', lang('confirm_password'), 'required');
       


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
                $post_data = $this->input->post();
                $parent                             = $this->data['Parent_model'];
               
                if(isset($post_data[$this->data['TableKey']])){
                    $id = base64_decode($post_data[$this->data['TableKey']]);
                    $user = $this->$parent->get($id,false,'UserID');

                        if(!$user){
                    
                           $errors['error'] =  lang('some_thing_went_wrong');
                           $errors['success'] =   false;
                           $success['redirect'] = true;
                           $success['url'] = 'cms/'.$this->router->fetch_class();
                           echo json_encode($errors);
                           exit;
                        }


                    
                   

                     
                
            unset($post_data['form_type']);
            unset($post_data['ConfirmPassword']);
            $save_parent_data                   = array();
            $save_parent_data['Password']         = md5($post_data['Password']);
            $update_by  = array();
            $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
             
             $this->User_model->update($save_parent_data,$update_by);       
                     
        
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;  
   

        
    }else{
           
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $success['redirect'] = true;
            $success['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }


    private function validate($check = true)
    {

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('RoleID', lang('role'), 'required');
        $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email|is_unique[users.Email]');
        // $this->form_validation->set_rules('Mobile', 'Mobile', 'required|is_unique[users.Mobile]');
        $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[ConfirmPassword]');
        $this->form_validation->set_rules('ConfirmPassword', lang('confirm_password'), 'required');
        $this->form_validation->set_rules('Mobile', 'Mobile No.', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }


    private function save()
    {

        if (!checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        //$child = $this->data['Child_model'];
        $save_parent_data = array();
        //$save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }

        //$save_parent_data['CityID'] = $post_data['CityID'];


        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
        $save_parent_data['RoleID'] = $post_data['RoleID'];
        $save_parent_data['Password'] = md5($post_data['Password']);
        $save_parent_data['Email'] = $post_data['Email'];
        $save_parent_data['Mobile'] = $post_data['Mobile'];
        $save_parent_data['FirstName'] = $post_data['FirstName'];
        $save_parent_data['MiddleName'] = $post_data['MiddleName'];
        $save_parent_data['LastName'] = $post_data['LastName'];
        

        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {

            


            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        if (!checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];

        if($post_data['Email'] != $post_data['EmailHidden']){
             $errors = array();
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            
            $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email|is_unique[users.Email]');
            

            if ($this->form_validation->run() == FALSE) {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
            } 

        }
        //$child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->get($id, false, 'UserID');


            if (!$this->data['result']) {

                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $success['redirect'] = true;
                $success['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }


            unset($post_data['form_type']);
            $save_parent_data = array();
            $save_child_data = array();
            $save_parent_data['Email'] = $post_data['Email'];
            $save_parent_data['Mobile'] = $post_data['Mobile'];
            $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
            $save_parent_data['RoleID'] = $post_data['RoleID'];
            $save_parent_data['FirstName'] = $post_data['FirstName'];
            $save_parent_data['MiddleName'] = $post_data['MiddleName'];
            $save_parent_data['LastName'] = $post_data['LastName'];
            

            $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

            $update_by = array();
            $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);


            $this->$parent->update($save_parent_data, $update_by);

            $success['error'] = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;


        } else {

            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    private function saveRights()
    {


        $parent = $this->data['Parent_model'];
        //  $child = $this->data['Child_model'];
        $user_id = $this->input->post('UserID');

        $modules = $this->Modules_users_rights_model->getModulesWithRights($user_id, $this->language);

        $can_view = $this->input->post('CanView');

        $can_add = $this->input->post('CanAdd');
        $can_edit = $this->input->post('CanEdit');
        $can_delete = $this->input->post('CanDelete');

        if (!empty($modules)) {

            foreach ($modules as $module) {

                $update_data[] = [
                    'ModuleRightID' => $module['ModuleRightID'],
                    'CanView' => (isset($can_view[$module['ModuleRightID']]) ? 1 : 0),
                    'CanAdd' => (isset($can_add[$module['ModuleRightID']]) ? 1 : 0),
                    'CanEdit' => (isset($can_edit[$module['ModuleRightID']]) ? 1 : 0),
                    'CanDelete' => (isset($can_delete[$module['ModuleRightID']]) ? 1 : 0),
                    'UpdatedAt' => date('Y-m-d H:i:s'),
                    'UpdatedBy' => $this->session->userdata['admin']['UserID']

                ];
            }

            $this->Modules_users_rights_model->update_batch($update_data, 'ModuleRightID');
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['reload'] = true;

            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }


    }


    private function delete()
    {

        if (!checkUserRightAccess(23, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $parent = $this->data['Parent_model'];
        // $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');

        $this->Modules_users_rights_model->delete($deleted_by);

        // $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    public function changePassword($UserID)
    {
        
       
        $parent                             = $this->data['Parent_model'];

        $this->data['result']           = $this->$parent->get(base64_decode($UserID),false,'UserID');


        $this->data['UserID'] = $UserID;

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/changePassword';
        
        
        $this->load->view('backend/layouts/default',$this->data);
    }

    public function sendVerifeSMS($Mobile, $random_password)
    {
        $para['msg'] = "كلمة المرور لاكسكلوسيف هي  $random_password Your ExcluSave password is $random_password";
        $withOutSpaceMobileNumer = str_replace(' ', '', $Mobile);
        $para['msisdn'] = $withOutSpaceMobileNumer;
        sms($para);
    }

    public function updateIsVerified()
    {
        $random_password = random_password(8,false,true);
        $post_data = $this->input->post();
        $update['IsVerified'] = $post_data['IsVerified'];
        $update['Password'] = md5($random_password);
        $update_by['UserID'] = $post_data['UserID'];
       
        $this->User_model->update($update, $update_by);
        $this->sendPasswordToUser($post_data['UserID'], $random_password);
        sendNotification('ExcluSave', 'Your account is verified.', array(), $post_data['UserID']);
       
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit;
    }

    public function updateIsActive()
    {
        $post_data = $this->input->post();
        $update['IsActive'] = $post_data['IsActive'];
        $update_by['UserID'] = $post_data['UserID'];
        $this->User_model->update($update, $update_by);
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit;
    }

    private function sendPasswordToUser($UserID, $Password)
    {
        $user_info = $this->User_model->get($UserID, true, 'UserID');
        $this->sendVerifeSMS($user_info['Mobile'], $Password);
        $email_template = get_email_template(8, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{email}}", $user_info['Email'], $message);
        $message = str_replace("{{password}}", $Password, $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);

       

    }

}