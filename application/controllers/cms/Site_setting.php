<?php

defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('UTC');

class Site_setting extends Base_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Site_setting_model');
        
        $this->data['language'] = $this->language;
    }

    public function index() {
        $this->edit(1);
    }
    
    public function edit($SiteSettingID) {

        $this->data['result'] = $this->Site_setting_model->get($SiteSettingID, false, 'SiteSettingID');

        if (!$this->data['result']) {
            redirect(base_url('cms/user'));
        }
        $this->data['view'] = 'backend/site_setting/edit';


        $this->data['SiteSettingID'] = $SiteSettingID;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action() {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {

            case 'update';
                $this->validate();
                $this->update();
                break;
        }
    }

    private function validate() {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('SiteName', lang('SiteName'), 'required');
        $this->form_validation->set_rules('Email', lang('email'), 'valid_email');




        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function update() {
        $post_data = $this->input->post();
        
        $this->data['result'] = $this->Site_setting_model->get(1, false, 'SiteSettingID');
        
        $post_data['UpdatedAt'] = date('Y-m-d H:i:s');

        unset($post_data['form_type']);
        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            unlink($this->data['result']->SiteImage);
            $post_data['SiteImage'] = $this->uploadImage("Image", "uploads/");
        }

        if (isset($post_data['OpenTime']) && $post_data['OpenTime'] != '')
        {
            $open_time = date('Y-m-d').' '.$post_data['OpenTime'];
            $post_data['OpenTime'] = strtotime($open_time);
        }

        if (isset($post_data['CloseTime']) && $post_data['CloseTime'] != '')
        {
            $close_time = date('Y-m-d').' '.$post_data['CloseTime'];
            $post_data['CloseTime'] = strtotime($close_time);
        }

        // dump($post_data);

        if ($post_data['OpenTime'] != '' && $post_data['CloseTime'] != '' && $post_data['CloseTime'] <= $post_data['OpenTime'])
        {
            $errors['error'] = 'Open time must be less than close time';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }

        // echo date_default_timezone_get();exit();

        $update_by['SiteSettingID'] = $post_data['SiteSettingID'];
        unset($post_data['form_type']);
        $this->Site_setting_model->update($post_data, $update_by);

        $success['error'] = false;
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/Site_setting';
        echo json_encode($success);
        exit;
    }

    
}
