<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends Base_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        checkAdminSession();
        $this->load->Model([
            'Question_model',
            'Question_image_model',
            'Question_comment_model',
            'Question_reported_model'
        ]);

        $this->data['language'] = $this->language;
    }

    public function index()
    {
        if (!checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['view'] = 'backend/question/manage';
        $this->data['results'] = $this->Question_model->getQuestions(false, $this->language);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function view($QuestionID)
    {
        if (!checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['view'] = 'backend/question/view';
        $question = $this->Question_model->getQuestions("questions.QuestionID = ".$QuestionID, $this->language);
        if (empty($question)) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $question = (object)$question[0];
        $question_images = $this->Question_image_model->getMultipleRows(array('QuestionID' => $QuestionID));
        $comments_count = $this->Question_comment_model->getCountComments($QuestionID);
        $reports_count = $this->Question_reported_model->getCountReports($QuestionID);

        $comments = $this->Question_comment_model->getComments($QuestionID);
        $reports = $this->Question_reported_model->getReports($QuestionID);

        $question->QuestionImages = $question_images ? $question_images : array();
        $question->CommentCount = $comments_count->Total;
        $question->ReportCount = $reports_count->Total;
        $question->QuestionComments = $comments ? $comments : array();
        $question->QuestionReports = $reports ? $reports : array();
        $this->data['question'] = $question;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function delete()
    {
        if (!checkUserRightAccess(58, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }

        $deleted_by['QuestionID'] = $this->input->post('id');
        $this->Question_image_model->delete($deleted_by);
        $this->Question_comment_model->delete($deleted_by);
        $this->Question_reported_model->delete($deleted_by);
        $this->Question_model->delete($deleted_by);

        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


}
