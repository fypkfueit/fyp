<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('User_model');
        // $this->load->Model('User_text_model');
        $this->load->Model('City_model');
        $this->load->Model('Notification_model');
        $this->load->Model('Role_model');
        //$this->load->Model('Coupon_model');
       // $this->load->Model('User_notification_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';
    }

    public function index()
    {
        $this->data['roles'] = $this->Role_model->getAllJoinedData(false, 'RoleID', $this->language, 'roles.IsActive = 1');
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['coupons'] = $this->Coupon_model->getAll(false);
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function all()
    {
        $this->data['notifications'] = $this->Notification_model->getMultipleRows(array('NotificationTo' => $this->session->userdata['admin']['UserID']));
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/all';
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function view($NotificationID = '')
    {
        if($NotificationID != ''){

            $notification_data = $this->Notification_model->get($NotificationID,false,'NotificationID');
            if($notification_data && $notification_data->NotificationTo = $this->session->userdata['admin']['UserID']){
                $this->Notification_model->update(array('IsRead' => 1),array('NotificationID' => $NotificationID));
                //print_rm($notification_data);
                if($notification_data->FypID > 0){
                    redirect(base_url('cms/fyp/view/'.$notification_data->FypID));
                }else{
                    redirect(base_url('cms/notfication/all'));//change later
                }

            }else{
                redirect(base_url('cms/notfication/all'));
            }

            $this->data['users'] = $this->Notification_model->getNotificationsWithUsers('notifications.NotificationID = '.$NotificationID);
            $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/user_detail';
            $this->load->view('backend/layouts/default', $this->data);

        }else{
            redirect(base_url('cms/notfication/all'));
        }
        
    }


    public function detail($NotificationID = '')
    {
        if($NotificationID != ''){

            $this->data['users'] = $this->Notification_model->getNotificationsWithUsers('notifications.NotificationID = '.$NotificationID);
            $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/user_detail';
            $this->load->view('backend/layouts/default', $this->data);

        }else{
            redirect(base_url('cms/notfication/all'));
        }
        
    }

    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', 'Title', 'required');
        $this->form_validation->set_rules('Description', 'Description', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    public function sendNotification()
    {
        $post_data = $this->input->post();
        $notification_type = $post_data['NotificationType'];
        $RoleID = $post_data['RoleID'];
        $CouponIDs = array();
        if ($RoleID == 2) { // If RoleID is coming as App User
            if (isset($post_data['CouponID'])) {
                $CouponIDs = $post_data['CouponID'];
                if(!empty($post_data['CouponID'])){
                    unset($post_data['RoleID']);
                }
                $post_data['CouponID'] = implode(',',$post_data['CouponID']);
            }
        }
        $title = $post_data['Title'];
        $description = $post_data['Description'];
        $data = array();
        $users = $this->User_model->getUsersWhereCouponIn($RoleID, $CouponIDs);
        //echo $this->db->last_query();
        //print_rm($users);
        $this->validate();
        if ($users) {
            $post_data['CreatedAt'] = time();
            $post_data['NotificationFrom'] = $this->session->userdata['admin']['UserID'];
            $other_data = array();
            $notification_id = $this->Notification_model->save($post_data);
            foreach ($users as $user) {
                $notification_status = 'Done';
                if ($notification_type == 'PushNotification') {
                    $res = sendNotification($title, $description, $data, $user['UserID']);

                    if($res == 'Device token not found!'){
                        $notification_status = 'Not Sent,Invalid Token';
                    }
                } elseif ($notification_type == 'Email') {
                    $data['to'] = $user['Email'];
                    $data['subject'] = $title;
                    $data['message'] = email_format($description);
                    sendEmail($data);
                } elseif ($notification_type == 'SMS') {
                    $sms_text = $title . "\n" . $description;
                    sendSms($user['Mobile'], $sms_text);
                }



                $other_data[] = [
                    'NotificationID' => $notification_id,
                    'UserID' => $user['UserID'],
                    'NotificationStatus' => $notification_status,
                    'CreatedAt' => date('Y-m-d H:i:s')
                ];
            }


             $this->User_notification_model->insert_batch($other_data);
        }

        $success['error'] = false;
        $success['success'] = 'Notification sent successfully';
        $success['redirect'] = false;
        echo json_encode($success);
        exit;
    }

}