<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            'Department_model',
            'Cls_model'
        ]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['TableKey'] = 'ProgramID';
                $this->data['Table'] = 'programs';
       
        
    }
     
    
    public function index($DepartmentID = '')
    {
          
         if($DepartmentID == ''){
            redirect(base_url('cms/department'));
         }
          $parent                             = $this->data['Parent_model'];
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        
          $this->data['results'] = $this->$parent->getMultipleRows(array('DepartmentID' => $DepartmentID));
          $this->data['department_data'] = $this->Department_model->get($DepartmentID,false,'DepartmentID');

          $this->data['DepartmentID'] = $DepartmentID;
          
          $this->load->view('backend/layouts/default',$this->data);
    }

    public function getClasses(){
        $ProgramID = $this->input->post('ProgramID');
        $results = $this->Cls_model->getMultipleRows(array('ProgramID' => $ProgramID));
       // echo $this->db->last_query();
       // print_rm($results);exit;

        $options = '<option value="">Select Class</option>';
        if($results){
            foreach ($results as $key => $value) {
               $options .= '<option value="'.$value->ClassID.'">'.$value->Title.'</option>';
            }
        }


       
        $response['success'] = true;
        $response['html'] = $options;

        echo json_encode($response);
        exit();
    }
    public function add($DepartmentID = '')
    {
         if($DepartmentID == ''){
            redirect(base_url('cms/department'));
         }

        if(!checkRightAccess(74,$this->session->userdata['admin']['RoleID'],'CanAdd')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        $this->data['department_data'] = $this->Department_model->get($DepartmentID,false,'DepartmentID');
        $this->data['DepartmentID'] = $DepartmentID;

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
       
        
        
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($id = '')
    {
        if(!checkRightAccess(74,$this->session->userdata['admin']['RoleID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        $this->data['result']          = $this->$parent->get($id,false,$this->data['TableKey']);
    
        
        if(!$this->data['result']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
        $this->data['department_data'] = $this->Department_model->get($this->data['result']->DepartmentID,false,'DepartmentID');
       
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->data[$this->data['TableKey']]   = $id;
    $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->update();
          break;
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required');
      
        



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
        
        if(!checkRightAccess(74,$this->session->userdata['admin']['RoleID'],'CanAdd')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        
        $save_parent_data                   = array();
        
       
        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           
            $sort = $getSortValue['SortOrder'] + 1;
        }
       
        
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['Title']                        = $post_data['Title'];
        $save_parent_data['DepartmentID']                        = $post_data['DepartmentID'];


        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');        
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];


        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {
                
            
            
              
                $success['error']   = false;
                $success['success'] = lang('save_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
    }
    
        private function update()
    {
        


                if(!checkRightAccess(74,$this->session->userdata['admin']['RoleID'],'CanEdit')){
                $errors['error'] =  lang('you_dont_have_its_access');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/'.$this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }
                $post_data = $this->input->post();
                $parent                             = $this->data['Parent_model'];
               
                if(isset($post_data[$this->data['TableKey']])){
                    $id = base64_decode($post_data[$this->data['TableKey']]);
                    $this->data['result']          = $this->$parent->get($id,false,$this->data['TableKey']);
    
        
                if(!$this->data['result']){
                   $errors['error'] =  lang('some_thing_went_wrong');
                   $errors['success'] =   false;
                   $errors['redirect'] = true;
                   $errors['url'] = 'cms/'.$this->router->fetch_class();
                   echo json_encode($errors);
                   exit;
                }

            
                
            unset($post_data['form_type']);
        $save_parent_data                   = array();
               
                
                    $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
                    $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');     
                    $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                     $save_parent_data['Title']                        = $post_data['Title'];
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    
                    
                    
                    
                    $this->$parent->update($save_parent_data,$update_by);
                    
                    
                
        
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;  
                
                
              
              
           
        
        


        
    }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    
    
    
    
   
    
    private function delete(){
        
         if(!checkRightAccess(74,$this->session->userdata['admin']['RoleID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];
        
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        
        $this->$parent->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    
    
    

}