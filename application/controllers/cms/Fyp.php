<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fyp extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            'Program_model',
            'Department_model',
            'Cls_model',
            'Domain_model',
            'Session_model',
            'User_model',
            'Notification_model'
        ]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['TableKey'] = 'FypID';
                $this->data['Table'] = 'fyps';
       
        
    }
     
    
    public function index()
    {
          $parent                             = $this->data['Parent_model'];
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';

          $where = false;

          if($this->session->userdata['admin']['RoleID'] == 3){
            $where = 'fyps.IsRequestSentToSupervisor = 1 AND fyps.SupervisorID = '.$this->session->userdata['admin']['UserID'];
          }else if($this->session->userdata['admin']['RoleID'] == 4){
            $where = 'fyps.StudentID = '.$this->session->userdata['admin']['UserID'].' OR (fyps.PartnerID = '.$this->session->userdata['admin']['UserID'].' AND fyps.IsApprovedByPartner != "Rejected")';
          }
        
          $this->data['results'] = $this->$parent->getAllFyps($where);
          
          $this->load->view('backend/layouts/default',$this->data);
    }
    public function add()
    {
        $parent                             = $this->data['Parent_model'];
        $this->checkAlreadyRegister(false,$this->session->userdata['admin']['UserID']);


        if(!checkRightAccess(78,$this->session->userdata['admin']['RoleID'],'CanAdd')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        

        $this->data['departments'] = $this->Department_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0));
        $this->data['domains'] = $this->Domain_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0));
        $this->data['sessions'] = $this->Session_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0));
        $this->data['supervisors'] = $this->User_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0,'RoleID' => 3));
        $this->data['students'] = $this->User_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0,'RoleID' => 4));
        

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($id = '')
    {
        
        $parent                             = $this->data['Parent_model'];
        $this->data['result']          = $this->$parent->getFypByID('fyps.FypID = '.$id);
        if(!$this->data['result']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }

        if($this->data['result']['IsApprovedBySupervisor'] == 1){
            redirect(base_url('cms/'.$this->router->fetch_class()));
        }

        if($this->session->userdata['admin']['RoleID'] == 4){
            if($this->data['result']['StudentID'] != $this->session->userdata['admin']['UserID'] || $this->data['result']['StudentCanEdit'] != 1){
                redirect(base_url('cms/'.$this->router->fetch_class())); 
            }
        }

        //print_rm($this->data['result']);

        
        $this->data['departments'] = $this->Department_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0));
        $this->data['programs'] = $this->Program_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0,'DepartmentID' => $this->data['result']['DepartmentID']));
        $this->data['classes'] = $this->Cls_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0,'ProgramID' => $this->data['result']['ProgramID']));
        $this->data['domains'] = $this->Domain_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0));
        $this->data['sessions'] = $this->Session_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0));
        $this->data['supervisors'] = $this->User_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0,'RoleID' => 3));
        $this->data['students'] = $this->User_model->getMultipleRows(array('IsActive' => 1, 'Hide' => 0,'RoleID' => 4));
        
        
        
       
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->data[$this->data['TableKey']]   = $id;
        $this->load->view('backend/layouts/default',$this->data);
        
    }


    public function view($id = '')
    {
        /*if(!checkRightAccess(78,$this->session->userdata['admin']['RoleID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }*/
        $parent                             = $this->data['Parent_model'];
        $this->data['result']          = $this->$parent->getFypByID('fyps.FypID = '.$id);
       // print_rm($this->data['result']);
        
        if(!$this->data['result']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }

        if($this->session->userdata['admin']['RoleID'] == 4){
            if($this->data['result']['StudentID'] != $this->session->userdata['admin']['UserID'] && $this->data['result']['PartnerID'] != $this->session->userdata['admin']['UserID']){
                redirect(base_url('cms/'.$this->router->fetch_class())); 
            }
        }elseif ($this->session->userdata['admin']['RoleID'] == 3) {
            if($this->data['result']['SupervisorID'] != $this->session->userdata['admin']['UserID']){
                redirect(base_url('cms/'.$this->router->fetch_class())); 
            }
        }
        
       
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/view';
        $this->data[$this->data['TableKey']]   = $id;
        $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->validate();
                $this->update();
          break;
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
        $post_data = $this->input->post();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('DepartmentID', 'Department', 'required');
        $this->form_validation->set_rules('ProgramID', 'Program', 'required');
        $this->form_validation->set_rules('ClassID', 'Class', 'required');
        $this->form_validation->set_rules('SessionID', 'Session', 'required');
        $this->form_validation->set_rules('RegistrationNumber', 'Registration Number', 'required');
        $this->form_validation->set_rules('Tool', 'Tools', 'required');
        $this->form_validation->set_rules('DomainID[]', 'Domains', 'required');
        $this->form_validation->set_rules('SupervisorID', 'Supervisor', 'required');
        $this->form_validation->set_rules('Title', 'Project Title', 'required');
        $this->form_validation->set_rules('ProjectSummary', 'Project Summary', 'required');
      
        if(isset($post_data['HasPartner'])){
            $this->form_validation->set_rules('PartnerRegistrationNumber', 'Partner Registration Number', 'required');
        }



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }

    private function validateRejection(){
        $errors = array();
        $post_data = $this->input->post();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('RejectReasonBySupervisor', 'Reject Reason', 'required');
        


        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }

    private function checkAlreadyRegister($ajax = true,$StudentID){
        $parent                             = $this->data['Parent_model'];
        if($this->session->userdata['admin']['RoleID'] == 4){
            $already_sent = $this->$parent->get($StudentID,false,'StudentID');
            if($already_sent ){
               // $this->session->set_flashdata('message','You already sent FYP registration request.');
                if($ajax){
                    $errors['error'] =  'You already sent FYP registration request.';
                    $errors['success'] = false;
                    $errors['redirect'] = true;
                    $errors['url'] = 'cms/'.$this->router->fetch_class();
                    echo json_encode($errors);
                    exit;
                }else{
                    $this->session->set_flashdata('message','You already sent FYP registration request.');
                    redirect(base_url('cms/'.$this->router->fetch_class())); 
                }
                
            }

            $already_partner = $this->$parent->getAllFyps('fyps.PartnerID = '.$StudentID.' AND (fyps.IsApprovedByPartner = "Approved" OR fyps.IsApprovedByPartner is NULL OR fyps.IsApprovedByPartner = "Pending")');
            if($already_partner ){
                //$this->session->set_flashdata('message','You already a partner with someone.');
                if($ajax){
                    $errors['error'] =  'You already a partner with someone.';
                    $errors['success'] = false;
                    $errors['redirect'] = true;
                    $errors['url'] = 'cms/'.$this->router->fetch_class();
                    echo json_encode($errors);
                    exit;
                 }else{
                    $this->session->set_flashdata('message','You already a partner with someone.');
                    redirect(base_url('cms/'.$this->router->fetch_class())); 
                 }
            }

        }
    }
    
    private function save()
    {
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        
        if(!checkRightAccess(78,$this->session->userdata['admin']['RoleID'],'CanAdd')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $student_data = $this->User_model->get($post_data['RegistrationNumber'],false,'RegistrationNumber');
        if(!$student_data){
            $errors['error'] =  'Invalid registration number.';
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
        $student_notification_data = array();
        $student_notification_data['UserID'] = $student_data->UserID;
        $student_notification_data['FirstName'] = $student_data->FirstName;
        $student_notification_data['LastName'] = $student_data->LastName;
        $student_notification_data['RegistrationNumber'] = $student_data->RegistrationNumber;
        $student_notification_data['Email'] = $student_data->Email;

        $this->checkAlreadyRegister(true,$student_data->UserID);
        
        
        $save_parent_data                   = array();
        
       
        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           
            $sort = $getSortValue['SortOrder'] + 1;
        }
       
        
        $save_parent_data['SortOrder']                  = $sort;
        $save_parent_data['IsActive']                   = 0;
        $save_parent_data['Title']                      = $post_data['Title'];
        $save_parent_data['ProjectSummary']             = $post_data['ProjectSummary'];
        $save_parent_data['DepartmentID']               = $post_data['DepartmentID'];
        $save_parent_data['ProgramID']                  = $post_data['ProgramID'];
        $save_parent_data['ClassID']                    = $post_data['ClassID'];
        $save_parent_data['SessionID']                  = $post_data['SessionID'];
        $save_parent_data['RegistrationNumber']         = $post_data['RegistrationNumber'];
        $save_parent_data['Tool']                       = $post_data['Tool'];
        $save_parent_data['DomainID']                   = implode(',',$post_data['DomainID']);
        $save_parent_data['SupervisorID']               = $post_data['SupervisorID'];
        $save_parent_data['StudentID']                  = $this->session->userdata['admin']['UserID'];
        $save_parent_data['IsDataBaseSubjectPassed']       = (isset($post_data['IsDataBaseSubjectPassed']) ? 1 : 0 );
        $save_parent_data['IsSoftwareEngineeringSubjectPassed']       = (isset($post_data['IsSoftwareEngineeringSubjectPassed']) ? 1 : 0 );

         $send_partner_email = false;

        if(isset($post_data['HasPartner']) && $post_data['HasPartner'] == 1){



            $partner_data = $this->User_model->get($post_data['PartnerRegistrationNumber'],false,'RegistrationNumber');
            if($partner_data){
                $save_parent_data['PartnerID'] = $partner_data->UserID;
                $send_partner_email = true;
                $partner_notification_data = array();
                $partner_notification_data['UserID'] = $student_data->UserID;
                $partner_notification_data['FirstName'] = $student_data->FirstName;
                $partner_notification_data['LastName'] = $student_data->LastName;
                $partner_notification_data['RegistrationNumber'] = $student_data->RegistrationNumber;
                $partner_notification_data['Email'] = $student_data->Email;

            }else{
                $errors['error'] =  'Partner does not exist.';
                $errors['success'] = false;
                echo json_encode($errors);
                exit;

            }

        }else{
            $save_parent_data['IsRequestSentToSupervisor'] = 1;
        }




        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');        
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];


        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {
                $message = 'submitted successfully';
                
                if($send_partner_email){
                    $send_partner_request = $this->sendPartnerRequest($student_notification_data,$partner_notification_data,$insert_id);
                    $message = 'FYP registration form is submitted and your request is sent to partner once he/she approve then supervior can view this form';
                }


                if(!$send_partner_email){// send request if partner approved request in this case there is no partner

                    $supervisor_data = $this->User_model->get($post_data['SupervisorID'],false,'UserID');
                    $supervisor_notification_data = array();
                    $supervisor_notification_data['UserID'] = $supervisor_data->UserID;
                    $supervisor_notification_data['FirstName'] = $supervisor_data->FirstName;
                    $supervisor_notification_data['LastName'] = $supervisor_data->FirstName;
                    $supervisor_notification_data['Email'] = $supervisor_data->Email;

                    $this->sendSupervisorRequest($student_notification_data,$supervisor_notification_data,$insert_id);
                    $message = 'FYP registration form is submitted. Wait for supervisor approval.';

                }
                
            
                
                $success['error']   = false;
                $success['success'] = $message;
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
    }


    private function sendPartnerRequest($student_data,$partner_data,$FypID){
        
        $email_template = get_email_template(5);//5 id for partner request


        $subject = $email_template->Heading;
        $message = $email_template->Description;

        
        $message = str_replace("{{partner_name}}", $partner_data['FirstName'].' '.$partner_data['LastName'], $message);
        $message = str_replace("{{student_name}}", $student_data['FirstName'].' '.$student_data['LastName'], $message);
        $message = str_replace("{{student_registration_number}}", $student_data['RegistrationNumber'], $message);
        
        $data['to'] = $partner_data['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        if (!in_array($_SERVER['HTTP_HOST'], ['localhost','localhost:8888','localhost:8080', 'projects.local', '127.0.0.1', '127.0.0.1:8080'])){
            sendEmail($data);//not sending email on localhost
        } 


        $notification_data = array();
        $notification_data['NotificationFrom']      = $student_data['UserID'];
        $notification_data['NotificationTo']        = $partner_data['UserID'];
        $notification_data['FypID']                 = $FypID;
        $notification_data['Title']                 = 'FYP Request';
        $notification_data['Description']           = 'You have a request for FYP registration';
        $notification_data['NotificationType']      = 'partner_request_notification';
        $notification_data['IsRead']                = 0;
        $notification_data['CreatedAt']             = time();

        $this->Notification_model->save($notification_data);

        



    }

    private function sendSupervisorRequest($student_data,$supervisor_data,$FypID){
        
        $email_template = get_email_template(6);//6 id for supervisor approval request


        $subject = $email_template->Heading;
        $message = $email_template->Description;

        
        $message = str_replace("{{supervisor_name}}", $supervisor_data['FirstName'].' '.$supervisor_data['LastName'], $message);
        $message = str_replace("{{student_name}}", $student_data['FirstName'].' '.$student_data['LastName'], $message);
        $message = str_replace("{{student_registration_number}}", $student_data['RegistrationNumber'], $message);
        
        $data['to'] = $supervisor_data['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        if (!in_array($_SERVER['HTTP_HOST'], ['localhost','localhost:8888','localhost:8080', 'projects.local', '127.0.0.1', '127.0.0.1:8080'])){
            sendEmail($data);//not sending email on localhost
        } 


        $notification_data = array();
        $notification_data['NotificationFrom']      = $student_data['UserID'];
        $notification_data['NotificationTo']        = $supervisor_data['UserID'];
        $notification_data['FypID']                 = $FypID;
        $notification_data['Title']                 = 'FYP Request';
        $notification_data['Description']           = 'You have a request for FYP registration approval.';
        $notification_data['NotificationType']      = 'supervisor_fyp_notification';
        $notification_data['IsRead']                = 0;
        $notification_data['CreatedAt']             = time();

        $this->Notification_model->save($notification_data);

        



    }
    
    private function update()
    {
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $FypID = base64_decode($post_data['FypID']);
        $result         = $this->$parent->getFypByID('fyps.FypID = '.$FypID);
        if($this->session->userdata['admin']['RoleID'] == 3 || $this->session->userdata['admin']['RoleID'] == 4){

            if($this->session->userdata['admin']['RoleID'] == 4){
                if($result['StudentCanEdit'] != 1 && ($this->session->userdata['admin']['UserID'] != $result['StudentID'] || $this->session->userdata['admin']['UserID'] != $result['PartnerID'])){
                    $errors['error'] =  lang('you_dont_have_its_access');
                    $errors['success'] = false;
                    $errors['redirect'] = true;
                    $errors['url'] = 'cms/'.$this->router->fetch_class();
                    echo json_encode($errors);
                    exit;
                }
            }


            if($this->session->userdata['admin']['RoleID'] == 3){
                if($this->session->userdata['admin']['UserID'] != $result['SupervisorID']){
                    $errors['error'] =  lang('you_dont_have_its_access');
                    $errors['success'] = false;
                    $errors['redirect'] = true;
                    $errors['url'] = 'cms/'.$this->router->fetch_class();
                    echo json_encode($errors);
                    exit;
                }
            }
            

        }
        

        $student_data = $this->User_model->get($post_data['RegistrationNumber'],false,'RegistrationNumber');
        if(!$student_data){
            $errors['error'] =  'Invalid registration number.';
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
        $student_notification_data = array();
        $student_notification_data['UserID'] = $student_data->UserID;
        $student_notification_data['FirstName'] = $student_data->FirstName;
        $student_notification_data['LastName'] = $student_data->LastName;
        $student_notification_data['RegistrationNumber'] = $student_data->RegistrationNumber;
        $student_notification_data['Email'] = $student_data->Email;

        //$this->checkAlreadyRegister(true,$student_data->UserID);
        
        
        $save_parent_data                   = array();
        
       
        
        $save_parent_data['Title']                      = $post_data['Title'];
        $save_parent_data['ProjectSummary']             = $post_data['ProjectSummary'];
        $save_parent_data['DepartmentID']               = $post_data['DepartmentID'];
        $save_parent_data['ProgramID']                  = $post_data['ProgramID'];
        $save_parent_data['ClassID']                    = $post_data['ClassID'];
        $save_parent_data['SessionID']                  = $post_data['SessionID'];
        $save_parent_data['RegistrationNumber']         = $post_data['RegistrationNumber'];
        $save_parent_data['Tool']                       = $post_data['Tool'];
        $save_parent_data['DomainID']                   = implode(',',$post_data['DomainID']);
        $save_parent_data['SupervisorID']               = $post_data['SupervisorID'];
        $save_parent_data['StudentCanEdit']             = 0;
        //$save_parent_data['StudentID']                  = $this->session->userdata['admin']['UserID'];
        $save_parent_data['IsDataBaseSubjectPassed']    = (isset($post_data['IsDataBaseSubjectPassed']) ? 1 : 0 );
        $save_parent_data['IsSoftwareEngineeringSubjectPassed']       = (isset($post_data['IsSoftwareEngineeringSubjectPassed']) ? 1 : 0 );

         $send_partner_email = false;

        if(isset($post_data['HasPartner']) && $post_data['HasPartner'] == 1 && $post_data['PartnerRegistrationNumber'] != $result['PartnerRegistrationNumber']){



            $partner_data = $this->User_model->get($post_data['PartnerRegistrationNumber'],false,'RegistrationNumber');
            if($partner_data){
                $save_parent_data['PartnerID'] = $partner_data->UserID;
                $send_partner_email = true;
                $partner_notification_data = array();
                $partner_notification_data['UserID'] = $student_data->UserID;
                $partner_notification_data['FirstName'] = $student_data->FirstName;
                $partner_notification_data['LastName'] = $student_data->LastName;
                $partner_notification_data['RegistrationNumber'] = $student_data->RegistrationNumber;
                $partner_notification_data['Email'] = $student_data->Email;

            }else{
                $errors['error'] =  'Partner does not exist.';
                $errors['success'] = false;
                echo json_encode($errors);
                exit;

            }

        }else{
            $save_parent_data['IsRequestSentToSupervisor'] = 1;
        }




        $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');        
        $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];


        $this->$parent->update($save_parent_data,array('FypID' => $FypID));
        if($FypID > 0)
            {
                $message = 'submitted successfully';
                
                if($send_partner_email){
                    $send_partner_request = $this->sendPartnerRequest($student_notification_data,$partner_notification_data,$FypID);
                    $message = 'FYP registration form is submitted and your request is sent to partner once he/she approve then supervior can view this form';
                }


                if(!$send_partner_email){// send request if partner approved request in this case there is no partner

                    $supervisor_data = $this->User_model->get($post_data['SupervisorID'],false,'UserID');
                    $supervisor_notification_data = array();
                    $supervisor_notification_data['UserID'] = $supervisor_data->UserID;
                    $supervisor_notification_data['FirstName'] = $supervisor_data->FirstName;
                    $supervisor_notification_data['LastName'] = $supervisor_data->FirstName;
                    $supervisor_notification_data['Email'] = $supervisor_data->Email;

                    $this->sendSupervisorRequest($student_notification_data,$supervisor_notification_data,$FypID);
                    $message = 'FYP registration form is submitted. Wait for supervisor approval.';

                }
                
            
                
                $success['error']   = false;
                $success['success'] = $message;
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/view/'.$FypID;
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }


                
    }




     public function FypApproval(){
        $post_data = $this->input->post();
        $parent  = $this->data['Parent_model'];
        $result         = $this->$parent->getFypByID('fyps.FypID = '.$post_data['FypID']);
        if(empty($result)){
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;

        }

        if(isset($post_data['UserType']) && $post_data['UserType'] == 'Partner'){
            if($result['IsRequestSentToSupervisor'] == 0){

                $supervisor_data = $this->User_model->get($result['SupervisorID'],false,'UserID');
                $supervisor_notification_data = array();
                $supervisor_notification_data['UserID'] = $supervisor_data->UserID;
                $supervisor_notification_data['FirstName'] = $supervisor_data->FirstName;
                $supervisor_notification_data['LastName'] = $supervisor_data->FirstName;
                $supervisor_notification_data['Email'] = $supervisor_data->Email;

                $student_data = $this->User_model->get($result['StudentID'],false,'UserID');
                $student_notification_data = array();
                $student_notification_data['UserID'] = $student_data->UserID;
                $student_notification_data['FirstName'] = $student_data->FirstName;
                $student_notification_data['LastName'] = $student_data->LastName;
                $student_notification_data['RegistrationNumber'] = $student_data->RegistrationNumber;
                $student_notification_data['Email'] = $student_data->Email;

                $this->sendSupervisorRequest($student_notification_data,$supervisor_notification_data,$post_data['FypID']);

                $this->Fyp_model->update(array('IsRequestSentToSupervisor' => 1),array('FypID' => $post_data['FypID']));
                

            }
             

                if($this->session->userdata['admin']['RoleID'] == 4)
                {
                    if($post_data['Status'] == 'Approved'){

                        $update['IsApprovedByPartner'] = 'Approved';

                    }else if($post_data['Status'] == 'Rejected'){
                        $update['IsApprovedByPartner'] = 'Rejected';

                    }
                    $this->Fyp_model->update($update,array('FypID' => $post_data['FypID']));

                    $this->sendPartnerResponseEmailToStudent($result,$post_data['Status']);
                    $success['error']   = false;
                    $success['reload']   = true;
                    $success['success'] = lang('update_successfully');
                    echo json_encode($success);
                    exit;

                }else
                {
                    $errors['error'] =  'Something went wrong';
                    $errors['success'] = false;
                    echo json_encode($errors);
                    exit;

                }
        }

        if(isset($post_data['UserType']) && $post_data['UserType'] == 'Supervisor'){

                if($this->session->userdata['admin']['RoleID'] == 3)
                {
                    if($post_data['Status'] == 'Approved'){

                        $update['IsApprovedBySupervisor'] = 'Approved';

                    }else if($post_data['Status'] == 'Rejected'){
                        $this->validateRejection();
                        $reason_array = array();
                        if($result['RejectReasonBySupervisor'] == ''){
                            $reason_array[] = $post_data['RejectReasonBySupervisor'];
                        }else{
                            $reason_array = json_decode($result['RejectReasonBySupervisor']);
                            $reason_array[] = $post_data['RejectReasonBySupervisor'];
                        }
                        


                        $update['IsApprovedBySupervisor'] = 'Rejected';
                        $update['RejectReasonBySupervisor'] = json_encode($reason_array);

                    }
                    $this->Fyp_model->update($update,array('FypID' => $post_data['FypID']));

                    $this->sendSupervisorResponseEmailToStudent($result,$post_data['Status']);
                    if($result['PartnerID'] > 0 && $result['IsApprovedByPartner'] != 'Rejected'){
                         $this->sendSupervisorResponseEmailToStudentPartner($result,$post_data['Status']);
                    }
                    $success['error']   = false;
                    $success['reload']   = true;
                    $success['success'] = lang('update_successfully');
                    echo json_encode($success);
                    exit;

                }else
                {
                    $errors['error'] =  'Something went wrong';
                    $errors['success'] = false;
                    echo json_encode($errors);
                    exit;

                }
        }
     }


     private function sendPartnerResponseEmailToStudent($result,$Status){
        
        


        $email_template = get_email_template(7);


        $subject = $email_template->Heading;
        $message = $email_template->Description;


        $message = str_replace("{{status}}", $Status, $message);
        $message = str_replace("{{student_name}}", $result['StudentName'], $message);
        $message = str_replace("{{partner_name}}", $result['PartnerName'], $message);
        $message = str_replace("{{partner_registration_number}}", $result['PartnerRegistrationNumber'], $message);
        
        $data['to'] = $result['StudentEmail'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        if (!in_array($_SERVER['HTTP_HOST'], ['localhost','localhost:8888','localhost:8080', 'projects.local', '127.0.0.1', '127.0.0.1:8080'])){
            sendEmail($data);//not sending email on localhost
        } 


       


        $notification_data = array();
        $notification_data['NotificationFrom']      = $result['PartnerID'];
        $notification_data['NotificationTo']        = $result['StudentID'];
        $notification_data['FypID']                 = $result['FypID'];
        $notification_data['Title']                 = 'FYP Request Approval Status By Partner';
        $notification_data['Description']           = 'Your partner '.$Status.' your request for FYP registration.';
        $notification_data['NotificationType']      = 'partner_fyp_notification';
        $notification_data['IsRead']                = 0;
        $notification_data['CreatedAt']             = time();

        $this->Notification_model->save($notification_data);

        



    }

    private function sendSupervisorResponseEmailToStudent($result,$Status){
        
        


        $email_template = get_email_template(8);


        $subject = $email_template->Heading;
        $message = $email_template->Description;


        $message = str_replace("{{status}}", $Status, $message);
        $message = str_replace("{{student_name}}", $result['StudentName'], $message);
        $message = str_replace("{{supervisor_name}}", $result['SupervisorName'], $message);
       
        
        $data['to'] = $result['StudentEmail'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        if (!in_array($_SERVER['HTTP_HOST'], ['localhost','localhost:8888','localhost:8080', 'projects.local', '127.0.0.1', '127.0.0.1:8080'])){
            sendEmail($data);//not sending email on localhost
        } 


       //print_rm($data);


        $notification_data = array();
        $notification_data['NotificationFrom']      = $result['SupervisorID'];
        $notification_data['NotificationTo']        = $result['StudentID'];
        $notification_data['FypID']                 = $result['FypID'];
        $notification_data['Title']                 = 'FYP Request Approval Status By Supervisor';
        $notification_data['Description']           = 'Your supervisor '.$Status.' your request for FYP registration.';
        $notification_data['NotificationType']      = 'supervisor_fyp_notification';
        $notification_data['IsRead']                = 0;
        $notification_data['CreatedAt']             = time();

        $this->Notification_model->save($notification_data);

        



    }

    private function sendSupervisorResponseEmailToStudentPartner($result,$Status){
        
        


        $email_template = get_email_template(8);


        $subject = $email_template->Heading;
        $message = $email_template->Description;


        $message = str_replace("{{status}}", $Status, $message);
        $message = str_replace("{{student_name}}", $result['PartnerName'], $message);
        $message = str_replace("{{supervisor_name}}", $result['SupervisorName'], $message);
       
        
        $data['to'] = $result['PartnerEmail'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        if (!in_array($_SERVER['HTTP_HOST'], ['localhost','localhost:8888','localhost:8080', 'projects.local', '127.0.0.1', '127.0.0.1:8080'])){
            sendEmail($data);//not sending email on localhost
        } 


       //print_rm($data);


        $notification_data = array();
        $notification_data['NotificationFrom']      = $result['SupervisorID'];
        $notification_data['NotificationTo']        = $result['PartnerID'];
        $notification_data['FypID']                 = $result['FypID'];
        $notification_data['Title']                 = 'FYP Request Approval Status By Supervisor';
        $notification_data['Description']           = 'Your supervisor '.$Status.' your request for FYP registration.';
        $notification_data['NotificationType']      = 'supervisor_fyp_notification';
        $notification_data['IsRead']                = 0;
        $notification_data['CreatedAt']             = time();

        $this->Notification_model->save($notification_data);

        



    }



    public function updateCanEdit(){

        if($this->session->userdata['admin']['RoleID'] == 4){
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
           
            echo json_encode($errors);
            exit;

        }
        $post_data = $this->input->post();
        $result    = $this->Fyp_model->getFypByID('fyps.FypID = '.$post_data['FypID']);

        if(empty($result)){
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        if($this->session->userdata['admin']['RoleID'] == 3){
            if($result['SupervisorID'] != $this->session->userdata['admin']['UserID']){
                $errors['error'] =  lang('you_dont_have_its_access');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
        }



        $this->Fyp_model->update(array('StudentCanEdit' => ($post_data['value'] == 0 ? 1:0)),array('FypID' => $post_data['FypID']));

        $success['error']   = false;
        $success['reload']   = true;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit;



    }
    
    
    
    
   
    
    private function delete(){
        
         if(!checkRightAccess(78,$this->session->userdata['admin']['RoleID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];
        
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        
        $this->$parent->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    
    
    

}