<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('UTC');


class Dashboard extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->load->model('Dashboard_model');
        
        $this->load->model('User_model');
        $this->load->model('Modules_users_rights_model');
        $this->load->model('Module_model');
        
        //$this->load->model('Dump_model');
        // $this->load->model('Dump2_model');
    }

    public function index()
    {
        
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';
        $this->load->view('backend/layouts/default', $this->data);
    }

    

}