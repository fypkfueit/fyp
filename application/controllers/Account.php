<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Card_model');
        $this->load->model('City_model');
        $this->load->model('Coupon_model');

        $this->data['language'] = $this->language;
    }

    public function register()
    {
        $this->data['cards'] = $this->Card_model->getCards('cards.IsActive = 1', $this->language);
        $this->data['cities'] = $this->City_model->getAllCities($this->language, false);
        $this->load->view('frontend/register', $this->data);
    }

    public function checkUser()
    {

    }

    public function signUp()
    {
        $post_data = $this->input->post();
        // dump($post_data);
        if (isset($post_data['UserID']) && $post_data['UserID'] > 0) {
            $this->signUpValidation();
            // User exist in db already
            $update_by['UserID'] = $post_data['UserID'];
            unset($post_data['UserID']);
            unset($post_data['termss']);
            if (isset($_FILES['IDImage']) && $_FILES['IDImage']['name'] != '') {
                ini_set('memory_limit', '-1');
                $post_data['IDImage'] = uploadImage("IDImage", "uploads/users/");
            }
            $post_data['RoleID'] = 2; // App User
            $post_data['IsVerified'] = 0; // Not verified, Needs admin verification
            $ProfileCompletedPercentage = 70;

            if (isset($_FILES['IDImage']) && $_FILES['IDImage']['name'] != '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            if (isset($post_data['DateOfBirth']) && $post_data['DateOfBirth'] !== '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            if (isset($post_data['Gender']) && $post_data['Gender'] !== '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            if (isset($post_data['CompanyName']) && $post_data['CompanyName'] !== '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            if (isset($post_data['Position']) && $post_data['Position'] !== '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            if (isset($post_data['Interest']) && $post_data['Interest'] !== '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            $post_data['ProfileCompletedPercentage'] = $ProfileCompletedPercentage;
            $post_data['UpdatedAt'] = time();
            $this->User_model->update($post_data, $update_by);
            $user_info = $this->User_model->getUserInfo("users.UserID = " . $update_by['UserID']);
            $this->sendWelcomeEmail($user_info);
            $this->sendWelcomeSMS($user_info);
            $response['status'] = true;
            $response['message'] = lang('registered_successfully');
            if (isset($post_data['Amount']) && $post_data['Amount'] > 0)
            {
                $response['redirect_url'] = base_url().'account/load_paytabs?token='.base64_encode($post_data['UserID']).'&verify_token='.base64_encode($post_data['Amount']);
            } else {
                $response['redirect_url'] = base_url();
            }
            echo json_encode($response);
            exit();
        } else {
            $this->signUpValidation();
            unset($post_data['termss']);
            if (isset($_FILES['IDImage']) && $_FILES['IDImage']['name'] != '') {
                ini_set('memory_limit', '-1');
                $post_data['IDImage'] = uploadImage("IDImage", "uploads/users/");
            }
            $post_data['RoleID'] = 2; // App User
            $post_data['IsVerified'] = 0; // Not verified, Needs admin verification
            $ProfileCompletedPercentage = 70;

            if (isset($_FILES['IDImage']) && $_FILES['IDImage']['name'] != '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            if (isset($post_data['DateOfBirth']) && $post_data['DateOfBirth'] !== '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            if (isset($post_data['Gender']) && $post_data['Gender'] !== '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            if (isset($post_data['CompanyName']) && $post_data['CompanyName'] !== '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            if (isset($post_data['Position']) && $post_data['Position'] !== '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            if (isset($post_data['Interest']) && $post_data['Interest'] !== '') {
                $ProfileCompletedPercentage = $ProfileCompletedPercentage + 5;
            }

            $post_data['ProfileCompletedPercentage'] = $ProfileCompletedPercentage;
            $post_data['CreatedAt'] = $post_data['UpdatedAt'] = time();
            $insert_id = $this->User_model->save($post_data);
            if ($insert_id > 0) {
                $user_info = $this->User_model->getUserInfo("users.UserID = " . $insert_id);
                $this->sendWelcomeEmail($user_info);
                $this->sendWelcomeSMS($user_info);
                $response['status'] = true;
                $response['message'] = lang('registered_successfully');
                if (isset($post_data['Amount']) && $post_data['Amount'] > 0)
                {
                    $response['redirect_url'] = base_url().'account/load_paytabs?token='.base64_encode($insert_id).'&verify_token='.base64_encode($post_data['Amount']);
                } else {
                    $response['redirect_url'] = base_url();
                }
                echo json_encode($response);
                exit();
            } else {
                $response['status'] = false;
                $response['message'] = lang('something_went_wrong');
                echo json_encode($response);
                exit();
            }
        }
    }

    private function signUpValidation()
    {
        $this->form_validation->set_error_delimiters('<div class="">', '</div>');
        $this->form_validation->set_rules('Email', lang('email'), 'trim|required|valid_email|is_unique[users.Email]');
        $this->form_validation->set_rules('Mobile', lang('mobile'), 'trim|required|is_unique[users.Mobile]');
        if ($this->form_validation->run() == FALSE) {
            $response['status'] = false;
            $response['message'] = validation_errors();
            echo json_encode($response);
            exit();
        } else {
            return true;
        }

    }

    private function sendWelcomeEmail($user_info)
    {
        $email_template = get_email_template(1, $this->language);
        $subject = $email_template->Heading;
        $message = $email_template->Description;
        $message = str_replace("{{name}}", $user_info['FirstName'], $message);
        $data['to'] = $user_info['Email'];
        $data['subject'] = $subject;
        $data['message'] = email_format($message);
        sendEmail($data);
    }

    private function sendWelcomeSMS($user_info)
    {
        $para['msg'] = "Thank you for registering with Exclusave Card, your request is under process. You will be notified via email once the account is activated.";
        $para['msisdn'] = $user_info['Mobile'];
        sms($para);
    }

    public function login()
    {
        $this->load->view('frontend/login', $this->data);
    }

    public function checkLogin()
    {

    }

    public function profile()
    {

    }

    public function updateProfile()
    {

    }

    public function applyCoupon()
    {
        $post_data = $this->input->post(); // CardID, CouponCode, Amount (On which discount is being checked)
        // dump($post_data);
        if (!empty($post_data)) {
            $coupon = $this->Coupon_model->getWithMultipleFields(array('CouponCode' => $post_data['CouponCode'], 'CardID' => $post_data['CardID']), true);
            // dump($coupon);
            if ($coupon) {
                $Date_timestamp = time();
                if ($coupon['ExpiryDate'] >= $Date_timestamp && $coupon['UsageCount'] > 0) {
                    if ($coupon['DiscountType'] == 'Percentage') {
                        $discount = ($coupon['DiscountFactor'] / 100) * $post_data['Amount'];
                        $response_data['DiscountedAmount'] = $post_data['Amount'] - $discount;
                        $response_data['DiscountAvailed'] = $discount;
                        $response_data['DiscountAppliedFactor'] = $coupon['DiscountFactor'] . '%';
                    } else {
                        $response_data['DiscountedAmount'] = $post_data['Amount'] - $coupon['DiscountFactor'];
                        $response_data['DiscountAvailed'] = $coupon['DiscountFactor'];
                        $response_data['DiscountAppliedFactor'] = $coupon['DiscountFactor'] . ' SAR';
                    }
                    if ($response_data['DiscountedAmount'] < 0) { // if discounted amount goes negative then made it 0, can be changed here if needed
                        $response_data['DiscountedAmount'] = 0;
                    }
                    $update['UsageCount'] = $coupon['UsageCount'] - 1;
                    $update_by['CouponID'] = $coupon['CouponID'];
                    $this->Coupon_model->update($update, $update_by);
                    $response_data['CouponID'] = $coupon['CouponID'];
                    $response_data['DiscountedAmount'] = (string)$response_data['DiscountedAmount'];
                    $response_data['DiscountAvailed'] = (string)$response_data['DiscountAvailed'];
                    $response_data['DiscountAppliedFactor'] = (string)$response_data['DiscountAppliedFactor'];
                    $response_data['status'] = true;
                    $response_data['message'] = lang('coupon_applied');
                    echo json_encode($response_data);
                    exit();
                } else {
                    $response_data['status'] = false;
                    $response_data['message'] = lang('coupon_expired');
                    echo json_encode($response_data);
                    exit();
                }
            } else {
                $response_data['status'] = false;
                $response_data['message'] = lang('coupon_invalid');
                echo json_encode($response_data);
                exit();
            }
        } else {
            $response_data['status'] = false;
            $response_data['message'] = lang('something_went_wrong');
            echo json_encode($response_data);
            exit();
        }
    }

    public function load_paytabs()
    {
        $UserID = base64_decode($_GET['token']);
        $Amount = base64_decode($_GET['verify_token']);
        $this->load->library('Paytabs');
        $user_info = $this->User_model->getUserInfo("users.UserID = " . $UserID);
        $pt = new Paytabs();
        $result = $pt->create_pay_page(array(
            //Customer's Personal Information
            'merchant_email' => "dev@zynq.net",
            'secret_key' => "c7YRQHdLzzMN3TyMS6VmhZQEvgUMjj1vq0mgHz2h9P4cIDZBJUY1TOFp4AboTdTkdgBVQDcZshcbUbME5KFZzloEWA2PNV91cKX2",
            'cc_first_name' => $user_info['FirstName'],          //This will be prefilled as Credit Card First Name
            'cc_last_name' => $user_info['LastName'],            //This will be prefilled as Credit Card Last Name
            'cc_phone_number' => $user_info['Mobile'],
            'phone_number' => $user_info['Mobile'],
            'email' => $user_info['Email'],

            //Customer's Billing Address (All fields are mandatory)
            //When the country is selected as USA or CANADA, the state field should contain a String of 2 characters containing the ISO state code otherwise the payments may be rejected.
            //For other countries, the state can be a string of up to 32 characters.
            'billing_address' => "manama bahrain",
            'city' => $user_info['CityTitle'],
            'state' => $user_info['CityTitle'],
            'postal_code' => "00973",
            'country' => "BHR",

            //Customer's Shipping Address (All fields are mandatory)
            'address_shipping' => "Juffair bahrain",
            'city_shipping' => $user_info['CityTitle'],
            'state_shipping' => $user_info['CityTitle'],
            'postal_code_shipping' => "00973",
            'country_shipping' => "BHR",

            //Product Information
            "products_per_title" => "Signup",   //Product title of the product. If multiple products then add “||” separator
            'quantity' => "1",                                    //Quantity of products. If multiple products then add “||” separator
            'unit_price' => $Amount,                                  //Unit price of the product. If multiple products then add “||” separator.
            "other_charges" => "0",                                     //Additional charges. e.g.: shipping charges, taxes, VAT, etc.

            'amount' => $Amount,                                          //Amount of the products and other charges, it should be equal to: amount = (sum of all products’ (unit_price * quantity)) + other_charges
            'discount' => "0",                                                //Discount of the transaction. The Total amount of the invoice will be= amount - discount
            'currency' => "SAR",                                            //Currency of the amount stated. 3 character ISO currency code


            //Invoice Information
            'title' => $user_info['FirstName'].' '.$user_info['MiddleName'].' '.$user_info['LastName'],               // Customer's Name on the invoice
            "msg_lang" => "en",                 //Language of the PayPage to be created. Invalid or blank entries will default to English.(Englsh/Arabic)
            "reference_no" => time(),        //Invoice reference number in your system


            //Website Information
            "site_url" => "http://www.schopfen.com",      //The requesting website be exactly the same as the website/URL associated with your PayTabs Merchant Account
            'return_url' => base_url(),
            "cms_with_version" => "API USING PHP",

            "paypage_info" => "1"
        ));
        $this->session->set_userdata('message', 'You have registered successfully. You can login once admin verifies your account.');
        redirect($result->payment_url);
    }

}