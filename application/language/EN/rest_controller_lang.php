<?php

/*
 * English language
 */

//General

$lang['some_thing_went_wrong'] = 'Something went wrong. Please try again.';


$lang['save_successfully'] = 'Saved Successfully';
$lang['update_successfully'] = 'Updated Successfully';
$lang['deleted_successfully'] = 'Deleted Successfully';
$lang['you_should_update_data_seperately_for_each_language'] = 'You would need to update data separately for each language';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['actions'] = 'Action';
$lang['submit'] = 'Submit';
$lang['back'] = 'Back';
$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['are_you_sure'] = 'Are you sure you want to delete this?';
$lang['view'] = 'View';
$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['is_active'] = 'Publish';
$lang['create_table'] = 'Create Table';
$lang['create_model'] = 'Create Model';
$lang['create_view'] = 'Create View';
$lang['create_controller'] = 'Create Controller';
$lang['logout'] = 'Logout';

$lang['please_add_role_first'] = 'Please add role first';
$lang['you_dont_have_its_access'] = 'You don\'t have its access';

$lang['SiteName'] = 'Site Name';
$lang['PhoneNumber'] = 'Phone Number';
$lang['Whatsapp'] = 'Whatsapp';
$lang['Skype'] = 'Skype';
$lang['Fax'] = 'Fax';
$lang['SiteLogo'] = 'Site Logo';
$lang['EditSiteSettings'] = 'Edit Site Settings';
$lang['FacebookUrl'] = 'Facebook Url';
$lang['GoogleUrl'] = 'Google Url';
$lang['LinkedInUrl'] = 'LinkedIn Url';
$lang['TwitterUrl'] = 'Twitter Url';
//end General

// Module Section
$lang['choose_parent_module'] = 'Choose Parent Module';
$lang['parent_module'] = 'Parent Module';
$lang['slug'] = 'Slug';
$lang['icon_class'] = 'Icon Class';

$lang['title'] = 'Title';
$lang['add_module'] = 'Add Module';
$lang['modules'] = 'Modules';
$lang['module'] = 'Module';
$lang['module_rights'] = 'Module Rights';

$lang['please_add_module_first'] = 'Please add module first';
$lang['rights'] = 'Rights';


// Roles Section

$lang['select_role'] = 'Select Role';
$lang['add_role'] = 'Add Role';
$lang['roles'] = 'Roles';
$lang['role'] = 'Role';
$lang['please_add_role_first'] = 'Please add role first';
$lang['choose_user_role'] = 'Choose user role';


// activity section

$lang['add_activity'] = 'Add Activity';
$lang['activities'] = 'Activities';
$lang['activity'] = 'Activity';
$lang['choose_activity'] = 'Choose Activity';

// user

$lang['please_add_user_first'] = 'Please add user first';

$lang['name'] = 'Name';
$lang['first_name'] = 'First Name';
$lang['last_name'] = 'Last Name';
$lang['middle_name'] = 'Middle Name';
$lang['user'] = 'User';
$lang['users'] = 'Users';
$lang['add_user'] = 'Add User';
$lang['select_user'] = 'Select User';
$lang['card_number'] = 'Card Number';

$lang['email'] = 'Email';
$lang['password'] = 'Password';
$lang['min_length'] = '(Min char 8)';
$lang['confirm_password'] = 'Confirm Password';


$lang['add_city'] = 'Add City';
$lang['citys'] = 'Cities';
$lang['city'] = 'City';
$lang['add_district'] = 'Add District';
$lang['districts'] = 'Districts';
$lang['district'] = 'District';
$lang['please_add_distric_first'] = 'Please add distric first';
$lang['choose_district'] = 'Choose District';
$lang['add_patient'] = 'Add Patient';
$lang['patients'] = 'Patients';
$lang['patient'] = 'Patient';
$lang['add_center'] = 'Add center';
$lang['centers'] = 'Centers';
$lang['center'] = 'Center';
$lang['add_screening_camp'] = 'Add Screening camp';
$lang['screening_camps'] = 'Screening camps';
$lang['screening_camp'] = 'Screening camp';
$lang['add_center_type'] = 'Add Center type';
$lang['center_types'] = 'Center Types';
$lang['center_type'] = 'Center Type';
$lang['add_email_template'] = 'Add Email Template';
$lang['email_templates'] = 'Email Templates';
$lang['email_template'] = 'Email Template';
$lang['Heading'] = 'Heading';
$lang['Description'] = 'Description';
$lang['Image'] = 'Image';
$lang['add_language'] = 'Add Language';
$lang['languages'] = 'Languages';
$lang['language'] = 'Language';
$lang['short_code'] = 'Short Code';

$lang['is_default'] = 'Is Default';
$lang['please_select_default_value_language_first'] = 'Please select another default value language first';
$lang['add_test'] = 'Add Test';
$lang['tests'] = 'Tests';
$lang['test'] = 'Test';
$lang['add_test'] = 'Add Test';
$lang['tests'] = 'Tests';
$lang['test'] = 'Test';
$lang['add_group'] = 'Add Group';
$lang['groups'] = 'Groups';
$lang['group'] = 'Group';
$lang['add_group'] = 'Add Group';
$lang['groups'] = 'Groups';
$lang['group'] = 'Group';
$lang['add_feed'] = 'Add Feed';
$lang['feeds'] = 'Feeds';
$lang['feed'] = 'Feed';
$lang['add_category'] = 'Add Category';
$lang['categorys'] = 'Categories';
$lang['category'] = 'Category';
$lang['add_child_category'] = 'Add Child Category';
$lang['child_catagories_of'] = 'Child Catagories Of';
$lang['add_postReported'] = 'Add PostReported';
$lang['postReporteds'] = 'PostReporteds';
$lang['postReported'] = 'PostReported';
$lang['add_repotedComment'] = 'Add RepotedComment';
$lang['repotedComments'] = 'RepotedComments';
$lang['repotedComment'] = 'RepotedComment';
$lang['add_repotedGroup'] = 'Add RepotedGroup';
$lang['repotedGroups'] = 'RepotedGroups';
$lang['repotedGroup'] = 'RepotedGroup';
$lang['add_type'] = 'Add Type';
$lang['types'] = 'Types';
$lang['type'] = 'Type';
$lang['add_user_type'] = 'Add User_type';
$lang['user_types'] = 'User_types';
$lang['user_type'] = 'User_type';
//////////////////
$lang['something_went_wrong'] = 'Something went wrong. Please try again later.';
$lang['registered_successfully'] = 'Registered successfully';
$lang['username'] = 'Username';
$lang['mobile'] = 'Mobile No';
$lang['sorry_you_cant_login'] = 'Sorry! You can not login at this moment. Please contact admin for further details.';
$lang['logged_in_successfully'] = 'Logged in successfully.';
$lang['user_not_found_with_these_login_details'] = 'User not found with these login details.';
$lang['user_not_found'] = 'User not found.';
$lang['email_already_exist'] = 'Email address already exist.';
$lang['username_already_exist'] = 'Username already exist.';
$lang['mobile_already_exist'] = 'Mobile no. already exist.';
$lang['user_logged_out_successfully'] = 'User logged out successfully.';
$lang['password_reset_link_sent_in_email'] = 'Password reset link is sent to you on your registered email.';
$lang['password_reset_link_sent_in_mobile'] = 'Password reset link is sent to you on your registered mobile no.';
$lang['update_available_for_app'] = 'An update is available to your app.';
$lang['followed_successfully'] = 'Followed successfully.';
$lang['unfollowed_successfully'] = 'Unfollowed successfully.';
$lang['add_product'] = 'Add Product';
$lang['products'] = 'Products';
$lang['product'] = 'Product';
$lang['please_verify_your_email'] = 'Your email address is not verified. Please verify it to proceed.';
$lang['please_verify_your_mobile'] = 'Your mobile no is not verified. Please verify it to proceed.';
$lang['verification_code_sent'] = 'Verification code is sent to your mobile no.';
$lang['mobile_no_verified'] = 'Your mobile no is verified successfully.';
$lang['mobile_no_failed_to_verified'] = 'Your mobile no verification has failed.';
$lang['profile_updated'] = 'Your profile is updated successfully.';
$lang['notification_marked_as_read'] = 'Notification marked as read.';
$lang['product_added_successfully'] = 'Product added successfully.';
$lang['product_updated_successfully'] = 'Product updated successfully.';
$lang['booth_name'] = 'Booth Name';
$lang['product_type'] = 'Product Type';
$lang['description'] = 'Description';
$lang['add_theme'] = 'Add Theme';
$lang['themess'] = 'Themes';
$lang['themes'] = 'Themes';
$lang['theme'] = 'Theme';
$lang['user_already_reported'] = 'User is already reported.';
$lang['user_reported_successfully'] = 'User reported successfully.';
$lang['user_already_blocked'] = 'User is already blocked.';
$lang['user_blocked_successfully'] = 'User blocked successfully.';
$lang['user_unblocked_successfully'] = 'User unblocked successfully.';
$lang['profile_customized_successfully'] = 'Profile customized successfully.';
$lang['feedback_sent_successfully'] = 'Feedback sent successfully.';
$lang['add_feedback'] = 'Add Feedback';
$lang['feedbacks'] = 'Feedbacks';
$lang['feedback'] = 'Feedback';
$lang['price'] = 'Price';
$lang['currency'] = 'Currency';
$lang['out_of_stock'] = 'Out of stock';
$lang['product_already_reported'] = 'Product is already reported.';
$lang['product_reported_successfully'] = 'Product reported successfully.';
$lang['product_disliked_successfully'] = 'Product disliked successfully.';
$lang['product_liked_successfully'] = 'Product liked successfully.';
$lang['comment_saved_successfully'] = 'Comment saved successfully';
$lang['added_to_wishlist'] = 'Product added to wishlist.';
$lang['removed_from_wishlist'] = 'Product removed from wishlist.';
$lang['added_to_cart'] = 'Product added to cart.';
$lang['cart_updated'] = 'Cart updated successfully.';
$lang['product_not_available'] = 'Sorry! This product is not available.';
$lang['your_order_sent_for_approval'] = 'Thank you for placing your order with us. You will be notified once relevant booths approve your ordered items.';
$lang['question_already_reported'] = 'Question is already reported.';
$lang['question_reported_successfully'] = 'Question reported successfully.';
$lang['your_cart_is_empty'] = 'Your cart is empty. Please add items to your cart first.';
$lang['add_card']            = 'Add Card';
$lang['cards']            = 'Cards';
$lang['card']            = 'Card';
$lang['add_store'] = 'Add Merchant';
$lang['stores'] = 'Merchants';
$lang['store'] = 'Merchant';
$lang['choose_store'] = 'Select Merchant';
$lang['coupon_applied'] = 'Hurray! Promocode applied successfully.';
$lang['coupon_expired'] = 'Sorry! This promocode has expired.';
$lang['coupon_invalid'] = 'Sorry! This promocode is invalid.';
$lang['added_to_favourite'] = 'Merchant added to favourites list.';
$lang['removed_from_favourite'] = 'Merchant removed from favourites list.';
$lang['already_added_to_favourites'] = 'Merchant is already added to favourites list.';
$lang['app_version_missing'] = 'App version is missing from your request.';
$lang['add_cardtype']            = 'Add Card Type';
$lang['cardtypes']            = 'Card Types';
$lang['cardtype']            = 'Card Type';
$lang['add_company']            = 'Add Company';
$lang['companys']            = 'Companys';
$lang['company']            = 'Company';
$lang['add_cardgroup']            = 'Add Card group';
$lang['cardgroups']            = 'Card groups';
$lang['cardgroup']            = 'Card group';
$lang['booking_created'] = 'Booking request created successfully.';
$lang['info_not_found'] = 'Information not found.';
$lang['wrong_old_password'] = 'Wrong old password provided.';
$lang['password_changed_successfully'] = 'Password changed successfully. Please use the new password next time you login.';
$lang['no_reviews'] = 'No Reviews.';


/////////////////////////////////// FRONT END STRING //////////////////////////////////////
$lang['login_frontend'] = 'LOGIN';
$lang['register_frontend'] = 'REGISTER';
$lang['home_frontend'] = 'HOME';
$lang['cards_frontend'] = 'CARDS';
$lang['blogs_frontend'] = 'GRINGISH';
$lang['faq_frontend'] = 'FAQ';
$lang['partner_with_us_frontend'] = 'PARTNER WITH US';
$lang['top_offers_frontend'] = 'TOP OFFERS';
$lang['apply_now'] = 'APPLY NOW';
$lang['bestmobile'] = 'BEST MOBILE';
$lang['savetimeshopping'] = 'SAVE TIME SHOPPING';
$lang['updatesoffers'] = 'UPDATES AND OFFERS';
$lang['satisfaction'] = 'SATISFACTION';
$lang['card_benefits'] = 'CARD BENIFITS';
$lang['GETCARD'] = 'GET CARD';
$lang['clicknow'] = 'CLICK NOW';
$lang['makepayment'] = 'MAKE PAYMENT';
$lang['username'] = 'USERNAME';
$lang['passwordhours'] = 'PASSWORD HOURS';
$lang['login_and_enjoy'] = 'LOGIN & ENJOY';
$lang['Featured'] = 'FEATURED';
$lang['Offers'] = 'OFFERS';
$lang['Benefits'] = 'BENEFITS';
$lang['NEWSANDEVENTS'] = 'NEWS AND EVENTS';
$lang['Read'] = 'READ';
$lang['ABOUTUS'] = 'ABOUT US';
$lang['NEWSEvents'] = 'NEWS & EVENTS';
$lang['EXCLUSAVEMAP'] = 'EXCLUSAVE MAP';
$lang['WISHLIST'] = 'WISHLIST';
$lang['CAREER'] = 'CAREER';
$lang['CONTACTUS'] = 'CONTACT US';
$lang['add_home_slide']            = 'Add Home Slide';
$lang['home_slides']            = 'Home Slides';
$lang['home_slide']            = 'Home Slide';
$lang['add_blog']            = 'Add Blog';
$lang['blogs']            = 'Blogs';
$lang['blog']            = 'Blog';
$lang['add_news']            = 'Add News & Event';
$lang['newss']            = 'News & Events';
$lang['news']            = 'News & Event';
$lang['add_faq']            = 'Add Faq';
$lang['faqs']            = 'Faqs';
$lang['faq']            = 'Faq';
$lang['add_career']            = 'Add Career';
$lang['careers']            = 'Careers';
$lang['career']            = 'Career';
$lang['cookies_usage_alert'] = 'This website uses cookies to ensure you get the best experience on our website.';
$lang['learn_more'] = 'Learn more';
$lang['got_it'] = 'Got it!';
$lang['add_colorPackage']            = 'Add Color Package';
$lang['colorPackages']            = 'Color Packages';
$lang['colorPackage']            = 'Color Package';
$lang['add_test']            = 'Add Test';
$lang['tests']            = 'Tests';
$lang['test']            = 'Test';
$lang['add_complaint']            = 'Add Complaint';
$lang['complaints']            = 'Complaints';
$lang['complaint']            = 'Complaint';
$lang['subscription_expire']            = 'YOUR SUBSCRIPTION HAS EXPIRED, KINDLY RENEW IT';
$lang['update_available']            = 'Please update the app';
$lang['add_test']            = 'Add Test';
$lang['tests']            = 'Tests';
$lang['test']            = 'Test';
$lang['add_test']            = 'Add Test';
$lang['tests']            = 'Tests';
$lang['test']            = 'Test';
$lang['add_tests']            = 'Add Tests';
$lang['testss']            = 'Testss';
$lang['tests']            = 'Tests';
$lang['add_test']            = 'Add Test';
$lang['tests']            = 'Tests';
$lang['test']            = 'Test';
$lang['add_test']            = 'Add Test';
$lang['tests']            = 'Tests';
$lang['test']            = 'Test';
$lang['add_test']            = 'Add Test';
$lang['tests']            = 'Tests';
$lang['test']            = 'Test';
$lang['add_department']            = 'Add Department';
$lang['departments']            = 'Departments';
$lang['department']            = 'Department';
$lang['add_program']            = 'Add Program';
$lang['programs']            = 'Programs';
$lang['program']            = 'Program';
$lang['add_cls']            = 'Add Class';
$lang['clss']            = 'Classes';
$lang['cls']            = 'Class';
$lang['add_domain']            = 'Add Domain';
$lang['domains']            = 'Domains';
$lang['domain']            = 'Domain';
$lang['add_functional_requirement']            = 'Add Functional Requirement';
$lang['functional_requirements']            = 'Functional Requirements';
$lang['functional_requirement']            = 'Functional Requirement';
$lang['add_fyp']            = 'Add Fyp';
$lang['fyps']            = 'Fyps';
$lang['fyp']            = 'Fyp';
$lang['add_sesion']            = 'Add Session';
$lang['sesions']            = 'Sessions';
$lang['sesion']            = 'Session';