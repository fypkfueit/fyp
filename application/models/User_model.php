<?php

Class User_model extends Base_Model
{
    public $offset = NULL, $limit = NULL;
    public function __construct()
    {
        parent::__construct("users");

    }

    public function getUserInfo($where, $system_language_code = 'EN')
    {

        if($system_language_code == 'EN'){
            $lang = 1;
        }else{
             $lang = 2;
        }
        $this->db->select('users.*,users.IsActive as UserIsActive, cities_text.Title as CityTitle, cities.Currency, cities.CurrencySymbol, cards_text.Title as CardTitle, cards.*, coupon_color_packages.*, IF (users.CouponID > 0, coupons.CardImage, cards.CardImage) as CardImage,cardgroups.IsCouponCodeRequired ,IF (users.CouponID > 0, coupons.CardBackgroundImage, cards.CardBackgroundImage) as CardBackgroundImage,coupons.CouponCode');
        $this->db->from('users');
        $this->db->join('cards', 'users.CardID = cards.CardID', 'LEFT');
        $this->db->join('cards_text', 'cards.CardID = cards_text.CardID', 'LEFT');
        $this->db->join('cardgroups', 'cards.CardgroupID = cardgroups.CardgroupID', 'LEFT');
        $this->db->join('coupons', 'users.CouponID = coupons.CouponID', 'LEFT');
        $this->db->join('coupon_color_packages', 'coupons.PackageColorID = coupon_color_packages.ColorPackageID', 'LEFT');
        $this->db->join('cities', 'cities.CityID = users.CityID', 'Left');
        $this->db->join('cities_text', 'cities_text.CityID = cities.CityID AND cities_text.SystemLanguageID = '.$lang.'', 'Left');
        //$this->db->join('system_languages', 'system_languages.SystemLanguageID = cities_text.SystemLanguageID');
      //  $this->db->where('system_languages.ShortCode', $system_language_code);
        $this->db->where($where);
        $result = $this->db->get();
        return $result->row_array();
    }

    public function getUserData($data, $system_language_code = false)
    {

        $this->db->select('users.*,roles.IsActive as RoleActivation');
        $this->db->from('users');
        // $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('roles', 'roles.RoleID = users.RoleID', 'Left');

        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
        // $this->db->where($where);
        $this->db->where('users.Email', $data['Email']);
        //$this->db->or_where('users.UName',$data['Email']);
        $this->db->where('users.Password', $data['Password']);

        /*if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }*/
        //$this->db->where('system_languages.IsDefault', '1');
        return $this->db->get()->row_array();


    }

    public function getUsersAjax($where = false, $system_language_code = 'EN',  $sort = 'ASC', $sort_field = 'SortOrder', $like = false)
    {
        $this->db->select('users.*,cities_text.Title as CityTitle,roles_text.Title as RoleTitle, cities.Currency, coupons.CouponCode, cities.CurrencySymbol,cards_text.Title as CardTitle');
        $this->db->from('users');
        // $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('cards', 'users.CardID = cards.CardID', 'LEFT');
        $this->db->join('cards_text', 'cards.CardID = cards_text.CardID', 'LEFT');

        $this->db->join('coupons', 'users.CouponID = coupons.CouponID','left');


        $this->db->join('roles', 'users.RoleID = roles.RoleID', 'LEFT');
        $this->db->join('roles_text', 'roles.RoleID = roles_text.RoleID', 'LEFT');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID', 'LEFT');
        $this->db->join('system_languages', 'cities_text.SystemLanguageID = system_languages.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }

        $this->db->where('system_languages.ShortCode', $system_language_code);

        $this->db->group_by('users.UserID');
       
        if($like)
        {

            $like = "(users.FirstName LIKE '%$like%' OR users.MiddleName LIKE '%$like%' OR users.LastName LIKE '%$like%' OR users.Email LIKE '%$like%' OR users.Mobile LIKE '%$like%' OR users.CardNumber Like '%$like%' OR coupons.CouponCode Like '%$like%')";
            $this->db->where($like);
        }
        if (!in_array($sort_field, array('Action'))) {
            if ($sort_field == 'User Name')
                $this->db->order_by('users.FirstName', $sort);
        }

        if ($this->limit) {
            $this->db->limit($this->limit);
        }

        if (!is_null($this->offset)){
            $this->db->offset($this->offset);
        }
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
        return $result->result();


    }

    public function getUsers($where = false, $system_language_code = 'EN', $sort_by = 'users.UserID', $sort_as = 'DESC', $limit = false, $start = 0)
    {
        $this->db->select('users.*,cities_text.Title as CityTitle,roles_text.Title as RoleTitle, cities.Currency, coupons.CouponCode, cities.CurrencySymbol,cards_text.Title as CardTitle');
        $this->db->from('users');
        // $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
         $this->db->join('cards', 'users.CardID = cards.CardID', 'LEFT');
        $this->db->join('cards_text', 'cards.CardID = cards_text.CardID AND cards_text.SystemLanguageID = 1', 'LEFT');

        $this->db->join('coupons', 'users.CouponID = coupons.CouponID','left');


        $this->db->join('roles', 'users.RoleID = roles.RoleID', 'LEFT');
        $this->db->join('roles_text', 'roles.RoleID = roles_text.RoleID AND roles_text.SystemLanguageID = 1', 'LEFT');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID', 'LEFT');
        $this->db->join('system_languages', 'cities_text.SystemLanguageID = system_languages.SystemLanguageID');

        if ($where) {
            $this->db->where($where);
        }

        $this->db->where('system_languages.ShortCode', $system_language_code);

        $this->db->group_by('users.UserID');
        
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
        return $result->result();


    }

    public function getUsersCount($where = false,  $sort = 'ASC', $sort_field = 'SortOrder', $like = false){
        $this->db->select('COUNT(users.UserID) as UsersCount');
        $this->db->from('users');
        // $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
        $this->db->join('cards', 'users.CardID = cards.CardID', 'LEFT');
        $this->db->join('cards_text', 'cards.CardID = cards_text.CardID AND cards_text.SystemLanguageID = 1', 'LEFT');

        $this->db->join('coupons', 'users.CouponID = coupons.CouponID','left');


        $this->db->join('roles', 'users.RoleID = roles.RoleID', 'LEFT');
        $this->db->join('roles_text', 'roles.RoleID = roles_text.RoleID AND roles_text.SystemLanguageID = 1', 'LEFT');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1', 'LEFT');
       

        if ($where) {
            $this->db->where($where);
        }

        //$this->db->group_by('users.UserID');
        if ($like)
        {

            $like = "(users.FirstName LIKE '%$like%' OR users.MiddleName LIKE '%$like%' OR users.LastName LIKE '%$like%' OR users.Email LIKE '%$like%' OR users.Mobile LIKE '%$like%' OR users.CardNumber Like '%$like%' OR coupons.CouponCode Like '%$like%')";
            $this->db->where($like);
        }
        

        
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
         return $result->row()->UsersCount;


    }
    public function getUsersNew($where = false, $system_language_code = 'EN', $sort_by = 'users.UserID', $sort_as = 'DESC', $limit = false, $start = 0)
    {
        $this->db->select('users.*,cities_text.Title as CityTitle,roles_text.Title as RoleTitle, cities.Currency, cities.CurrencySymbol,cards_text.Title as CardTitle');
        $this->db->from('users');
        $this->db->join('cards', 'users.CardID = cards.CardID', 'LEFT');
        $this->db->join('cards_text', 'cards.CardID = cards_text.CardID');
        $this->db->join('roles', 'users.RoleID = roles.RoleID', 'LEFT');
        $this->db->join('roles_text', 'roles.RoleID = roles_text.RoleID');
        $this->db->join('cities', 'users.CityID = cities.CityID', 'LEFT');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID', 'LEFT');
        $this->db->join('system_languages slct', 'cards_text.SystemLanguageID = slct.SystemLanguageID');
        $this->db->join('system_languages slrt', 'roles_text.SystemLanguageID = slrt.SystemLanguageID');
        $this->db->join('system_languages slctt', 'cities_text.SystemLanguageID = slctt.SystemLanguageID');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where('slct.ShortCode', $system_language_code);
        $this->db->where('slrt.ShortCode', $system_language_code);
        $this->db->where('slctt.ShortCode', $system_language_code);
        $this->db->group_by('users.UserID');
        $this->db->order_by($sort_by, $sort_as);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            if ($result->num_rows() > 1) {
                return $result->result_array();
            } else {
                return $result->row_array();
            }
        } else {
            return array();
        }
    }

    public function getAdminUsers($where = false,  $sort = 'ASC', $sort_field = 'SortOrder', $like = false)
    {
        $this->db->select('users.*,roles_text.Title as RoleTitle');
        $this->db->from('users');
        $this->db->join('roles', 'users.RoleID = roles.RoleID', 'LEFT');
        $this->db->join('roles_text', 'roles.RoleID = roles_text.RoleID AND roles_text.SystemLanguageID = 1');
        
        if($where){
            $this->db->where($where);
        }
        if ($like)
        {

            $like = "(users.FirstName LIKE '%$like%' OR users.MiddleName LIKE '%$like%' OR users.LastName LIKE '%$like%' OR users.Email LIKE '%$like%' OR users.Mobile LIKE '%$like%')";
            $this->db->where($like);
        }
        if (!in_array($sort_field, array('Action'))) {
            if ($sort_field == 'User Name')
                $this->db->order_by('users.FirstName', $sort);
        }

        if ($this->limit) {
            $this->db->limit($this->limit);
        }

        if (!is_null($this->offset)) {
            $this->db->offset($this->offset);
        }
        
        $this->db->group_by('users.UserID');
        $result = $this->db->get();
        return $result->result();
    }

    public function getAdminUsersCount($where = false,  $sort = 'ASC', $sort_field = 'SortOrder', $like = false)
    {
        $this->db->select('COUNT(users.UserID) as UsersCount');
        $this->db->from('users');
        $this->db->join('roles', 'users.RoleID = roles.RoleID', 'LEFT');
        $this->db->join('roles_text', 'roles.RoleID = roles_text.RoleID AND roles_text.SystemLanguageID = 1');
        $this->db->where('users.RoleID', 1);
        if($where){
            $this->db->where($where);
        }
        if ($like)
        {

            $like = "(users.FirstName LIKE '%$like%' OR users.MiddleName LIKE '%$like%' OR users.LastName LIKE '%$like%' OR users.Email LIKE '%$like%' OR users.Mobile LIKE '%$like%')";
            $this->db->where($like);
        }
        if (!in_array($sort_field, array('Action'))) {
            if ($sort_field == 'User Name')
                $this->db->order_by('users.FirstName', $sort);
        }


        
       // $this->db->group_by('users.UserID');
        $result = $this->db->get();
        //echo $this->db->last_query();exit;
        return $result->row()->UsersCount;
    }

    public function getStoreUsers($where = false,  $sort = 'ASC', $sort_field = 'SortOrder', $like = false)
    {
        $this->db->select("users.*,roles_text.Title as RoleTitle, stores.*, stores_text.*, IF (stores.ParentID = 0, 'Merchant Admin', 'Branch User') as StoreUserType");
        $this->db->from('users');
        $this->db->join('stores', 'users.StoreID = stores.StoreID', 'LEFT');
        $this->db->join('stores_text', 'stores_text.StoreID = stores.StoreID');
        $this->db->join('roles', 'users.RoleID = roles.RoleID', 'LEFT');
        $this->db->join('roles_text', 'roles.RoleID = roles_text.RoleID AND roles_text.SystemLanguageID = 1');
        $this->db->where('users.RoleID', 3);
        if($where){
            $this->db->where($where);
        }
        if ($like)
        {

            $like = "(users.FirstName LIKE '%$like%' OR users.MiddleName LIKE '%$like%' OR users.LastName LIKE '%$like%' OR users.Email LIKE '%$like%' OR users.Mobile LIKE '%$like%' OR stores_text.Title LIKE '%$like%' OR stores_text.Address LIKE '%$like%')";
            $this->db->where($like);
        }
        if (!in_array($sort_field, array('Action'))) {
            if ($sort_field == 'User Name')
                $this->db->order_by('users.FirstName', $sort);
        }

        if ($this->limit) {
            $this->db->limit($this->limit);
        }

        if (!is_null($this->offset)) {
            $this->db->offset($this->offset);
        }
        $this->db->group_by('users.UserID');
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();
    }


    public function getStoreUsersCount($where = false,  $sort = 'ASC', $sort_field = 'SortOrder', $like = false)
    {
        $this->db->select('COUNT(users.UserID) as UsersCount');
        $this->db->from('users');
        $this->db->join('stores', 'users.StoreID = stores.StoreID', 'LEFT');
        $this->db->join('stores_text', 'stores_text.StoreID = stores.StoreID AND stores_text.SystemLanguageID = 1');
        $this->db->join('roles', 'users.RoleID = roles.RoleID', 'LEFT');
        $this->db->join('roles_text', 'roles.RoleID = roles_text.RoleID AND roles_text.SystemLanguageID = 1');
        $this->db->where('users.RoleID', 3);
        if($where){
            $this->db->where($where);
        }
        if ($like)
        {

            $like = "(users.FirstName LIKE '%$like%' OR users.MiddleName LIKE '%$like%' OR users.LastName LIKE '%$like%' OR users.Email LIKE '%$like%' OR users.Mobile LIKE '%$like%' OR stores_text.Title LIKE '%$like%' OR stores_text.Address LIKE '%$like%')";
            $this->db->where($like);
        }
        
        //$this->db->group_by('users.UserID');
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->row()->UsersCount;
    }

    public function getBlockedUsers($UserID)
    {
        $this->db->select('users.*, users_text.*');
        $this->db->from('users_blocked');
        $this->db->join('users', 'users_blocked.BlockedUserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID', 'LEFT');
        $this->db->where('users_blocked.UserID', $UserID);
        return $this->db->get()->result_array();
    }

    public function getUsersWhereCouponIn($RoleID, $CouponIDs)
    {
        $this->db->select('users.*');
        $this->db->from('users');
        $this->db->where('RoleID', $RoleID);
        if (!empty($CouponIDs)) {
            $this->db->where_in('CouponID', $CouponIDs);
        }
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
        return $result->result_array();


    }

}

?>