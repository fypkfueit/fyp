<?php
Class Dashboard_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getMostOrdersByTechnician()
    {
        $sql = "SELECT cities_text.Title, users.Image, users.UserID, COUNT(bookings.BookingID) AS bookings_count 
                  FROM bookings
                  LEFT JOIN users ON bookings.TechnicianID = users.UserID 
                  LEFT JOIN cities ON users.CityID = cities.CityID 
                  JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
                  WHERE bookings.Status = 5 AND bookings.TechnicianID > 0
                  GROUP BY bookings.TechnicianID 
                  ORDER BY bookings_count DESC LIMIT 10";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getMostOrdersByCategory()
    {
        $sql = "SELECT categories.Image, categories.CategoryID, categories_text.Title, COUNT(bookings.BookingID) AS bookings_count 
                  FROM bookings
                  LEFT JOIN categories ON bookings.CategoryID = categories.CategoryID 
                  JOIN categories_text ON categories.CategoryID = categories_text.CategoryID AND categories_text.SystemLanguageID = 1
                  GROUP BY bookings.CategoryID 
                  ORDER BY bookings_count DESC LIMIT 10";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getTotalSales()
    {
        $sql = "SELECT SUM(booking_invoice.TotalCost) as sales
                  FROM booking_invoice
                  JOIN bookings ON booking_invoice.BookingID = bookings.BookingID
                  WHERE bookings.Status = 5";

        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['sales'] > 0 ? number_format($data['sales'], 2) : 0.00);
    }

    public function getMonthlySales()
    {
        $sql = "SELECT SUM(booking_invoice.TotalCost) as sales
                  FROM booking_invoice
                  JOIN bookings ON booking_invoice.BookingID = bookings.BookingID
                  WHERE bookings.Status = 5 AND MONTH(FROM_UNIXTIME(bookings.BookingTime)) = MONTH(CURRENT_DATE())
                  AND YEAR(FROM_UNIXTIME(bookings.BookingTime)) = YEAR(CURRENT_DATE())";

        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['sales'] > 0 ? number_format($data['sales'], 2) : 0.00);
    }

    public function getWeeklySales()
    {
        $sql = "SELECT SUM(booking_invoice.TotalCost) as sales
                  FROM booking_invoice
                  JOIN bookings ON booking_invoice.BookingID = bookings.BookingID
                  WHERE bookings.Status = 5 AND YEARWEEK(FROM_UNIXTIME(bookings.BookingTime), 1) = YEARWEEK(CURDATE(), 1)";

        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['sales'] > 0 ? number_format($data['sales'], 2) : 0.00);
    }

    public function getTodaysSales()
    {
        $Date_timestamp = time();
        $Date = date('Y-m-d', $Date_timestamp);
        $sql = "SELECT SUM(booking_invoice.TotalCost) as sales
                  FROM booking_invoice
                  JOIN bookings ON booking_invoice.BookingID = bookings.BookingID
                  WHERE bookings.Status = 5 AND DATE_FORMAT(FROM_UNIXTIME(bookings.BookingTime), '%Y-%m-%d') = '$Date'";

        $query = $this->db->query($sql);
        $data = $query->row_array();
        return ($data['sales'] > 0 ? number_format($data['sales'], 2) : 0.00);
    }

}
