<?php

Class Notification_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("notifications");
    }

    public function getNotifications($where, $start = 0, $limit = false, $language = 'EN')
    {
        $this->db->select("notifications.*");
        $this->db->from('notifications');
        

        $this->db->where($where);
        
        $this->db->order_by('notifications.NotificationID', 'DESC');
        if ($limit)
        {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();
    }


    public function getNotificationsWithUsers($where)
    {
        $this->db->select("notifications.*,user_notifications.UserID,user_notifications.NotificationStatus,users.FirstName,users.MiddleName,users.LastName,users.Email");
        $this->db->from('notifications');
        $this->db->join('user_notifications','user_notifications.NotificationID = notifications.NotificationID');
        $this->db->join('users','user_notifications.UserID = users.UserID');
        

        $this->db->where($where);
        
        
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result();
    }


}