<?php
    Class Fyp_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("fyps");

        }



        public function getAllFyps($where = false){
        	$this->db->select("fyps.*,CONCAT(users.FirstName,' ',users.LastName) AS SupervisorName");
        	$this->db->from('fyps');
        	$this->db->join('users','users.UserID = fyps.SupervisorID');
        	if($where){
        		$this->db->where($where);
        	}

        	$this->db->where('fyps.Hide',0);

        	return $this->db->get()->result();
        	

        }


        public function getFypByID($where){
            $this->db->select("fyps.*,CONCAT(supervisor.FirstName,' ',supervisor.LastName) AS SupervisorName,supervisor.Email as SupervisorEmail,CONCAT(partner.FirstName,' ',partner.LastName) AS PartnerName,partner.RegistrationNumber as PartnerRegistrationNumber,partner.Email as PartnerEmail,CONCAT(student.FirstName,' ',student.LastName) AS StudentName,student.RegistrationNumber as StudentRegistrationNumber,student.Email as StudentEmail,departments.Title as DepartmentTitle,programs.Title as ProgramTitle,classes.Title as ClassTitle,sessions.Title as SessionTitle");
            $this->db->from('fyps');
            $this->db->join('users supervisor','supervisor.UserID = fyps.SupervisorID');
            $this->db->join('users partner','partner.UserID = fyps.PartnerID','left');
            $this->db->join('users student','student.UserID = fyps.StudentID','left');
            $this->db->join('departments','departments.DepartmentID = fyps.DepartmentID','left');
            $this->db->join('programs','programs.ProgramID = fyps.ProgramID','left');
            $this->db->join('classes','classes.ClassID = fyps.ClassID','left');
            $this->db->join('sessions','sessions.SessionID = fyps.SessionID','left');


            if($where){
                $this->db->where($where);
            }

            $this->db->where('fyps.Hide',0);

            return $this->db->get()->row_array();
            

        }


    }