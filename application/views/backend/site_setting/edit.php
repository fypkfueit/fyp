<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('EditSiteSettings'); ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/site_setting/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate="">
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="SiteSettingID" value="<?php echo $SiteSettingID; ?>">


                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="SiteName"><?php echo lang('SiteName'); ?>  </label>
                                        <input type="text" class="form-control" name="SiteName" id="SiteName" required value="<?php echo $result->SiteName; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="PhoneNumber"><?php echo lang('PhoneNumber'); ?> </label>
                                        <input type="text" class="form-control" name="PhoneNumber" id="PhoneNumber" value="<?php echo $result->PhoneNumber; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Email"><?php echo lang('email'); ?> </label>
                                        <input type="email" class="form-control" name="Email" id="Email" value="<?php echo $result->Email; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Whatsapp"><?php echo lang('Whatsapp'); ?> </label>
                                        <input type="text" class="form-control" name="Whatsapp" id="Whatsapp" value="<?php echo $result->Whatsapp; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Skype"><?php echo lang('Skype'); ?> </label>
                                        <input type="text" class="form-control" name="Skype" id="Skype" value="<?php echo $result->Skype; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Fax"><?php echo lang('Fax'); ?> </label>
                                        <input type="text" class="form-control" name="Fax" id="Fax" value="<?php echo $result->Fax; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Image"><?php echo lang('SiteLogo'); ?> </label>
                                        <input type="file" class="filestyle" id="Image" name="Image[]" data-placeholder="No Image" accept="image/*">
                                    </div>
                                    <br>
                                    <?php if ($result->SiteImage != '') { ?>
                                        <img src="<?php echo base_url($result->SiteImage); ?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>
                                    <?php } ?>
                                </div>
                            </div>

                            <hr/>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DefaultPackageColorUpper">Default Package Color Upper</label>
                                        <input type="text" class="form-control color_picker" name="DefaultPackageColorUpper" id="DefaultPackageColorUpper" value="<?php echo $result->DefaultPackageColorUpper; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DefaultPackageColorLower">Default Package Color Lower</label>
                                        <input type="text" class="form-control color_picker" name="DefaultPackageColorLower" id="DefaultPackageColorLower" value="<?php echo $result->DefaultPackageColorLower; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DefaultFontColor">Default Font Color</label>
                                        <input type="text" class="form-control color_picker" name="DefaultFontColor" id="DefaultFontColor" value="<?php echo $result->DefaultFontColor; ?>">
                                    </div>
                                </div>
                            </div>
                            <hr/>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="OpenTime">Open Time <small style="color: red;">(GMT Time)</small></label>
                                        <input type="text" class="form-control my_timepicker" name="OpenTime" id="OpenTime" value="<?php echo date('h:i A', $result->OpenTime); ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CloseTime">Close Time <small style="color: red;">(GMT Time)</small></label>
                                        <input type="text" class="form-control my_timepicker" name="CloseTime" id="CloseTime" value="<?php echo date('h:i A', $result->CloseTime); ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="VatNumber">VAT Number </label>
                                        <input type="text" class="form-control" name="VatNumber" id="VatNumber" value="<?php echo $result->VatNumber; ?>">
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="VatPercentage">VAT Percentage </label>
                                        <input type="text" class="form-control number-with-decimals" name="VatPercentage" id="VatPercentage" value="<?php echo $result->VatPercentage; ?>">
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="LoyaltyFactor">Loyalty Factor <small style="color:red;">(Defines 1 SAR is equal to how many loyalty points)</small> </label>
                                        <input type="text" class="form-control number-with-decimals" name="LoyaltyFactor" id="LoyaltyFactor" value="<?php echo $result->LoyaltyFactor; ?>">
                                    </div>
                                </div>
                            </div>

                            <hr/>
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="IOSAppVersion">IOS App Version  </label>
                                        <input type="text" class="form-control" name="IOSAppVersion" id="IOSAppVersion" required value="<?php echo $result->IOSAppVersion; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="AndroidAppVersion">Android App Version</label>
                                        <input type="text" class="form-control" name="AndroidAppVersion" id="AndroidAppVersion" value="<?php echo $result->AndroidAppVersion; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="IOSAppVersion">Merchant IOS App Version  </label>
                                        <input type="text" class="form-control" name="MerchantIOSAppVersion" id="MerchantIOSAppVersion" required value="<?php echo $result->MerchantIOSAppVersion; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="MerchantAndroidAppVersion">Merchant Android App Version</label>
                                        <input type="text" class="form-control" name="MerchantAndroidAppVersion" id="MerchantAndroidAppVersion" value="<?php echo $result->MerchantAndroidAppVersion; ?>">
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="FacebookUrl"><?php echo lang('FacebookUrl'); ?> </label>
                                        <input type="text" class="form-control" name="FacebookUrl" id="FacebookUrl" value="<?php echo $result->FacebookUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="GoogleUrl"><?php echo lang('GoogleUrl'); ?> </label>
                                        <input type="text" class="form-control" name="GoogleUrl" id="GoogleUrl" value="<?php echo $result->GoogleUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="LinkedInUrl"><?php echo lang('LinkedInUrl'); ?> </label>
                                        <input type="text" class="form-control" name="LinkedInUrl" id="LinkedInUrl" value="<?php echo $result->LinkedInUrl; ?>">
                                    </div>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="TwitterUrl"><?php echo lang('TwitterUrl'); ?> </label>
                                        <input type="text" class="form-control" name="TwitterUrl" id="TwitterUrl" value="<?php echo $result->TwitterUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="InstagramUrl">Instagram </label>
                                        <input type="text" class="form-control" name="InstagramUrl" id="InstagramUrl" value="<?php echo $result->InstagramUrl; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group label-floating">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        <?php echo lang('submit'); ?>
                                    </button>



                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>