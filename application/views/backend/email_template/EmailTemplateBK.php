<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- If you delete this tag, the sky will fall on your head -->
    <meta name="viewport" content="width=device-width" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo site_title(); ?></title>

    <style>
        /* -------------------------------------
                        GLOBAL
        ------------------------------------- */
        * {
            margin:0;
            padding:0;
        }
        * { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

        img {
            max-width: 100%;
        }
        .collapse {
            margin:0;
            padding:0;
        }
        body {
            -webkit-font-smoothing:antialiased;
            -webkit-text-size-adjust:none;
            width: 100%!important;
            height: 100%;
        }


        /* -------------------------------------
                        ELEMENTS
        ------------------------------------- */
        a { color: #2BA6CB;}

        .btn {
            text-decoration:none;
            color: #FFF;
            background-color: #666;
            padding:10px 16px;
            font-weight:bold;
            margin-right:10px;
            text-align:center;
            cursor:pointer;
            display: inline-block;
        }

        p.callout {
            padding:15px;
            background-color:#ECF8FF;
            margin-bottom: 15px;
        }
        .callout a {
            font-weight:bold;
            color: #2BA6CB;
        }

        table.social {
            /* 	padding:15px; */
            background-color: #ebebeb;

        }
        .social .soc-btn {
            padding: 3px 7px;
            font-size:12px;
            margin-bottom:10px;
            text-decoration:none;
            color: #FFF;font-weight:bold;
            display:block;
            text-align:center;
        }
        a.fb { background-color: #3B5998!important; }
        a.tw { background-color: #1daced!important; }
        a.gp { background-color: #DB4A39!important; }
        a.ms { background-color: #000!important; }

        .sidebar .soc-btn {
            display:block;
            width:100%;
        }

        /* -------------------------------------
                        HEADER
        ------------------------------------- */
        table.head-wrap { width: 100%;}

        .header.container table td.logo { padding: 15px; }
        .header.container table td.label { padding: 15px; padding-left:0px;}


        /* -------------------------------------
                        BODY
        ------------------------------------- */
        table.body-wrap { width: 100%;}


        /* -------------------------------------
                        FOOTER
        ------------------------------------- */
        table.footer-wrap { width: 100%;	clear:both!important;
            border-spacing: 0px;
        }
        .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
        .footer-wrap .container td.content p {
            font-size:10px;
            font-weight: bold;

        }


        /* -------------------------------------
                        TYPOGRAPHY
        ------------------------------------- */
        h1,h2,h3,h4,h5,h6 {
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
        }
        h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

        h1 { font-weight:200; font-size: 44px;}
        h2 { font-weight:200; font-size: 37px;}
        h3 { font-weight:500; font-size: 27px;}
        h4 { font-weight:500; font-size: 23px;}
        h5 { font-weight:900; font-size: 17px;}
        h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

        .collapse { margin:0!important;}

        p, ul {
            margin-bottom: 10px;
            font-weight: normal;
            font-size:14px;
            line-height:1.6;
        }
        p.lead { font-size:17px; }
        p.last { margin-bottom:0px;}

        ul li {
            margin-left:5px;
            list-style-position: inside;
        }

        /* -------------------------------------
                        SIDEBAR
        ------------------------------------- */
        ul.sidebar {
            background:#ebebeb;
            display:block;
            list-style-type: none;
        }
        ul.sidebar li { display: block; margin:0;}
        ul.sidebar li a {
            text-decoration:none;
            color: #666;
            padding:10px 16px;
            /* 	font-weight:bold; */
            margin-right:10px;
            /* 	text-align:center; */
            cursor:pointer;
            border-bottom: 1px solid #777777;
            border-top: 1px solid #FFFFFF;
            display:block;
            margin:0;
        }
        ul.sidebar li a.last { border-bottom-width:0px;}
        ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}





        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            display:block!important;
            max-width:900px!important;
            margin:0 auto!important; /* makes it centered */
            clear:both!important;
        }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            padding:15px 0px;
            max-width:100%;
            margin:0 auto;
            display:block;
        }


        .content table { width: 100%; }


        /* Odds and ends */
        .column {
            width: 300px;
            float:left;
        }
        .column tr td { padding: 15px; }
        .column-wrap {
            padding:0!important;
            margin:0 auto;
            max-width:600px!important;
        }
        .column table { width:100%;}
        .social .column {
            width: 280px;
            min-width: 279px;
            float:left;
        }

        /* Be sure to place a .clear element after each set of columns, just to be safe */
        .clear { display: block; clear: both; }


        /* -------------------------------------------
                        PHONE
                        For clients that support media queries.
                        Nothing fancy.
        -------------------------------------------- */
        @media only screen and (max-width: 600px) {

            a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

            div[class="column"] { width: auto!important; float:none!important;}

            table.social div[class="column"] {
                width:auto!important;
            }

        }


        /* Nabeel CSS */
        .first_td{
            border-bottom: 1px solid #b8b8b8;
            padding-left: 30px;
        }
        .first_td img{
            width: 250px;
            margin-bottom: 20px;
        }
        .main_tabel_div{
            border: 15px solid #111111;
            width: 96%;
            padding: 30px 0px 0px 0px;
            border-spacing: 0px;
        }
        .second_td{
            padding-left: 30px;
            padding-bottom: 20px;
            padding-right: 30px;
        }
        .second_td .change_color{
            text-align: center;
            float: none;
            color: #b99316;
            font-size: 21px;
            font-weight: bold;
        }
        .second_td .anchor {
            text-align: center;
        }
        .second_td a{
            color: #000;
            font-weight: normal;
            background-color: #d4af37;
            border-radius: 5px;
            padding: 7px 25px;
            display: inline-block;
            text-decoration: none;
            margin-top: 15px;
        }
        .second_td p:nth-child(6){
            font-weight: bold;
            padding-bottom: 20px;
            padding-left: 0px;
            margin-bottom: 0px;
        }
        .second_td p:nth-child(7){
            font-weight: bold;
            padding-bottom: 0px;
            padding-left: 0px;
            width: 100%;
            margin-bottom: 0px;
            display: inline-block;
        }
        .second_td p:nth-child(8){
            font-weight: bold;
            padding-bottom: 3px;
            padding-left: 0px;
            width: 100%;
            display: inline-block;
            margin-bottom: 20px;
        }
        .footer-wrap table{
            width: 100%;
        }
        .footer-wrap p{
            color: #fff;
            margin-bottom: 0px;
            padding: 0px 0px;
            margin-top: 14px;
        }
    </style>

</head>
<body bgcolor="#FFFFFF">
<!-- HEADER -->
<div class="container">
    <div class="main_tabel_div">
        <?php
        $Description = '';
        $DefaultLang = getDefaultLanguage();
        if ($DefaultLang) {
            $lang_id = $DefaultLang->SystemLanguageID;
        } else {
            $lang_id = '1';
        }
        if ($result) {
            foreach ($result as $value) {
                if ($value->SystemLanguageID == $lang_id) {
                    $Description = $value->Description;
                }
            }
        }
        ?>
        <table class="head-wrap">
            <tr>
                <td></td>
                <td class="header">
                    <div class="content">
                        <table>
                            <tr>
                                <td align="left" class="first_td"><img src="<?php echo base_url() . $site_setting->SiteImage; ?>" /></td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
        <!-- /HEADER -->
        <!-- BODY -->
        <table class="body-wrap">
            <tr>
                <td></td>
                <td class="" bgcolor="#FFFFFF">
                    <div class="content">
                        <table>
                            <tr>
                                <td class="second_td">
                                    <?php echo $Description; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
        <!-- /BODY -->
        <!-- FOOTER -->
        <table class="footer-wrap">
            <tr>
                <td></td>
                <td>

                    <table bgcolor="#111111">
                        <tr>
                            <td align="center">
                                <p>
                                    © 2018 CMS.

                                </p>
                            </td>
                        </tr>
                    </table>

                </td>
                <td></td>
            </tr>
        </table>
        <!-- /FOOTER -->
    </div>
</div>
</body>
</html>