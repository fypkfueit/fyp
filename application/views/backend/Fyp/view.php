<?php $result = convertEmptyToNA($result); ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h5 class="card-title alert alert-success"><?php echo $result['Title']; ?></h5>
            </div>

            <?php if($result['IsApprovedBySupervisor'] == 'Rejected'){ ?>
                <div class="col-lg-12 col-md-12">
                    <h5 class="card-title">Supervisor Rejection Reason</h5>
                <?php 

                $reasons = json_decode($result['RejectReasonBySupervisor']);
                if($reasons){
                    foreach ($reasons as $reason) { ?>

                            
                            <p class="alert alert-danger"><?php echo $reason; ?></p>
                        

                    <?php

                    }
                }
                ?>
                </div>
            

        <?php } ?>
            
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5 class="card-title">Student Details</h5>
                                    <p>&nbsp;</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Student Name</label>
                                        <h5>
                                            <?php echo $result['StudentName']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Student Registration Number</label>
                                        <h5><?php echo $result['StudentRegistrationNumber']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Student Email</label>
                                        <h5><?php echo $result['StudentEmail']; ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        


                        
                   
                    </div>
                </div>
            </div>
            <?php if($result['PartnerID'] > 0){ ?>
            <div class="col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5 class="card-title">Partner Details</h5>
                                    <p><b> Approval Status : </b><?php echo $result['IsApprovedByPartner']; ?></p>
                                </div>
                                <?php if($this->session->userdata['admin']['UserID'] == $result['PartnerID'] && $result['IsApprovedByPartner'] == 'Pending'){ ?>

                                <div class="col-sm-6">
                                    <button class="btn btn-success btn-sm"
                                            onclick="Approval('<?php echo $result['FypID']; ?>','Partner','Approved');">
                                        Approve
                                        <div class="ripple-container"></div>
                                    </button>
                                </div>
                                <div class="col-sm-6">
                                    <button class="btn btn-danger btn-sm"
                                            onclick="Approval('<?php echo $result['FypID']; ?>','Partner','Rejected');">
                                        Reject
                                        <div class="ripple-container"></div>
                                    </button>
                                </div>
                            <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        
                        
                        <div class="row">
                            
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Partner Name</label>
                                        <h5>
                                            <?php echo $result['PartnerName']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Partner Registration Number</label>
                                        <h5><?php echo $result['PartnerRegistrationNumber']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Partner Email</label>
                                        <h5><?php echo $result['PartnerEmail']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    


                        
                   
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5 class="card-title">Supervisor Details</h5>
                                    <p><b> Approval Status : </b><?php echo $result['IsApprovedBySupervisor']; ?></p>
                                </div>
                                <?php if($this->session->userdata['admin']['UserID'] == $result['SupervisorID'] && ($result['IsApprovedBySupervisor'] == 'Pending' OR $result['IsApprovedBySupervisor'] == 'Rejected')){ ?>
                                <div class="col-sm-6">
                                    <button class="btn btn-success btn-sm"
                                            onclick="Approval('<?php echo $result['FypID']; ?>','Supervisor','Approved');">
                                        Approve
                                        <div class="ripple-container"></div>
                                    </button>
                                </div>
                                <?php } ?>
                                <?php if($this->session->userdata['admin']['UserID'] == $result['SupervisorID']){ ?>
                                 <div class="col-sm-6">
                                    <button class="btn btn-danger btn-sm"
                                            onclick="Approval('<?php echo $result['FypID']; ?>','Supervisor','Rejected');">
                                        <?php echo (($result['IsApprovedBySupervisor'] == 'Pending' OR $result['IsApprovedBySupervisor'] == 'Approved') ? 'Reject' : 'Reject Again'); ?>
                                        <div class="ripple-container"></div>
                                    </button>
                                </div>
                            <?php } ?>

                            <?php if($this->session->userdata['admin']['UserID'] == $result['SupervisorID'] || $this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 5){ ?>
                                <div class="col-sm-12 checkbox-radios">
                                    <div class="form-group checkbox">
                                    
                                        <label for="CanEdit">
                                                <input  value="1" type="checkbox" data-fyp-id="<?php echo $result['FypID'];?>" data-can-edit-value ="<?php echo $result['StudentCanEdit'];?>" id="CanEdit" <?php echo ($result['StudentCanEdit'] == 1 ? 'checked':''); ?> /> Student Can Edit?
                                        </label>
                                            
                                    </div>
                                </div>

                            
                        <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        


                        <div class="row">
                            
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Supervisor Name</label>
                                        <h5>
                                            <?php echo $result['SupervisorName']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Supervisor Email</label>
                                        <h5><?php echo $result['SupervisorEmail']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                   
                    </div>
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Student Class Detail </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Department</label>
                                        <h5><?php echo $result['DepartmentTitle']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Program</label>
                                        <h5><?php echo $result['ProgramTitle']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Class</label>
                                        <h5><?php echo $result['ClassTitle']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Session</label>
                                        <h5><?php echo $result['SessionTitle']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Student Subject Detail </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Software Engineering 1 Subject Passed ?</label>
                                        <h5><?php echo ($result['IsSoftwareEngineeringSubjectPassed'] == 1 ? 'Yes' : 'No'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Database Passed Subject ?</label>
                                        <h5><?php echo ($result['IsDataBaseSubjectPassed'] == 1 ? 'Yes' : 'No'); ?></h5>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Fyp Details </h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Domains</label>
                                        <h5>
                                           <?php echo DomainsTitle(explode(',',$result['DomainID'])); ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Tools</label>
                                        <h5>
                                            <?php echo $result['Tool']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Summary</label>
                                        <h5>
                                            <?php echo $result['ProjectSummary']; ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<div class="modal fade" id="RejectionModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Fyp Rejection</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/FypApproval" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
      <div class="modal-body">
         
                            <input type="hidden" name="UserType" value="Supervisor">
                            <input type="hidden" name="Status" value="Rejected">
                            <input type="hidden" name="FypID" value="<?php echo $result['FypID']; ?>">
                            <div class="row">

                                <div class="col-md-12">
                                        <label style="color:green" for="RejectReasonBySupervisor">Reason*</label>
                                        <textarea style="color:green" class="form-control" id="RejectReasonBySupervisor" name="RejectReasonBySupervisor" rows="5"></textarea>
                                </div>
                            </div>

        
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
<script>
    function Approval(fyp_id,user_type,status) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });


                    if(user_type == 'Supervisor' && status == 'Rejected'){
                        $.unblockUI();
                        $('#RejectionModel').modal('show');
                        return true;
                    }

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/fyp/FypApproval',
                        data: {
                            'FypID': fyp_id,
                            'UserType': user_type,
                            'Status': status
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            if(result.success){
                                showSuccess(result.success);
                            }else{
                                showError(result.error);
                            }
                            if(result.reload){
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                             }
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }
</script>
<script>

    $(document).ready(function() {
        $('#CanEdit').on('click',function(){
            fyp_id = $(this).attr('data-fyp-id');
            value = $(this).attr('data-can-edit-value');
            $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/fyp/updateCanEdit',
                        data: {
                            'FypID': fyp_id,
                            'value': value
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            if(result.success){
                                showSuccess(result.success);
                            }else{
                                showError(result.error);
                            }
                            if(result.reload){
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                             }
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });

        });
    });
    
</script>