<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Fyp Registrations</h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Fyp Registation</button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th>Registration #</th>
                                    <th>Title</th>
                                    <th>Database Passed</th>
                                    <th>S.E Passed</th>
                                    <th>Supervisor</th>
                                    <th>Approved By Supervisor</th>


                                     
                                            <th><?php echo lang('actions');?></th>
                                    

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value->FypID;?>">

                                            <td><?php echo $value->RegistrationNumber; ?></td>
                                            <td><?php echo $value->Title; ?></td>
                                            <td><?php echo ($value->IsDataBaseSubjectPassed ? lang('yes') : lang('no')); ?></td>
                                            <td><?php echo ($value->IsSoftwareEngineeringSubjectPassed ? lang('yes') : lang('no')); ?></td>
                                            <td><?php echo $value->SupervisorName; ?></td>
                                            <td><?php echo $value->IsApprovedBySupervisor; ?></td>


                                            

                                            
                                            <td>
                                                <a  href="<?php echo base_url('cms/'.$ControllerName.'/view/'.$value->FypID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a>

                                                <?php if(checkRightAccess(78,$this->session->userdata['admin']['RoleID'],'CanEdit')){?>
                                                    <a  href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->FypID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                                <?php if($this->session->userdata['admin']['RoleID'] == 4 && $value->StudentCanEdit == 1){ ?>
                                                    <a  href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->FypID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">edit</i><div class="ripple-container"></div></a>

                                                <?php } ?>


                                                
                                               
                                                <?php if(checkRightAccess(78,$this->session->userdata['admin']['RoleID'],'CanDelete')){?>
                                                    <!--<a title = "Delete" href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->FypID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>-->
                                                <?php } ?>
                                            </td>
                                           
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>