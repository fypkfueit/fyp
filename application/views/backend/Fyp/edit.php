<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <div class="card" style="background-image:url('<?= base_url("assets/kfueit.png");?>'); backface-visibility: 10%; background-repeat: no-repeat;background-size: cover; ">
                    <div class="card-header card-header-icon" data-background-color="#0db14b">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content" style="color: blue;">
                        <h4 class="card-title">Update FYP Registration Form</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data" autocomplete="off"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="FypID" value="<?php echo base64_encode($result['FypID']); ?>">

                            <div class="row">
                            <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label"
                                               for="DepartmentID">Department *</label>
                                        <select id="DepartmentID" class="selectpicker" data-style="select-with-transition"
                                                required name="DepartmentID">
                                                <option value="">Select Department</option>
                                            <?php foreach($departments as $department){ ?>
                                                    <option value="<?php echo $department->DepartmentID; ?>" <?php echo ($department->DepartmentID == $result['DepartmentID'] ? 'selected' : ''); ?>><?php echo $department->Title; ?></option>
                                                    <?php

                                                }
                                                ?>
                                          <!--    <option value="2">DS </option> -->
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label"
                                               for="ProgramID">Program *</label>
                                        <select id="ProgramID" class="selectpicker" data-style="select-with-transition"
                                                required name="ProgramID">
                                               <option value="">Select Program</option>
                                                <?php foreach($programs as $program){ ?>
                                                    <option value="<?php echo $program->ProgramID; ?>" <?php echo ($program->ProgramID == $result['ProgramID'] ? 'selected' : ''); ?>><?php echo $program->Title; ?></option>
                                                    <?php

                                                }
                                                ?>

                                           
                                           
                                        </select>
                                    </div>
                                </div>
                                
                                
                            </div>
                            <div class="row">
                                
                                <div class="col-md-5">
                                <div class="form-group label-floating">
                                        <label class="control-label"
                                               for="ClassID">Class *</label>
                                        <select id="ClassID" class="selectpicker" data-style="select-with-transition"
                                                required name="ClassID">
                                                <option value="">Select Class</option>
                                                <?php foreach($classes as $class){ ?>
                                                    <option value="<?php echo $class->ClassID; ?>" <?php echo ($class->ClassID == $result['ClassID'] ? 'selected' : ''); ?>><?php echo $class->Title; ?></option>
                                                    <?php

                                                }
                                                ?>
                                           
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="col-md-5">
                                <div class="form-group label-floating">
                                        <label class="control-label"
                                               for="SessionID">Session *</label>
                                        <select id="SessionID" class="selectpicker" data-style="select-with-transition"
                                                required name="SessionID">

                                           <option value="">Select Session</option>
                                            <?php foreach($sessions as $session){ ?>
                                                    <option value="<?php echo $session->SessionID; ?>" <?php echo ($session->SessionID == $result['SessionID'] ? 'selected' : ''); ?>><?php echo $session->Title; ?></option>
                                                    <?php

                                                }
                                                ?>
                                        </select>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="RegistrationNumber">Registration Number *</label>
                                        <input type="text" name="RegistrationNumber" parsley-trigger="change" required
                                               class="form-control" id="RegistrationNumber" readonly value="<?php echo $result['StudentRegistrationNumber']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"> Tools </label>
                                        <input type="text" name="Tool" parsley-trigger="change" required
                                               class="form-control" id="Tool" value="<?php echo $result['Tool']; ?>">
                                    </div>
                                </div>
                            </div>
                              
                            
                            
                            
                            
                            
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="PromoCodes">Domains</label>
                                        <select id="PromoCodes" class="selectpicker" data-style="select-with-transition" name="DomainID[]" multiple>
                                            <?php 
                                                $selected_domains = explode(',',$result['DomainID']);

                                            foreach($domains as $domain){ 

                                                ?>
                                                    <option value="<?php echo $domain->DomainID; ?>" <?php echo ((in_array($domain->DomainID, $selected_domains)) ? 'selected' : ''); ?>><?php echo $domain->Title; ?></option>
                                                    <?php

                                                }
                                                ?>
                                        </select>
                                    </div>
                               </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label"
                                               for="SupervisorID">Supervisor *</label>
                                        <select id="SupervisorID" class="selectpicker" data-style="select-with-transition"
                                                required name="SupervisorID">
                                                <option value="">Select Supervisor</option>
                                            <?php foreach($supervisors as $supervisor){ ?>
                                                    <option value="<?php echo $supervisor->UserID; ?>" <?php echo ($supervisor->UserID == $result['SupervisorID'] ? 'selected' : ''); ?>><?php echo $supervisor->FirstName.' '.$supervisor->LastName; ?></option>
                                                    <?php

                                                }
                                                ?>
                                          <!--    <option value="2">DS </option> -->
                                        </select>
                                    </div>
                                </div>
                            

                            
                            </div>
                            <div class="row">
                                
                                    
                                    
                                        <div class="col-md-5 form-group">
                                            <div class="form-group checkbox checkbox-radios">
                                                <label for="HasPartner">
                                                    <input name="HasPartner" value="1" type="checkbox" id="HasPartner" <?php echo ($result['PartnerID'] > 0 ? 'checked':''); ?>/> Has Partner?
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-5 Partner" style="<?php echo ($result['PartnerID'] > 0 ? 'display: block;':'display: none;'); ?>">
                                            <div class="form-group label-floating">
                                                 <label class="control-label" for="PartnerRegistrationNumber"> Partner Registration Number </label>
                                                <input type="text" name="PartnerRegistrationNumber" parsley-trigger="change"
                                               class="form-control" id="PartnerRegistrationNumber" value="<?php echo $result['PartnerRegistrationNumber']; ?>">
                                            </div>
                                           
                                         </div>
                                            
                               
                        </div>

                             <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Project Title</label>
                                        <input type="text" name="Title" parsley-trigger="change" required
                                               class="form-control" id="Title" value="<?php echo $result['Title']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-10">
                                        <label style="color:green" for="ProjectSummary">Add Project Summary </label>
                                        <textarea style="color:green" class="form-control" id="ProjectSummary" name="ProjectSummary" rows="5"><?php echo $result['ProjectSummary']; ?></textarea>
                                </div>
                            </div>

                          
                            
                            
                        <div class="row">
                                <div class="col-sm-8 checkbox-radios">
                                    <div class="form-group label-floating">
                                    <h6>Mark these subjects Passed </h6>
                                        <div class="col-sm-8 checkbox">
                                            <label for="IsDataBaseSubjectPassed">
                                                <input name="IsDataBaseSubjectPassed" value="1" type="checkbox" id="IsDataBaseSubjectPassed" <?php echo ($result['IsDataBaseSubjectPassed'] == 1 ? 'checked':''); ?> /> Database
                                            </label>
                                            <br>
                                            </div>
                                            <div class="col-sm-8 checkbox">
                                            <label for="IsSoftwareEngineeringSubjectPassed">
                                                <input name="IsSoftwareEngineeringSubjectPassed" value="1" type="checkbox" id="IsSoftwareEngineeringSubjectPassed" <?php echo ($result['IsSoftwareEngineeringSubjectPassed'] == 1 ? 'checked':''); ?> /> Software Engineering 1
                                            </label></div>
                                        </div>
                                </div>
                        </div>


                        

                        
                                
                     </div>    
                           
                            
                            <div class="col-sm-12">
                                <div class="form-group text-right mr-sm-2">
                                    <button class="btn btn-success waves-effect waves-light" type="submit">
                                        Update
                                    </button>
                                    
                                    <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>">
                                        <button type="button" class="btn btn-info waves-effect m-l-5">
                                            <?php echo lang('back'); ?>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/fyp_custom.js"></script>