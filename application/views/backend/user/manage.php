<div class="content"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>

                    </div>
                    <div class="card-content">

                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#admins" data-toggle="tab" aria-expanded="true">Admins</a>
                            </li>
                            <li class="">
                                <a href="#badge_users" data-toggle="tab" aria-expanded="false">Batch Advisors</a>
                            </li>
                            <li class="">
                                <a href="#supervisor_verified" data-toggle="tab" aria-expanded="false">Supervisors</a>
                            </li>
                            <li class="">
                                <a href="#students_verified" data-toggle="tab" aria-expanded="false">Students</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="toolbar">
                                    <a href="<?php echo base_url('cms/' . $ControllerName . '/add'); ?>">
                                        <button type="button"
                                                class="btn btn-primary waves-effect w-md waves-light m-b-5">Add
                                            User
                                        </button>
                                    </a>
                                </div>
                            <div class="material-datatables tab-pane active" id="admins">
                                <h4 class="card-title">Admins</h4>
                                
                                <table id="admin_ajax_based_datatable" class="table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>Mobile No.</th>
                                        <th><?php echo lang('is_active'); ?></th>

                                        <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanView')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="badge_users">
                                <h4 class="card-title">Badge Advisors</h4>
                                <div class="toolbar">
                                   
                              </div>
                                <table id="badge_ajax_based_datatable" class="table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>Mobile No.</th>
                                        <th><?php echo lang('is_active'); ?></th>

                                        <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanView')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    

                                    </tbody>
                                </table>
                            </div>
                            <div class="material-datatables tab-pane" id="supervisor_verified">
                                <h4 class="card-title">Supervisors</h4>
                                <div class="toolbar">
                                   
                              </div>
                                <table id="supervisor_ajax_based_datatable" class="table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>Mobile No.</th>
                                        <th><?php echo lang('is_active'); ?></th>

                                        <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanView')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    

                                    </tbody>
                                </table>
                                   
                                
                            </div>
                            <div class="material-datatables tab-pane" id="students_verified">
                                <h4 class="card-title">Students</h4>
                                <div class="toolbar">
                                    <!--<a href="<?php /*echo base_url('cms/' . $ControllerName . '/add'); */?>">
                                        <button type="button"
                                                class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Admin
                                            User
                                        </button>
                                    </a>-->
                                </div>
                                <table id="students_ajax_based_datatable" class="table table-striped table-no-bordered table-hover"
                                       cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>Mobile No.</th>
                                        <th><?php echo lang('is_active'); ?></th>

                                        <?php if (checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanEdit') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanDelete') || checkRightAccess(23, $this->session->userdata['admin']['RoleID'], 'CanView')) { ?>
                                            <th><?php echo lang('actions'); ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $(document).ready(function () {
        $('table.datatable').DataTable({
            "ordering": true
        });
    });
    $(document).ready(function() {
     var ajax_based_datatable = $('#admin_ajax_based_datatable').DataTable( {
         "processing": true,
         "serverSide": true,
         stateSave: true,
         "ajax": base_url + "cms/user/ajax_users/1",
         "columns": [
             { "data": "User Name" },
             { "data": "Email" },
             { "data": "Mobile", "orderable": false  },
             { "data": "Publish" , "orderable": false, "searchable": false},
             { "data": "Action", "orderable": false, "searchable": false  }
         ]
     });


     var badge_ajax_based_datatable = $('#badge_ajax_based_datatable').DataTable( {
         "processing": true,
         "serverSide": true,
         stateSave: true,
         "ajax": base_url + "cms/user/ajax_users/2",
         "columns": [
             { "data": "User Name" },
             { "data": "Email" },
             { "data": "Mobile", "orderable": false  },
             { "data": "Publish" , "orderable": false, "searchable": false},
             { "data": "Action", "orderable": false, "searchable": false  }
         ]
     });

     var supervisor_ajax_based_datatable = $('#supervisor_ajax_based_datatable').DataTable( {
         "processing": true,
         "serverSide": true,
         stateSave: true,
         "ajax": base_url + "cms/user/ajax_users/3",
         "columns": [
             { "data": "User Name" },
             { "data": "Email" },
             { "data": "Mobile", "orderable": false  },
             { "data": "Publish" , "orderable": false, "searchable": false},
             { "data": "Action", "orderable": false, "searchable": false  }
         ]
     });
     var students_ajax_based_datatable = $('#students_ajax_based_datatable').DataTable( {
         "processing": true,
         "serverSide": true,
         stateSave: true,
         "ajax": base_url + "cms/user/ajax_users/4",
         "columns": [
             { "data": "User Name" },
             { "data": "Email" },
             { "data": "Mobile", "orderable": false  },
             { "data": "Publish" , "orderable": false, "searchable": false},
             { "data": "Action", "orderable": false, "searchable": false  }
         ]
     });



 } );
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>