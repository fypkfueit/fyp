<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('change_password');?></h4>
                      
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" autocomplete="off"  data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update_password">
                            <input type="hidden" name="UserID" value="<?php echo $UserID; ?>">
                            <input type="hidden" name="RoleID" value="<?php echo $result->RoleID; ?>">
                            
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Password">New Password <?php echo lang('min_length'); ?></label>
                                        <input type="password" name="Password" parsley-trigger="change" required  class="form-control" id="Password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ConfirmPassword"><?php echo lang('confirm_password'); ?></label>
                                        <input type="password" name="ConfirmPassword" parsley-trigger="change" required  class="form-control" id="ConfirmPassword">
                                    </div>
                                </div>

                            </div>


                           <div class="row">

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>
                        </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>