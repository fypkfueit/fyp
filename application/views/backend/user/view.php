<?php
$user = convertEmptyToNA($user);
if ($user->IsActive == 0) {
    $is_active_msg = 'Activate';
    $is_active_cls = "btn btn-success";
    $is_active_val = 1;
} elseif ($user->IsActive == 1) {
    $is_active_msg = 'Deactivate';
    $is_active_cls = "btn btn-danger";
    $is_active_val = 0;
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="row">
                                <div class="col-sm-9">
                                    <h5>User Details</h5>
                                </div>
                                <div class="col-sm-3">
                                    <?php
                                    if ($user->IsVerified == 0) { ?>
                                        <a href="javascript:void(0);"
                                           onclick="VerifyUser( '<?php echo $user->UserID; ?>', '1');">
                                            <button class="btn btn-success">
                                                Verify
                                                <div class="ripple-container"></div>
                                            </button>
                                        </a>
                                    <?php }
                                    ?>
                                    <a href="javascript:void(0);"
                                       onclick="UserActive('<?php echo $user->UserID; ?>', '<?php echo $is_active_val; ?>');">
                                        <button class="<?php echo $is_active_cls; ?>">
                                            <?php echo $is_active_msg; ?>
                                            <div class="ripple-container"></div>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">First Name</label>
                                        <h5><?php echo $user->FirstName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Middle Name</label>
                                        <h5><?php echo $user->MiddleName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Last Name</label>
                                        <h5><?php echo $user->LastName; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Email</label>
                                        <h5><?php echo $user->Email; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Mobile No.</label>
                                        <h5><?php echo $user->Mobile; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Card Title</label>
                                        <h5><?php echo $user->CardTitle; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">City</label>
                                        <h5><?php echo $user->CityTitle; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Role</label>
                                        <h5><?php echo $user->RoleTitle; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Card Number</label>
                                        <h5><?php echo $user->CardNumber !== 'N/A' ? $user->CardNumber : 'N/A'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Verified?</label>
                                        <h5><?php echo $user->IsVerified == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Is Active?</label>
                                        <h5><?php echo $user->IsActive == 1 ? 'Yes' : 'No'; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Device Type</label>
                                        <h5><?php echo $user->DeviceType; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Device OS</label>
                                        <h5><?php echo $user->OS; ?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Transaction ID</label>
                                        <h4><?php echo $user->TransactionID; ?></h4>
                                    </div>
                                </div>
                            </div>
                            <?php $transaction_status = verifyPaytabsPayment($user->TransactionID); ?>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Transaction Amount</label>
                                        <h4>
                                            <?php echo isset($transaction_status->amount) ? number_format($transaction_status->amount, 2) : 'N/A'; ?>&nbsp;
                                            <?php echo isset($transaction_status->currency) ? $transaction_status->currency : 'N/A'; ?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Transaction Card Last 4 Digits</label>
                                        <h4><?php echo isset($transaction_status->card_last_four_digits) ? $transaction_status->card_last_four_digits : 'N/A'; ?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Expiry Date</label>
                                        <h4><?php echo showDateFromString($user->ExpiryDate); ?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">Transaction Message</label>
                                        <h4><?php echo isset($transaction_status->result) ? $transaction_status->result : 'N/A'; ?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <div class="form-line">
                                        <label class="control-label">ID Image</label>
                                        <?php
                                        if ($user->IDImage !== 'N/A') { ?>
                                            <a href="<?php echo base_url($user->IDImage); ?>" data-fancybox="gallery"
                                               target="_blank"><img src="<?php echo base_url($user->IDImage); ?>"
                                                                    alt="ID Image" style="height: 30%;width: 30%;"></a>
                                        <?php } else { ?>
                                            <h5>N/A</h5>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function VerifyUser(user_id, val) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/user/updateIsVerified',
                        data: {
                            'UserID': user_id,
                            'IsVerified': val
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }

    function UserActive(user_id, val) {
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: function () {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: base_url + '' + 'cms/user/updateIsActive',
                        data: {
                            'UserID': user_id,
                            'IsActive': val
                        },
                        dataType: "json",
                        cache: false,
                        success: function (result) {
                            showSuccess(result.success);
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                },
                cancel: function () {

                }
            }
        });
    }

    $(document).ready(function () {
        $('.fancybox').fancybox({
            beforeShow: function () {
                this.title = $(this.element).data("caption");
            }
        });
    }); // ready
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>