<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/favicon.ico" type="image/x-icon">
    <!-- App title -->
    <title>KFUEIT | <?php echo site_title(); ?></title>
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/backend/plugins/summernote/summernote.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/backend/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo base_url(); ?>assets/backend/css/material-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/demo.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <!--     Fonts and icons     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/font-awesome.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/backend/css/google-roboto-300-700.css" rel="stylesheet"/>
    <!--   Core JS Files   -->


    <!-- filer -->
    <!-- <link href="<?php echo base_url(); ?>assets/backend/plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/backend/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" />-->

    <!--end filer -->
    <script src="<?php echo base_url(); ?>assets/backend/js/jquery-3.1.1.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>assets/backend/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/material.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/perfect-scrollbar.jquery.min.js"
            type="text/javascript"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css"/>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7vMfxgK_KPChKiJkCwnjn7m_J5h7hNlM&libraries=places&region=sa"></script>
    <!--<script src="https://js.pusher.com/4.4/pusher.min.js"></script>-->

    <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEhNO5iT3tbOsqtCrEiECHa8_Scky7zfQ&libraries=places"></script>-->

</head>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var delete_msg = '<?php echo lang('are_you_sure');?>';
</script>
<style>
    .sidebar[data-active-color="rose"] li.active > a, .off-canvas-sidebar[data-active-color="rose"] li.active > a {
        background-color: #886c1e;
        box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px #886c1e;

    .validate_error {
        border: 1px solid red;
    }

    #validation-msg {
        visibility: hidden;
        min-width: 250px;
        margin-left: -125px;
        text-align: center;
        border-radius: 2px;
        padding: 16px;
        position: fixed;
        z-index: 9999;
        left: 50%;
        top: 80px;
        font-size: 17px;
    }

    #validation-msg.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 4.5s;
        animation: fadein 0.5s, fadeout 0.5s 4.5s;
    }

    .panel-default {
        border: none !important;
    }

</style>
<body>
<div class="wrapper">
        
  
  