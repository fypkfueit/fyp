<?php $products = productsPosted(); ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-icon" data-background-color="purple">
            <i class="material-icons">shop</i>
        </div>
        <div class="card-content">
            <h4 class="card-title">Products Posted</h4>
            <ul class="nav nav-pills nav-pills-warning">
                <li class="active">
                    <a href="#products_most_selling" data-toggle="tab" aria-expanded="true">Most Selling</a>
                </li>
                <li class="">
                    <a href="#products_least_selling" data-toggle="tab" aria-expanded="false">Least Selling</a>
                </li>
                <li class="">
                    <a href="#products_most_liked" data-toggle="tab" aria-expanded="false">Most Liked</a>
                </li>

            </ul>

            <div class="tab-content">
                <div class="material-datatables tab-pane active" id="products_most_selling">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>

                            <th>Product Title</th>
                            <th>Image</th>
                            <th>Purchase Count</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($products['most_selling_products']) {
                            foreach ($products['most_selling_products'] as $product) {
                                $product_images = getProductImages($product->ProductID);
                                ?>
                                <tr id="<?php echo $product->ProductID; ?>">
                                    <td><?php echo $product->Title; ?></td>
                                    <td>
                                        <?php
                                        if (isset($product_images[0]->ProductImage) && $product_images[0]->ProductImage != '') { ?>
                                            <a data-fancybox="gallery" href="<?php echo base_url($product_images[0]->ProductImage); ?>" target="_blank"
                                               title="Click to view in new tab"><img
                                                        src="<?php echo base_url($product_images[0]->ProductImage); ?>"
                                                        style="width: 80px !important;height: 80px !important;"></a>
                                        <?php } else { ?>
                                            <h5>N/A</h5>
                                        <?php }
                                        ?>
                                    </td>
                                    <td><?php echo $product->OrderCount; ?></td>
                                    <td><a href="<?php echo base_url('cms/product/view/' . $product->ProductID); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <div class="material-datatables tab-pane" id="products_least_selling">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>

                            <th>Product Title</th>
                            <th>Image</th>
                            <th>Purchase Count</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($products['least_selling_products']) {
                            foreach ($products['least_selling_products'] as $product) {
                                $product_images = getProductImages($product->ProductID);
                                ?>
                                <tr id="<?php echo $product->ProductID; ?>">
                                    <td><?php echo $product->Title; ?></td>
                                    <td>
                                        <?php
                                        if (isset($product_images[0]->ProductImage) && $product_images[0]->ProductImage != '') { ?>
                                            <a data-fancybox="gallery" href="<?php echo base_url($product_images[0]->ProductImage); ?>" target="_blank"
                                               title="Click to view in new tab"><img
                                                        src="<?php echo base_url($product_images[0]->ProductImage); ?>"
                                                        style="width: 80px !important;height: 80px !important;"></a>
                                        <?php } else { ?>
                                            <h5>N/A</h5>
                                        <?php }
                                        ?>
                                    </td>
                                    <td><?php echo $product->OrderCount; ?></td>
                                    <td><a href="<?php echo base_url('cms/product/view/' . $product->ProductID); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <div class="material-datatables tab-pane" id="products_most_liked">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>

                            <th>Product Title</th>
                            <th>Image</th>
                            <th>Like Count</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($products['most_liked_products']) {
                            foreach ($products['most_liked_products'] as $product) {
                                $product_images = getProductImages($product->ProductID);
                                ?>
                                <tr id="<?php echo $product->ProductID; ?>">
                                    <td><?php echo $product->Title; ?></td>
                                    <td>
                                        <?php
                                        if (isset($product_images[0]->ProductImage) && $product_images[0]->ProductImage != '') { ?>
                                            <a data-fancybox="gallery" href="<?php echo base_url($product_images[0]->ProductImage); ?>" target="_blank"
                                               title="Click to view in new tab"><img
                                                        src="<?php echo base_url($product_images[0]->ProductImage); ?>"
                                                        style="width: 80px !important;height: 80px !important;"></a>
                                        <?php } else { ?>
                                            <h5>N/A</h5>
                                        <?php }
                                        ?>
                                    </td>
                                    <td><?php echo $product->LikeCount; ?></td>
                                    <td><a href="<?php echo base_url('cms/product/view/' . $product->ProductID); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
            </div><!-- tab-content -->
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
</div>