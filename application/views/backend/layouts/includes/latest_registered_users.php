<?php $latest = getLatestUsers(); ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-icon" data-background-color="purple">
            <i class="material-icons">face</i>
        </div>
        <div class="card-content">
            <h4 class="card-title">New Registrations</h4>
            <ul class="nav nav-pills nav-pills-warning">
                <li class="active">
                    <a href="#booths" data-toggle="tab" aria-expanded="true">Booths</a>
                </li>
                <li class="">
                    <a href="#users" data-toggle="tab" aria-expanded="false">Users</a>
                </li>

            </ul>

            <div class="tab-content">
                <div class="material-datatables tab-pane active" id="booths">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>

                            <th>Booth Name</th>
                            <th>City</th>
                            <th>Email</th>
                            <th>Mobile No.</th>
                            <th><?php echo lang('is_active'); ?></th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($latest['booths']) {
                            foreach ($latest['booths'] as $booth) { ?>
                                <tr id="<?php echo $booth->UserID; ?>">
                                    <td><?php echo $booth->BoothName; ?></td>
                                    <td><?php echo $booth->CityTitle; ?></td>
                                    <td><?php echo $booth->Email; ?></td>
                                    <td><?php echo $booth->Mobile; ?></td>
                                    <td><?php echo($booth->IsActive ? lang('yes') : lang('no')); ?></td>
                                    <td><a href="<?php echo base_url('cms/user/view/' . base64_encode($booth->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <div class="material-datatables tab-pane" id="users">
                    <table id="" class="datatable table table-striped table-no-bordered table-hover"
                           cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>

                            <th>User Name</th>
                            <th>City</th>
                            <th>Email</th>
                            <th>Mobile No.</th>
                            <th><?php echo lang('is_active'); ?></th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($latest['users']) {
                            foreach ($latest['users'] as $user) { ?>
                                <tr id="<?php echo $user->UserID; ?>">
                                    <td><?php echo $user->FullName; ?></td>
                                    <td><?php echo $user->CityTitle; ?></td>
                                    <td><?php echo $user->Email; ?></td>
                                    <td><?php echo $user->Mobile; ?></td>
                                    <td><?php echo($user->IsActive ? lang('yes') : lang('no')); ?></td>
                                    <td><a href="<?php echo base_url('cms/user/view/' . base64_encode($booth->UserID)); ?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View">visibility</i><div class="ripple-container"></div></a></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
            </div><!-- tab-content -->
        </div>
        <!-- end content-->
    </div>
    <!--  end card  -->
</div>