<?php $site_setting = site_settings(); ?>
<div class="sidebar" data-active-color="rose" data-background-color="black">
    <!--
Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
Tip 2: you can also add an image using data-image tag
Tip 3: you can change the color of the sidebar with data-background-color="white | black"
-->

    <!--<div class="logo">
        <a href="<?php /*echo base_url('cms/dashboard'); */ ?>" class="simple-text">
            <?php /*echo $site_setting->SiteName; */ ?>
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="<?php /*echo base_url('cms/dashboard'); */ ?>" class="simple-text">
            <?php /*echo $site_setting->SiteName; */ ?>
        </a>
    </div>-->
    <div class="sidebar-wrapper scrollbar" id="style-1">
        <div class="force-overflow">
            <div class="user">
                <div class="photo" style="background-image:url('<?= base_url("assets/logo.jpg");?>'); background-size: cover;">
                    <?php /*if($this->session->userdata['admin']['Image'] == ''){
                            $image = base_url('assets/backend/img/no_img.png');
                        }else{
                            $image = base_url($this->session->userdata['admin']['Image']);
                        }
                        */ ?><!--
                        <img src="<?php /*echo $image;*/ ?>" />-->
                    <!--<img src="<?php echo site_logo(); ?>">-->
                </div>
                <div class="info">
                    <a href="javascript:void(0);" class="collapsed">
                        <?php echo site_title(); ?>
                    </a>
                </div>
            </div>
            <?php
            $url_module = $this->uri->segment(2);
            //$url = explode('cms/', $_SERVER['REQUEST_URI']);
            //$url_module = $url[1];
            ?>
            <ul class="nav">
                <li class="<?php echo($url_module == 'dashboard' ? 'active' : ''); ?>">
                        <a href="<?php echo base_url('cms/dashboard'); ?>">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                </li>

                <?php

                //$modules = getAllActiveModules($this->session->userdata['admin']['RoleID'],$language,'modules.ParentID = 0 AND modules.IsActive = 1');
                $modules = getAllActiveModules($this->session->userdata['admin']['RoleID'], $language, 'modules.ParentID = 0 AND modules.IsActive = 1',true);

                if (!empty($modules)) {
                    foreach ($modules as $value) {
                        // $getParentUserAccess = getActiveUserModule($this->session->userdata['admin']['UserID'],$language,'modules.ParentID = 0 AND modules.IsActive = 1 AND modules.ModuleID = '.$value->ModuleID);

                        if ($value['CanView'] == 1 || $value['CanAdd'] == 1 || $value['CanEdit'] == 1 || $value['CanDelete'] == 1) {

                            //$child_modules = getAllActiveModules($this->session->userdata['admin']['RoleID'],$language,'modules.ParentID = '.$value->ModuleID.' AND modules.IsActive = 1');

                            $child_modules = getAllActiveModules($this->session->userdata['admin']['RoleID'], $language, 'modules.ParentID = ' . $value['ModuleID'] . ' AND modules.IsActive = 1',true);
                            if (!empty($child_modules)) {

                                $sub = array_column($child_modules, 'Slug');
                                // print_rm($sub);
                                // $getParent = getAllActiveModules($this->session->userdata['admin']['RoleID'],$language,"modules.Slug = '".$this->uri->segment(2)."' AND modules.IsActive = 1");
                                if (in_array($url_module, $sub)) {
                                    echo '<script>$( document ).ready(function() {
                                         setTimeout(function(){  $("#parent' . $value['ModuleID'] . '").click(); }, 500);
  
});</script>';
                                }

                                ?>

                                <li>
                                    <a data-toggle="collapse" href="#<?php echo $value['ModuleID']; ?>"
                                       id="parent<?php echo $value['ModuleID']; ?>">
                                        <i class="material-icons"><?php echo $value['IconClass']; ?></i>
                                        <p><?php echo $value['ModuleTitle']; ?>
                                            <b class="caret"></b>
                                        </p>
                                    </a>


                                    <?php


                                    if (!empty($child_modules)) { ?>

                                        <div class="collapse" id="<?php echo $value['ModuleID']; ?>">
                                            <ul class="nav">


                                                <?php
                                                foreach ($child_modules as $child_value) {
                                                    //$getChildUserAccess = getActiveUserModule($this->session->userdata['admin']['UserID'],$language,'modules.ParentID = '.$value->ModuleID.' AND modules.IsActive = 1 AND modules.ModuleID = '.$child_value->ModuleID);
                                                    if (($child_value['CanView'] == 1 || $child_value['CanAdd'] == 1 || $child_value['CanEdit'] == 1 || $child_value['CanDelete'] == 1)) { ?>


                                                        <li class="<?php echo($url_module == $child_value['Slug'] ? 'active' : ''); ?>">
                                                            <a href="<?php echo base_url('cms/' . $child_value['Slug']); ?>"><?php echo $child_value['ModuleTitle']; ?></a>
                                                        </li>
                                                    <?php }
                                                } ?>

                                            </ul>
                                        </div>

                                    <?php } ?>
                                </li>
                            <?php } else { ?>

                                <li class="<?php echo($url_module == $value['Slug'] ? 'active' : ''); ?>">
                                    <a href="<?php echo base_url('cms/' . $value['Slug']); ?>">
                                        <i class="material-icons"><?php echo $value['IconClass']; ?></i>
                                        <p><?php echo $value['ModuleTitle']; ?></p>
                                    </a>
                                </li>

                            <?php } ?>

                            <?php
                        }
                    }
                }


                ?>


                <li>
                    <a href="<?php echo base_url('cms/account/logout'); ?>">
                        <i class="material-icons">lock</i>
                        <p><?php echo lang('logout'); ?></p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="main-panel">
    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-minimize">
                <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                    <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                    <i class="material-icons visible-on-sidebar-mini">view_list</i>
                </button>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <a class="navbar-brand" href="#"> Dashboard </a> -->
            </div>

            <!--<div class="edit-permission">
                <div class="togglebutton">
                    <label><input id="toggleEditPermissions" type="checkbox"></label>
                    <span class="txt">Edit Mode</span>
                </div>
            </div>-->


            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!--<li>
                        <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">dashboard</i>
                            <p class="hidden-lg hidden-md">Dashboard</p>
                        </a>
                    </li>-->
                    <?php $notifications = get_notifications(); ?>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">notifications</i>
                            <?php
                            if ($notifications['result_count'] > 0) { ?>
                            
                                <span class="notification notification_count"><?php echo  $notifications['result_count']; ?></span>
                            <?php } ?>
                           
                            <p class="hidden-lg hidden-md">
                                Notifications
                                <b class="caret"></b>
                            </p>
                        </a>
                        <ul class="dropdown-menu notification_item">
                            <?php
                            if ($notifications['result']) {
                                foreach ($notifications['result'] as $notification) { ?>
                                    <li>
                                        <a href="<?php echo base_url('cms/notification/view') . '/' . $notification->NotificationID; ?>">
                                            <?php echo $notification->Description; ?>
                                        </a>
                                    </li>
                                <?php }
                            }else{ ?>

                                <li><a href="javascript:void(0);">No notifications to show</a></li>
                           <?php } ?>
                            

                           

                           
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url('cms/account/logout') ?>" class="dropdown-toggle"
                           title="Click here to logout">
                            <i class="material-icons">input</i>
                            <p class="hidden-lg hidden-md">Logout</p>
                        </a>
                    </li>
                    <li class="separator hidden-lg hidden-md"></li>
                </ul>
                <!--<form class="navbar-form navbar-right" role="search">
                    <div class="form-group form-search is-empty">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="material-input"></span>
                    </div>
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </form>-->
            </div>
        </div>
    </nav>


