<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-content">
                        
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="row hide">
            <div class="col-lg-3 col-md-6 col-sm-6">
                Coming Soon
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">shopping_cart</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Total Orders</p>
                        <h3 class="card-title"><?php /*echo $TotalOrdersCount; */?></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="rose">
                        <i class="material-icons">watch_later</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Today's Orders</p>
                        <h3 class="card-title"><?php /*echo $TodayOrdersCount; */?></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">assignment_turned_in</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Completed Orders</p>
                        <h3 class="card-title"><?php /*echo $CompletedOrdersCount; */?></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="red">
                        <i class="material-icons">alarm</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Overdue Orders</p>
                        <h3 class="card-title"><?php /*echo $OverdueOrdersCount; */?></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row hide">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="purple">
                        <i class="material-icons">monetization_on</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Total Sales</p>
                        <h3 class="card-title"><?php /*echo $total_sales; */?></h3> SAR
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">monetization_on</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Today's Sales</p>
                        <h3 class="card-title"><?php /*echo $todays_sales; */?></h3> SAR
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="blue">
                        <i class="material-icons">monetization_on</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Sales This Week</p>
                        <h3 class="card-title"><?php /*echo $weekly_sales; */?></h3> SAR
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="rose">
                        <i class="material-icons">monetization_on</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Sales This Month</p>
                        <h3 class="card-title"><?php /*echo $monthly_sales; */?></h3> SAR
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {

        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

        demo.initVectorMap();
    });
</script>

<script>
    $(document).ready(function () {
        $('table.datatable').DataTable({
            "ordering": true
        });
    });
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>