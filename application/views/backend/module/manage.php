<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="purple">
                                    <i class="material-icons">assignment</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                                    <div class="toolbar hide">
                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                           <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                                        </a>          
                                    </div>
                                    <div class="material-datatables">
                                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                             <thead>
                                        <tr>
                                            <th><?php echo lang('title');?></th>
                                            <th><?php echo lang('slug');?></th>
                                            <th><?php echo lang('is_active');?></th>
                                            <th><?php echo lang('actions');?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($results){
                                                foreach($results as $value){ ?>
                                                    <tr id="<?php echo $value->ModuleID;?>">
                                                    <td><?php echo $value->Title; ?></td>
                                                    <td><?php echo $value->Slug; ?></td>
                                                    <td><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                                    <td>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->ModuleID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i><div class="ripple-container"></div></a>
                                                            <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->ModuleID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                                        </td> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- end content-->
                            </div>
                            <!--  end card  -->
                        </div>
                        <!-- end col-md-12 -->
                    </div>
                    <!-- end row -->
                </div>
            </div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>




             