<head>
	<link rel="stylesheet" media="all" href="css/log.in.css">
</head>
<body class="bg form-loader">
  <div class="big-logo">
    <a href="index.html">
      <img src="images/logo.png" style="height: 60px;" class="logo" alt="">
    </a>
  </div>

  <!-- Login Start Here -->
  <div class="login-box custom-min-height">
    <form id="loginForm" name="loginForm"  novalidate="novalidate">
      <div class="group">
        <input type="email" name="emailId" placeholder="Email Address">
      </div>

      <div class="group last">
        <input type="password" name="password" placeholder="Password">
      </div>

      <div class="content">
        <a href="#forgotPasswordModal" data-toggle="modal" class="pull-left">Forgot Password ?</a>
        <a href="register.php" class="pull-right" style="direction: rtl;" >Register</a>
      </div>

      <button type="submit"  class="btn submit">Login</button>
    </form>
  </div>
  <!-- Login End Here -->
  <!-- Modal -->
  <div id="forgotPasswordModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Forgot Password ?</h4>
        </div>
        <div class="modal-body">
          <form name="forgotPasswordForm" id="forgotPasswordForm" novalidate="novalidate">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label translate="login.emailid">Email Address</label>
                  <input class="form-control" type="text" name="email" id="forgotUsername" required="">
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn save">Submit</button>
        </div>
      </div>
    </div>
  <div class=""></div><div class=""><div class="default-wrapper" style="top: 0px; left: 0px; right: 0px; bottom: 0px;">

   <div class="cg-busy-default-sign">

      <div class="cg-busy-default-spinner">
         <div class="bar1"></div>
         <div class="bar2"></div>
         <div class="bar3"></div>
         <div class="bar4"></div>
         <div class="bar5"></div>
         <div class="bar6"></div>
         <div class="bar7"></div>
         <div class="bar8"></div>
         <div class="bar9"></div>
         <div class="bar10"></div>
         <div class="bar11"></div>
         <div class="bar12"></div>
      </div>

      <div class="cg-busy-default-text"></div>

   </div>

</div></div></div>

  
  <!-- END Modal -->
  
 
 
<!-- Start unauthorized Modal -->

<div class="modal fade" id="unauthorizedModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title ">Warning Message</h4>
        </div>
        <div class="modal-body">
          <p style="font-size:17px;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn got-it " >Ok</button>
        </div>
      </div>
    </div>
  </div>

<!-- end unauthorized Modal -->



<!-- Successs Modal -->
<div id="forgotsuccessModal" class="modal success-modal" data-easein="expandIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Info</h4>
        </div>
        <div class="modal-body">
          <p style="font-size:17px;">Request sent successfully, you'll receive an email shortly: </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn got-it ">Ok</button>
        </div>
      </div>
    </div>
  </div>

<!-- END Successs Modal -->
<!-- Jquery -->
  <script src="plugins/jquery/jquery.min.js"></script>

  <!-- Angular -->
  

  <!-- jQuery-slimscroll -->
  <script src="plugins/jQuery-slimScroll/js/jquery.slimscroll.js"></script>

  <!-- jquery validator -->
  <script src="plugins/validation/jquery.validate.min.js"></script>

  <!-- Bootstrap-select   -->
  <script src="plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

  <!-- Bootstrap   -->
  <script src="plugins/bootstrap/js/bootstrap.min.js"></script>

  <!-- Wow -->
  <script src="plugins/wow.min.js"></script>

  <!-- Script -->
  <script src="assets/js/script.js"></script>

  <!-- angular busy -->
  

  <!-- Toast -->
 

  <!-- loading bar -->


  <!-- angular translate -->
  

  <script src="assets/js/service/commonService.js"></script>
  <script src="assets/js/controllers/loginCtrl.js"></script>
<div class="" ></div><div class=""><div class="cg-busy-default-wrapper" style=" top: 0px; left: 0px; right: 0px; bottom: 0px;">

   <div class="cg-busy-default-sign">

      <div class="cg-busy-default-spinner">
         <div class="bar1"></div>
         <div class="bar2"></div>
         <div class="bar3"></div>
         <div class="bar4"></div>
         <div class="bar5"></div>
         <div class="bar6"></div>
         <div class="bar7"></div>
         <div class="bar8"></div>
         <div class="bar9"></div>
         <div class="bar10"></div>
         <div class="bar11"></div>
         <div class="bar12"></div>
      </div>

      <div class="cg-busy-default-text ng-binding"></div>

   </div>

</div></div></body>
</body>





