<style>
    .card .card-content p {
        height: auto !important
    }
</style>
<section id="news-details" class="news-details">
    <div class="heading text-center">
        <h1 class="heading-text " id="owlDynamicHeading">Blog Detail</h1>
    </div>
    <div class="inner-news-details">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 blogs-item ">
                    <div class="card">
                        <a href="javascript:void(0)">
                            <div class="card-header">
                                <div class="blog-box">
                                    <img alt="" style="width:100%" src="<?php echo base_url($blog->Image); ?>">
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="user-list">
                                    <div>
                                        <div class="list-action-left">
                                            <img src="<?php echo frontend_assets(); ?>images/defaultUserBig.png" alt="">

                                        </div>
                                        <div class="list-content">
                                            <span class="title "><?php echo $blog->AddedFromFrontend == 1 ? $blog->Name : getUserInfo($blog->CreatedBy, true); ?></span>
                                            <span class="caption "><?php echo date('d/m/Y', strtotime($blog->CreatedAt)); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3>Title</h3>
                    <h1><?php echo $blog->Title; ?></h1>
                    <?php echo $blog->Description; ?>
                    <div class="text-right">
                        <a href="<?php echo base_url('blogs'); ?>" class="btn ">Go Back To Blogs</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>