﻿<style>

    .card .card-content p {height:auto !important}
</style>
<section id="blogs" class="blogs">
    <div class="heading text-center">
        <h1 class="heading-text " id="owlDynamicHeading">NEWS &amp; EVENTS</h1>
    </div>
    <div class="inner-news-events">
        <div class="container">
            <div class="row news-list">
                <?php
                foreach ($news_events as $news_event) { ?>
                    <div class="col-md-4 col-sm-6 blogs-item ">
                        <div class="card">
                            <a href="<?php echo base_url('news-detail?id=' . $news_event->NewsID); ?>">
                                <div class="card-header">
                                    <div class="blog-box">
                                        <img alt="" style="width:100%"
                                             src="<?php echo base_url($news_event->Image); ?>">
                                    </div>
                                </div>
                                <div class="card-content">
                                    <h1><?php echo $news_event->Title; ?></h1>
                                    <?php echo $news_event->Description; ?>
                                    <div class="user-list">
                                        <div>
                                            <div class="list-action-left"><img alt=""
                                                                               src="<?php echo frontend_assets(); ?>images/defaultUserBig.png">
                                            </div>
                                            <div class="list-content">
                                                <span class="title "><?php echo getUserInfo($news_event->CreatedBy, true); ?></span>
                                                <span class="caption "><?php echo date('d/m/Y', strtotime($news_event->CreatedAt)); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php }
                ?>
            </div>
        </div>
    </div>
</section>