﻿<style>

    .blogs .card .card-content p {
        height: 0px !important;
    }

    .card .card-content p {
        height: 0px !important
    }

    .blogs .card .card-content .user-list div .list-content .title a {
        font-size: 17px;
        opacity: 0.5;
        color: #333;
    }
</style>
<section id="blogs" class="blogs">
    <div class="container">
        <div class="row blogs-list">
            <?php
            foreach ($blogs as $blog) { ?>
            <div>
                <div class="col-md-4 col-sm-6 blogs-item ">
                    <div class="card">
                        <a href="<?php echo base_url('blog-detail?id=' . $blog->BlogID); ?>">
                            <div class="card-header">
                                <div class="blog-box">
                                    <img alt="" style="width:100%"
                                         src="<?php echo base_url($blog->Image); ?>">
                                </div>
                            </div>
                            <div class="card-content">
                                <h1><?php echo $blog->Title; ?></h1>
                                <?php echo $blog->Description; ?>
                                <div class="user-list">
                                    <div>
                                        <div class="list-action-left"><img alt="" src="<?php echo frontend_assets(); ?>images/defaultUserBig.png">
                                        </div>
                                        <div class="list-content">
                                            <span class="title "><?php echo $blog->AddedFromFrontend == 1 ? $blog->Name : getUserInfo($blog->CreatedBy, true); ?></span>
                                            <span class="caption "><?php echo date('d/m/Y', strtotime($blog->CreatedAt)); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="blog-buttons">
                    <a href="#articleModal" class="btn upload-article " data-toggle="modal">Upload Article</a>
                    <a href="https://www.surveymonkey.com/r/CustomerEng" class="btn upload-article " target="_blank">Take
                        Survey</a>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="articleModal" class="modal fade blog-modal form-loader" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title ">ADD NEW ARTICLE</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('index/addBlog'); ?>" method="POST" class="ajaxForm" name="blogForm" id="blogForm" novalidate="novalidate" onsubmit="return false;">
                    <h3>Basic Information</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control required" type="text" name="Name"
                                       placeholder="(First,Middle,Last)" id="fullName" required="">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input class="form-control required" type="text" name="Email" id="email"
                                       placeholder="john@gmail.com" required="">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Mobile No</label>
                                <input class="form-control required" type="text" name="Mobile" id="mobileNo" required="">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Company</label>
                                <input class="form-control " type="text" name="Company" id="companyName"
                                       required="">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Position</label>
                                <input class="form-control " type="text" name="Position" id="position" required="">
                            </div>
                        </div>
                    </div>

                    <h3>Blog Information</h3>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Blog Title</label>
                                <input class="form-control required" type="text" name="Title" id="title" required="">
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="upload-file">
                                <label style="font-size:17px;color:#333;font-weight: 400;">Upload File</label>
                                <div class="attach-icon">
                                    <div class="input-file-container">
                                        <input class="input-file" accept=".jpg, .jpeg, .png, .JPG, .JPEG, .PNG,mp4"
                                               ngf-select="uploadFile($file)" name="Image" id="uploadFile" type="file"
                                               required="" data-error="#fileuploadError">

                                        <label tabindex="0" for="uploadFile" class="input-file-trigger"><i
                                                    class="icon-attachment"></i></label>
                                    </div>
                                </div>
                                <span class="filePlaceholder " style="color: #999; top: 54%;font-size: 16px;">JPG,JPEG,PNG,MP4,AVI,FLV</span>
                                <p class="file-return"><span id="file-attachment"></span>
                                <div class="cg-busy cg-busy-backdrop cg-busy-backdrop-animation "></div>
                                <div class="cg-busy cg-busy-animation ">
                                    <div class="cg-busy-default-wrapper"
                                         style="position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px;">

                                        <div class="cg-busy-default-sign">

                                            <div class="cg-busy-default-spinner">
                                                <div class="bar1"></div>
                                                <div class="bar2"></div>
                                                <div class="bar3"></div>
                                                <div class="bar4"></div>
                                                <div class="bar5"></div>
                                                <div class="bar6"></div>
                                                <div class="bar7"></div>
                                                <div class="bar8"></div>
                                                <div class="bar9"></div>
                                                <div class="bar10"></div>
                                                <div class="bar11"></div>
                                                <div class="bar12"></div>
                                            </div>

                                            <div class="cg-busy-default-text"></div>

                                        </div>

                                    </div>
                                </div>
                                </p>
                                <span id="fileuploadError"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group article">
                                <label>Enter Article</label>
                                <textarea class="autoExpand animated abc form-control required" rows="3" data-min-rows="3"
                                          type="text" cols="50" name="Description" id="description" maxlength="500"
                                          required="" data-error="#articleError"></textarea>
                                <span class="maxlength">0/500</span>
                                <span id="articleError"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn save" onclick="$('#blogForm').submit();">Submit</button>
            </div>
        </div>
    </div>
    <div class="cg-busy cg-busy-backdrop cg-busy-backdrop-animation "></div>
    <div class="cg-busy cg-busy-animation ">
        <div class="cg-busy-default-wrapper" style="position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px;">

            <div class="cg-busy-default-sign">

                <div class="cg-busy-default-spinner">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                    <div class="bar4"></div>
                    <div class="bar5"></div>
                    <div class="bar6"></div>
                    <div class="bar7"></div>
                    <div class="bar8"></div>
                    <div class="bar9"></div>
                    <div class="bar10"></div>
                    <div class="bar11"></div>
                    <div class="bar12"></div>
                </div>

                <div class="cg-busy-default-text "></div>

            </div>

        </div>
    </div>
</div>