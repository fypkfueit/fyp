<div class="main-wishlist form-loader"  style="position: relative;">
    <div class="main">

      <div class="content bg-white contentWithFixPadding">
        <div class="wishlist">
          <div class="breadcrumb-area">
            <ol class="breadcrumb text-center">
              <li class="breadcrumb-item active " >WISHLIST</li>
            </ol>
          </div>
          <div class="wishlist-card mb30 form-loader bg-grey">
            <form name="wishListForm" id="wishListForm"   novalidate="novalidate">
              <div class="">
                <h3 class="m-b-20">Brand Information</h3>
                <div class="row m-b-30">
                  <div class="col-xs-12">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th></th>
                            <th class="text-center">Brand Name<span style="color: red;"> *</span>
                              <p class="caption ">(e.g. Fridays)</p>
                            </th>
                            <th style="padding-top: 0!important;" class="text-center ">Brand Category<span style="color: red;"> *</span>

                            </th>
                            <th class="text-center ">Branch Location<span style="color: red;"> *</span>
                              <p class="caption ">(e.g. Saudi Arabia/ Jeddah)</p>
                            </th>
                            <th class="text-center ">Brand Manager Name <span style="color: red;"> *</span>
                              <p class="caption ">(e.g. Mr. Mohammed Abdullah)</p>
                            </th>
                            <th style="padding-top: 0!important;" class="text-center ">Brand Manager Contact<span style="color: red;"> *</span>

                            </th>
                            <th style="padding-top: 0!important;" class="text-center ">Manger Email Address<span style="color: red;"> *</span>

                            </th>
                          </tr>
                        </thead>
                        <tbody>
                       <tr >
                            <td class="text-center ">1</td>
                            <td class="text-center"><input type="text" class="form-control " name="brandName" ></td>
                            <td class="text-center">
                              <select class="form-control">
                              	<option>All</option>
								<option>Food</option>
								<option>Beauty</option>
								<option>Health</option>
								<option>Shopping</option>
								<option>Education</option>
								<option>Leisure</option>
								<option>Services</option>
								<option>Travel</option>
								<option>Professional</option>
                              </select>
                            </td>
                            <td class="text-center"><input type="text" class="form-control " name="branchLocation" ></td>
                            <td class="text-center"><input type="text" class="form-control " name="managerName" ></td>
                            <td class="text-center"><input type="text" class="form-control " name="contactNo" ></td>
                            <td class="text-center"><input type="text" class="form-control" name="email" placeholder="john@gmail.com"></td>
                          </tr>
                           <tr>
                            <td class="text-center ">2</td>
                            <td class="text-center"><input type="text" class="form-control " name="brandName"></td>
                            <td class="text-center">
                              <select class="form-control">
                              	<option>All</option>
								<option>Food</option>
								<option>Beauty</option>
								<option>Health</option>
								<option>Shopping</option>
								<option>Education</option>
								<option>Leisure</option>
								<option>Services</option>
								<option>Travel</option>
								<option>Professional</option>
                              </select>
                            </td>
                            <td class="text-center"><input type="text" class="form-control" name="branchLocation" ></td>
                            <td class="text-center"><input type="text" class="form-control " name="managerName" ></td>
                            <td class="text-center"><input type="text" class="form-control" name="contactNo" ></td>
                            <td class="text-center"><input type="text" class="form-control " name="email" placeholder="john@gmail.com" ></td>
                          </tr>
							<tr>
                            <td class="text-center ">3</td>
                            <td class="text-center"><input type="text" class="form-control " name="brandName"></td>
                            <td class="text-center">
                              <select class="form-control">
                              	<option>All</option>
								<option>Food</option>
								<option>Beauty</option>
								<option>Health</option>
								<option>Shopping</option>
								<option>Education</option>
								<option>Leisure</option>
								<option>Services</option>
								<option>Travel</option>
								<option>Professional</option>
                              </select>
                            </td>
                            <td class="text-center"><input type="text" class="form-control " name="branchLocation" ></td>
                            <td class="text-center"><input type="text" class="form-control" name="managerName" ></td>
                            <td class="text-center"><input type="text" class="form-control " name="contactNo" ></td>
                            <td class="text-center"><input type="text" class="form-control " name="email" placeholder="john@gmail.com" ></td>
                          </tr>
							<tr >
                            <td class="text-center ">4</td>
                            <td class="text-center"><input type="text" class="form-control " name="brandName" ></td>
                            <td class="text-center">
                              <select class="form-control">
                              	<option>All</option>
								<option>Food</option>
								<option>Beauty</option>
								<option>Health</option>
								<option>Shopping</option>
								<option>Education</option>
								<option>Leisure</option>
								<option>Services</option>
								<option>Travel</option>
								<option>Professional</option>
                              </select>
                            </td>
                            <td class="text-center"><input type="text" class="form-control " name="branchLocation"></td>
                            <td class="text-center"><input type="text" class="form-control " name="managerName" ></td>
                            <td class="text-center"><input type="text" class="form-control " name="contactNo" ></td>
                            <td class="text-center"><input type="text" class="form-control " name="email" placeholder="john@gmail.com"></td>
                          </tr>
                           <tr>
                            <td class="text-center ">5</td>
                            <td class="text-center"><input type="text" class="form-control" name="brandName"></td>
                            <td class="text-center">
                              <select class="form-control">
                              	<option>All</option>
								<option>Food</option>
								<option>Beauty</option>
								<option>Health</option>
								<option>Shopping</option>
								<option>Education</option>
								<option>Leisure</option>
								<option>Services</option>
								<option>Travel</option>
								<option>Professional</option>
                              </select>
                            </td>
                            <td class="text-center"><input type="text" class="form-control " name="branchLocation" ></td>
                            <td class="text-center"><input type="text" class="form-control " name="managerName" ></td>
                            <td class="text-center"><input type="text" class="form-control " name="contactNo"></td>
                            <td class="text-center"><input type="text" class="form-control " name="email" placeholder="john@gmail.com" ></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
          <div  class="basic-form ">
                <h3 >Basic Information</h3>
            <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="">Name</label>
                      <input class="form-control " type="text" placeholder="(First,Middle,Last)" >
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="">Email Address</label>
                      <input class="form-control " type="email" >
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="">Mobile No</label>
                      <input class="form-control " type="text" id="mobileNumber" >
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="">City</label>
                      <input class="form-control" type="text" >
                    </div>
                  </div>
                </div>
              </div>
              <div class="row m-b-30">
                <div class="col-xs-12">
                  <div class="vwt-CheckboxWrapper">
                    <input class="vwt-Checkbox" type="checkbox" name="terms" id="notify" data-error="#termsError">
                    <label class="vwt-CheckboxLabel" for="notify" name="terms">
                  <span class="circle"></span>
                  <span class="check"></span>
                  <span class="box"></span>
                    <label id="termsText" class="">Please notify me when one of these brands join your network

                    </label>
                    <span id="termsError" style="font-size: 14px;"></span>
                  </label></div>
                 
                </div>
              </div>
              <div class="text-right">
                <a class="btn submit " >Submit</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    
    </div>
 </div>