<style>
    .card .card-content p {
        height: auto !important
    }
</style>
<section id="news-details" class="news-details">
    <div class="heading text-center">
        <h1 class="heading-text " id="owlDynamicHeading">New & Events Detail</h1>
    </div>
    <div class="inner-news-details">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 blogs-item ">
                    <div class="card">
                        <a href="javascript:void(0)">
                            <div class="card-header">
                                <div class="blog-box">
                                    <img alt="" style="width:100%" src="<?php echo base_url($news_event->Image); ?>">
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="user-list">
                                    <div>
                                        <div class="list-action-left">
                                            <img src="<?php echo frontend_assets(); ?>images/defaultUserBig.png" alt="">

                                        </div>
                                        <div class="list-content">
                                            <span class="title "><?php echo getUserInfo($news_event->CreatedBy, true); ?></span>
                                            <span class="caption "><?php echo date('d/m/Y', strtotime($news_event->CreatedAt)); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3>Title</h3>
                    <h1><?php echo $news_event->Title; ?></h1>
                    <?php echo $news_event->Description; ?>
                    <div class="text-right">
                        <a href="<?php echo base_url('news-events'); ?>" class="btn ">Go Back To News</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>