<?php $contact_us = getPageData(2, $language); ?>
<style>
    .contact-us:before {
        background-image: url(<?php echo base_url($contact_us->Image); ?>); !important;
    }
</style>
<section id="contact-us" class="contact-us form-loader" >
    <div class="contact-box">
      <div class="inner-box">
        <div class="inquiries-cell">
          <ul>
            <li>
              <div class="left-item">
                <i class="fa fa-envelope-o"></i>
              </div>
              <div class="right-item">
                <h2  >Customers inquiries</h2>
                <h3><?php echo $contact_us->Email; ?></h3>
              </div>
            </li>
            <li>
              <div class="left-item">
                <i class="fa fa-envelope-o"></i>
              </div>
              <div class="right-item">
                <h2 >Merchants inquiries</h2>
                <h3><?php echo $contact_us->Facebook; ?></h3>
              </div>
            </li>
            <li>
              <div class="left-item">
                <i class="fa fa-envelope-o"></i>
              </div>
              <div class="right-item">
                <h2  >Other inquiries</h2>
                <h3><?php echo $contact_us->Linkedin; ?></h3>
              </div>
            </li>
            <li>
              <div class="left-item">
                <i class="fa fa-phone"></i>
              </div>
              <div class="right-item">
                <h2  >Toll Free</h2>
                <h3><?php echo $contact_us->Instagram; ?></h3>
              </div>
            </li>
            <li>
              <div class="left-item">
                <i class="fa fa-mobile"></i>
              </div>
              <div class="right-item">
                <h2  >Mobile</h2>
                <h3><?php echo $contact_us->Twitter; ?></h3>
              </div>
            </li>
          </ul>
		  </div>
      </div>
    </div>
  <div class="cg-busy cg-busy-backdrop cg-busy-backdrop-animation"></div><div class="cg-busy cg-busy-animation"><div class="cg-busy-default-wrapper" style="position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px;">

   <div class="cg-busy-default-sign">

      <div class="cg-busy-default-spinner">
         <div class="bar1"></div>
         <div class="bar2"></div>
         <div class="bar3"></div>
         <div class="bar4"></div>
         <div class="bar5"></div>
         <div class="bar6"></div>
         <div class="bar7"></div>
         <div class="bar8"></div>
         <div class="bar9"></div>
         <div class="bar10"></div>
         <div class="bar11"></div>
         <div class="bar12"></div>
      </div>

      <div class="cg-busy-default-text"></div>

   </div>

</div></div></section>