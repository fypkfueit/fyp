<section id="about-us" class="about-us">
    <div class="container">
        <div class="heading text-center">
            <h1 class="heading-text">Merchant Promo Codes</h1>
        </div>
        <div class="content">
            <table class="" cellspacing="0" width="100%"
                   style="width:100%;">
                <thead>
                <tr>
                    <th>Merchant Title</th>
                    <th>Merchant Promo Code</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($sponsored_stores)) {
                    foreach ($sponsored_stores as $store) { ?>
                        <tr>
                            <td><?php echo $store->Title; ?></td>
                            <td><?php echo $store->MerchantPromoCode; ?></td>
                        </tr>
                        <?php
                    }
                } else { ?>
                    <tr>
                        <td class="alert alert-danger" colspan="2" style="text-align: center;">No data found!</td>
                    </tr>
                <?php }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
