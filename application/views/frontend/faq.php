<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>

    .faq .tabs-left .tab-content .panel-group .panel-default .panel-heading .panel-title > a.collapsed .glyphicon-plus:before {
        content: "\002b";
    }

    .faq .tabs-left .tab-content .panel-group .panel-default .panel-heading .panel-title > a .glyphicon-plus:before {
        content: "\2212";
    }
</style>
<section id="faq" class="faq">
    <div class="container">


        <div class="tabbable tabs-left">
            <div class="row">
                <div class="col-md-3">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <?php
                        $i = 0;
                        foreach ($faqs_categories as $faqs_category) { ?>
                            <li role="presentation" class="<?php echo($i == 0 ? 'active' : ''); ?>"><a
                                        href="#tab<?php echo $faqs_category->FaqID; ?>"
                                        aria-controls="tab<?php echo $faqs_category->FaqID; ?>" role="tab"
                                        data-toggle="tab"><?php echo $faqs_category->Title; ?></a></li>
                            <?php $i++;
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-md-9">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php
                        $i = 0;
                        foreach ($faqs_categories as $faqs_category) {
                            $faqs_questions = getFaqsInCategory($faqs_category->FaqID, $language);
                            ?>
                            <div role="tabpanel" class="tab-pane <?php echo($i == 0 ? 'active' : ''); ?>"
                                 id="tab<?php echo $faqs_category->FaqID; ?>">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <?php
                                    $j = 1;
                                    foreach ($faqs_questions as $question) {
                                        $title = str_replace(' ', '_', $question->Title);
                                        ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading_<?php echo $title; ?>_<?php echo $j; ?>">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse_<?php echo $title; ?>_<?php echo $j; ?>" aria-expanded="true"
                                                       aria-controls="collapse_<?php echo $title; ?>_<?php echo $j; ?>">
                                                        <?php echo $j; ?>) <?php echo $question->Title; ?> <br>
                                                        <i class="pull-right glyphicon  glyphicon-plus"
                                                           style="    top: -15px"></i>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_<?php echo $title; ?>_<?php echo $j; ?>" class="panel-collapse collapse " role="tabpanel"
                                                 aria-labelledby="heading_<?php echo $title; ?>_<?php echo $j; ?>">
                                                <div class="panel-body">
                                                    <?php echo $question->Description; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $j++;
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php $i++;
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
</section>