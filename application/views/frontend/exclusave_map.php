<script type="text/javascript"
        src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false&key=AIzaSyA0OIAdWfb-dgdzhkOhLRT8YrOB0XP7-hU"></script>
<div class="main-exclusave-map">

    <div class="main">
        <div class="content contentWithFixPadding">
            <div class="exclusave-map">

                <div class="breadcrumb-area">
                    <ol class="breadcrumb text-center">
                        <li class="breadcrumb-item active ">EXCLUSAVE MAP</li>
                    </ol>
                </div>

                <div class="inner-exclusave-map">
                    <div id="myMap" style="width: 100%;height: 503px;"></div>
                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    var markers = [];
    var center_lat = <?php echo(isset($stores[0]) ? $stores[0]->Latitude : 21.484716); ?>;
    var center_lng = <?php echo(isset($stores[0]) ? $stores[0]->Longitude : 39.189606); ?>;
    var map = new google.maps.Map(document.getElementById('myMap'), {
        zoom: 13,
        center: new google.maps.LatLng(center_lat, center_lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    <?php if ($stores && count($stores) > 0)
    { ?>
    var infowindow = new google.maps.InfoWindow();
    var marker;
    <?php
    $i = 0;
    foreach ($stores as $store)
    { ?>
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(<?php echo $store->Latitude; ?>, <?php echo $store->Longitude; ?>),
        // icon: 'http://maps.google.com/mapfiles/ms/icons/green.png',
        // icon: '<?php echo base_url('assets/favicon.ico'); ?>',
        map: map
    });
    google.maps.event.addListener(marker, 'click', (function (marker) {
        return function () {
            infowindow.setContent('<h5 style="color:black;"><?php echo str_replace("'", '', $store->Title) . ', ' . str_replace("'", '', $store->CityTitle); ?></h5>');
            infowindow.open(map, marker);
        }
    })(marker));
    markers.push(marker);
    <?php $i++; } ?>
    <?php } ?>
    function myClick(index) {
        google.maps.event.trigger(markers[index], 'click');
    }
</script>