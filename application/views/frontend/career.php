﻿<style>

    [type="date"] {
        background: #fff url(<?php echo frontend_assets(); ?>images/calendar_2.png) 97% 50% no-repeat;
    }

    [type="date"]::-webkit-inner-spin-button {
        display: none;
    }

    [type="date"]::-webkit-calendar-picker-indicator {
        opacity: 0;
    }

</style>
<?php $career = getPageData(9, $language); ?>
<section class="career" style="position: relative;">

    <div class="banner"></div>

    <div class="container">
        <div class="title pt30 pb30">
            <span class="text-left ">CAREER</span>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table class="table career-table mb30">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Job Title</th>
                        <th class="text-center ">City</th>
                        <th class="text-center ">Date Posted</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach ($careers as $career) { ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td class="hoverItem ">
                                <?php echo $career->Title; ?>
                                <div class="itemContent">
                                    <?php echo $career->Description; ?>
                                </div>
                            </td>
                            <td class="text-center"><?php echo $career->City; ?></td>
                            <td class="text-center"><?php echo date('d-m-Y', strtotime($career->CreatedAt)); ?></td>
                        </tr>
                        <?php $i++;
                    }
                    ?>
                    </tbody>
                </table>
                <div class="panel panel-default collapse in" id="demo">
                    <div class="panel-heading ">Fill Form</div>
                    <div class="panel-body">
                        <form action="<?php echo base_url('index/careerApply'); ?>" method="POST"
                              class="ajaxForm form-horizontal m-tb-30" id="careerForm" name="careerform"
                              onsubmit="return false;">
                            <div class="form-group">
                                <label class="control-label col-sm-2">First Name &nbsp;:</label>
                                <div class="col-sm-5">
                                    <input type="text" name="FirstName" class="form-control required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Last Name &nbsp;:</label>
                                <div class="col-sm-5">
                                    <input type="text" name="LastName" class="form-control required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Gender &nbsp;:</label>
                                <div class="col-sm-5">
                                    <div style="display: inline-block;">
                                        <input type="radio" id="Male" name="Gender" value="Male" checked>
                                        <label for="Male" class="radio-inline">Male&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                    </div>
                                    <div style="display: inline-block;">
                                        <input type="radio" id="Female" name="Gender" value="Female">
                                        <label for="Female" class="radio-inline">Female&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 ">Date of Birth &nbsp;:</label>
                                <div class="col-sm-5">
                                    <div class="date-time">
                                        <input type="text" class="form-control required datepicker" name="DOB" data-date-end-date="0d">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 ">Job Type &nbsp;:</label>
                                <div class="col-sm-5">
                                    <select class="form-control required" name="JobType" id="sel1">
                                        <?php foreach ($careers as $career) { ?>
                                            <option value="<?php echo $career->Title; ?>"><?php echo $career->Title; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-5">
                                    <p class="jobType ">
                                        "Ambassador, Volunteer, Employee"
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 ">City &nbsp;:</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control required" name="City">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 ">Mobile No &nbsp;:</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control required" name="Mobile">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Email Address &nbsp;:</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control required" name="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2 ">Upload CV &nbsp;:</label>
                                <div class="col-sm-5">
                                    <div class="upload-file form-control">
                                        <div class="attach-icon">
                                            <div class="input-file-container">
                                                <input class="input-file" accept=".pdf, .doc, .docx" name="CV"
                                                       id="uploadFile" type="file" required="">
                                                <label tabindex="0" for="uploadFile" class="input-file-trigger"><i
                                                            class="fa fa-ellipsis-h" aria-hidden="true"></i></label>
                                            </div>
                                        </div>
                                        <p id="file-attachment" class="text-left m0"></p>
                                        <div class="cg-busy cg-busy-backdrop cg-busy-backdrop-animation  "></div>
                                        <div class="cg-busy cg-busy-animation  ">
                                            <div style=" top: 0px; left: 0px; right: 0px; bottom: 0px;">

                                                <div class="cg-busy-default-sign">

                                                    <div class="cg-busy-default-spinner">
                                                        <div class="bar1"></div>
                                                        <div class="bar2"></div>
                                                        <div class="bar3"></div>
                                                        <div class="bar4"></div>
                                                        <div class="bar5"></div>
                                                        <div class="bar6"></div>
                                                        <div class="bar7"></div>
                                                        <div class="bar8"></div>
                                                        <div class="bar9"></div>
                                                        <div class="bar10"></div>
                                                        <div class="bar11"></div>
                                                        <div class="bar12"></div>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div id="file"></div>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="button" class="btn submit" onclick="$('#careerForm').submit();">Send
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div></div>
    <div>
        <div style=" top: 0px; left: 0px; right: 0px; bottom: 0px;">


        </div>
    </div>
</section>
