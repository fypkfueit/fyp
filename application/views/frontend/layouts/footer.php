<?php $site_settings = site_settings(); ?>
<footer class="footer">
    <div class="footer-widget-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="footer-menu">
                        <ul class="bs-docs-footer-links">
                            <li class="<?php echo($view == 'about_us' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('about-us'); ?>">ABOUT US</a>
                            </li>
                            <li class="<?php echo($view == 'news_events' || $view == 'news_detail' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('news-events'); ?>">NEWS &amp; EVENTS</a>
                            </li>
                            <li class="<?php echo($view == 'exclusave_map' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('exclusave-map'); ?>">EXCLUSAVE MAP</a>
                            </li>
                            <li class="<?php echo($view == 'wishlist' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('wishlist'); ?>">WISHLIST</a>
                            </li>
                            <li class="<?php echo($view == 'career' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('career'); ?>">CAREER</a>
                            </li>
                            <li class="<?php echo($view == 'contact_us' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('contact-us'); ?>">CONTACT US</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="col-md-4">
                        <div class="right-item">
                            <ul class="social-icons">
                                <li><a class="twitter" href="<?php echo $site_settings->TwitterUrl; ?>" target="_blank"><i
                                                class="fa fa-twitter"></i></a></li>
                                <li><a class="facebook" href="<?php echo $site_settings->FacebookUrl; ?>"
                                       target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="instagram" href="<?php echo $site_settings->InstagramUrl; ?>"
                                       target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-dismissible text-center cookiealert" role="alert">
        <div class="cookiealert-container">
            <?php echo lang('cookies_usage_alert'); ?>
            <a href="http://cookiesandyou.com/" target="_blank"><?php echo lang('learn_more'); ?></a>
            <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
                <?php echo lang('got_it'); ?>
            </button>
        </div>
    </div>
</footer>
<script src="<?php echo frontend_assets(); ?>js/jquery.min.js"></script>
<script src="<?php echo frontend_assets(); ?>js/jquery.slimscroll.js"></script>
<script src="<?php echo frontend_assets(); ?>js/bootstrap-select.min.js"></script>
<script src="<?php echo frontend_assets(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo frontend_assets(); ?>js/wow.min.js"></script>
<script src="<?php echo frontend_assets(); ?>js/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js"></script>
<script src="<?php echo frontend_assets(); ?>plugins/cookie_alert/cookiealert.js"></script>
<script src="<?php echo frontend_assets(); ?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo frontend_assets(); ?>plugins/bootstrap_notify/bootstrap-notify.js"></script>
<script src="<?php echo frontend_assets(); ?>js/script.js?v=<?php echo rand(); ?>"></script>
<script src="<?php echo frontend_assets(); ?>functions.js?v=<?php echo rand(); ?>"></script>
<script>
    $(document).ready(function () {
        hideCustomLoader();
        <?php
        if ($this->session->flashdata('message') && $this->session->flashdata('message') != '')
        { ?>
        showMessage('<?php echo $this->session->flashdata('message'); ?>', 'warning');
        <?php }
        ?>
        <?php
        if ($this->session->userdata('message') && $this->session->userdata('message') != '')
        { ?>
        showMessage('<?php echo $this->session->userdata('message'); ?>', 'warning');
        <?php
        $this->session->unset_userdata('message');
        }
        ?>
    });
</script>
</body>
</html>