<!DOCTYPE html>
<html>
<head>
    <title><?php echo site_title(); ?></title>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo frontend_assets(); ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo frontend_assets(); ?>css/bootstrap-select.min.css" rel="stylesheet">
    <link rel="stylesheet"
          href="<?php echo frontend_assets(); ?>plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo frontend_assets(); ?>plugins/bootstrap_notify/animate.css">
    <link href="<?php echo frontend_assets(); ?>css/slick.css" rel="stylesheet">
    <link href="<?php echo frontend_assets(); ?>css/icomoon.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo frontend_assets(); ?>plugins/cookie_alert/cookiealert.css">
    <link href="<?php echo frontend_assets(); ?>css/style.min.css?v=<?php echo rand(); ?>" rel="stylesheet">
    <style>
        .error {
            color: red !important;
        }

        .error-border {
            border: 1px red solid !important;
        }

        .overlaybg {
            display: block;
            position: fixed;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, .3);
            z-index: 999999;
        }

        .overlaybg > img {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
    <script>
        var base_url = '<?php echo base_url(); ?>';
        var align_notify_message = '<?php echo($language == 'AR' ? 'left' : 'right'); ?>';
    </script>
</head>
<body class="top-bar-nav">
<div class="overlaybg" style="display: block !important;">
    <img src="<?php echo frontend_assets(); ?>loader.svg">
</div>
<div class="wrapper">
    <div class="wrap-sticky" style="height:92px;">
        <nav class="navbar navbar-default navbar-sticky navbar-scrollspy divinnav">
            <div class="container">
                <div class="attr-nav">
                    <ul>
                        <li class="desktop-select">
                            <div class="lang">
                                <a href="javascript:void(0);" onclick="changeLanguage('EN');"
                                   class="<?php echo($language == 'EN' ? 'selected' : ''); ?>"><span>  </span>English</a>
                                <a href="javascript:void(0);" onclick="changeLanguage('AR');"
                                   class="<?php echo($language == 'AR' ? 'selected' : ''); ?>"><span> &nbsp; | &nbsp; </span>العربية</a>
                            </div>
                        </li>
                        <li class="loginRegister">
                            <div class="inner-item">
                                <a href="<?php echo base_url('account/login'); ?>" class="desktop-login">LOGIN</a>
                                <span>&nbsp; | &nbsp;</span>
                                <a href="<?php echo base_url('account/register'); ?>">REGISTER</a>
                                <!-- end ngIf: !loginUserInfo -->
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>">
                        <img src="<?php echo frontend_assets(); ?>images/logo.png" width="181" height="60" class="logo"
                             alt="">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="<?php echo($view == 'index' ? 'active' : ''); ?>">
                            <a href="<?php echo base_url('home'); ?>">HOME</a>
                        </li>
                        <li class="<?php echo($view == 'cards' ? 'active' : ''); ?>">
                            <a href="<?php echo base_url('cards'); ?>">CARDS</a>
                        </li>
                        <li class="<?php echo($view == 'blogs' || $view == 'blog_detail' ? 'active' : ''); ?>">
                            <a href="<?php echo base_url('blogs'); ?>">GRINGISH</a>
                        </li>
                        <li class="<?php echo($view == 'faq' ? 'active' : ''); ?>">
                            <a href="<?php echo base_url('faq'); ?>">FAQ</a>
                        </li>
                        <li class="<?php echo($view == 'partner_with_us' ? 'active' : ''); ?>">
                            <a href="<?php echo base_url('advertise-with-us'); ?>">PARTNER WITH US</a>
                        </li>
                        <li class="device-login">
                            <a href="<?php echo base_url('account/login'); ?>">LOGIN</a>
                        </li>
                        <li class="device-select">
                            <div class="lang">
                                <a href="" class="selected">English</a>
                                <a href="">&nbsp; | &nbsp; العربية</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
