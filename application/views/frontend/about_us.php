<?php $about_us = getPageData(1, $language); ?>
<section id="about-us" class="about-us"
         style="background-image: url(<?php echo base_url($about_us->Image); ?>)">
    <div class="container">
        <div class="heading text-center">
            <h1 class="heading-text"><?php echo $about_us->Title; ?></h1>
        </div>

        <div class="content">
            <?php echo $about_us->Description; ?>
        </div>
    </div>
</section>
