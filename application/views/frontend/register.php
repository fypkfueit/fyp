<!DOCTYPE html>
<html>
<head>
    <title>Exclusave</title>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo frontend_assets(); ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo frontend_assets(); ?>css/bootstrap-select.min.css" rel="stylesheet">
    <link rel="stylesheet"
          href="<?php echo frontend_assets(); ?>plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo frontend_assets(); ?>plugins/bootstrap_notify/animate.css">
    <link href="<?php echo frontend_assets(); ?>css/style.min.css" rel="stylesheet">
    <link href="<?php echo frontend_assets(); ?>css/icomoon.css" rel="stylesheet">
    <script>
        var base_url = '<?php echo base_url(); ?>';
        var align_notify_message = '<?php echo($language == 'AR' ? 'left' : 'right'); ?>';
    </script>
</head>
<body class="bg-register">
<div class="overlaybg" style="display: block !important;">
    <img src="<?php echo frontend_assets(); ?>loader.svg">
</div>
<style>
    input[type=number]::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }

    input[type="date"]::-webkit-calendar-picker-indicator,
    input[type="date"]::-webkit-inner-spin-button {
        opacity: .0;
        height: 30px;
        width: 30px;
        margin-bottom: 20px;
        z-index: 10;
    }

    .fieldstyle {
        opacity: 1 !important;
    }

    .fieldstyleremove {

        opacity: .6 !important;
        color: #fff !important;
    }

    .fieldstylee {
        opacity: 1 !important;
    }

    .fieldstyleremovee {

        opacity: .6 !important;
        color: #fff !important;
    }

    .overlaybg {
        display: block;
        position: fixed;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .3);
        z-index: 999999;
    }

    .overlaybg > img {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    body {
        position: relative;
        background-image: url(<?php echo frontend_assets(); ?>images/login-bg_50.png);
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center center;
        background-size: cover;
    }

</style>
<div class="big-logo">
    <a href="<?php echo base_url(); ?>">
        <img src="<?php echo frontend_assets(); ?>images/logo.png" class="logo" alt="" style="height: 60px;">
    </a>
</div>
<div class="select-lang hide">
    <li class="register-select">
        <div class="lang">
            <a href="javascript:void(0)" class="selected"> English</a>
            <a href="javascript:void(0)">
                &nbsp; | &nbsp; العربية</a>
        </div>
    </li>
</div>
<!-- Register Start Here -->
<section id="register" class="register">
    <div id="smartwizard" class="sw-main sw-theme-arrows sw-theme-default">

        <ul class="nav nav-tabs step-anchor">
            <li class="active nav-tabss"><a href="javascript:void(0);" style="padding-left:0px!important ">CARD
                    TYPE<span class="arrow"></span></a></li>
            <li id="basic-infoo"><a href="javascript:void(0);">BASIC INFO<span class="arrow"></span></a></li>
            <li id="contact-infoo"><a href="javascript:void(0);">CONTACT INFO<span class="arrow"></span></a></li>
            <li id="professional-infoo"><a href="javascript:void(0);">PROFESSIONAL INFO<span class="arrow"></span></a>
            </li>
            <li id="payment-infoo"><a href="javascript:void(0);">PAYMENT INFO<!--<span class="arrow"></span>--></a></li>
        </ul>

        <div class="sw-container tab-content" style="min-height: 120px;">
            <div id="card-type" class="sw-main sw-theme-arrows step-content" style="display: block;">
                <div class="card-type">
                    <form id="cardTypeForm" name="cardTypeForm">
                        <div class="card-item">
                            <?php
                            $i = 0;
                            foreach ($cards as $card) { ?>
                                <div class="item <?php echo($i == 0 ? 'active' : ''); ?>" id="item"
                                     onclick="$('.amountField').val('<?php echo $card->Amount; ?>');">
                                    <input type="radio" name="CardID" value="<?php echo $card->CardID; ?>"
                                           class="input-hidden" id="Card<?php echo $card->CardID; ?>"
                                           data-error="#cardTypeError">
                                    <label for="Card<?php echo $card->CardID; ?>">
                                        <div class="corporate-card">
                                            <img src="<?php echo base_url($card->CardLogoImage); ?>">
                                            <h3 class="text-corporate "><?php echo $card->Title; ?></h3>
                                        </div>
                                    </label>
                                </div>
                                <?php
                                $i++;
                            }
                            ?>
                        </div>
                        <span id="cardTypeError"></span>
                    </form>
                </div>
                <!-- Content Footer Area -->
                <div class="btn-group navbar-btn sw-btn-group" role="group">
                    <div class="prev" id="pre">
                        <button class="btn btn-default sw-btn-prev  disabled" type="button">Previous</button>
                    </div>
                    <div class="next" id="next">
                        <button class="btn btn-default sw-btn-next " type="button" onClick="myFunction();">Next</button>
                    </div>
                </div>
            </div>

            <div id="basic-info" class="sw-main sw-theme-arrows step-content">
                <div class="basic-info">
                    <form id="basicInfoForm" name="basicinfoform">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="group">
                                    <input type="text" name="FirstName" id="FirstName">
                                    <span id="FirstNameError"></span>
                                    <label style="opacity: .6;
    color: #fff;">First Name <span style="color: red;"> *</span></label>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="group">
                                    <input type="text" name="MiddleName" id="MiddleName">
                                    <label style="opacity: .6;
    color: #fff;">Middle Name </label>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="group">
                                    <input type="text" name="LastName" id="LastName">
                                    <span id="LastNameError"></span>
                                    <label style="opacity: .6;
    color: #fff;">Last Name <span style="color: red;"> *</span></label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="group date-time">
                                    <label style="font-size: 17px;opacity: .6;
    color: #fff;">Date of Birth <span style="color: red;"> *</span></label>
                                    <input class="form-control datepicker" data-date-end-date="0d" type="text"
                                           name="DateOfBirth" id="DateOfBirth"
                                           style="margin-top: 2px
		    left: 40px;
    background-position: right;
    margin-top: 2px;

    background-repeat: no-repeat; color:grey">

                                    <i class="icon-calendar"></i>
                                    <span id="dateError"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="radio-title gender makeActiveJQ">
                                    <h3 class="">Gender<span style="color: red;"> *</span></h3>
                                    <!-- ngRepeat: gender in genderType --><span class="obj">
                      <input type="radio" name="Gender" value="Female" data-error="#genderError">
                      <label for="Female"><i class="icon-female"></i>
                        <span class=" text-female custom-color">Female</span></label>
                                    </span>
                                    <!-- end ngRepeat: gender in genderType --><span class="obj">
                      <input type="radio" name="Gender" id="Male" value="Male" data-error="#genderError">
                      <label for="Male"><i class="icon-male"></i>
                        <span class=" text-male custom-color">Male</span></label>
                                    </span>
                                    <!-- end ngRepeat: gender in genderType --> <br>
                                    <span id="genderError"></span>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-5">
                                <div class="radio-title martialstatus makeActiveJQ">
                                    <h3 class="">Marital Status<span style="color: red;"> *</span></h3>
                                    <span class="obj">
									  <input type="radio" name="MaritalStatus" value="Married"
                                             data-error="#martialError">
									  <label for="Married"><i class="icon-married"></i>
										<span class="text-married custom-color">Married</span></label>
                                    </span>
                                    <!-- end ngRepeat: martial in MartialType -->
                                    <span class="obj">
                      <input type="radio" name="MaritalStatus" value="Single" data-error="#martialError">
                      <label for="Single"><i class="icon-unmarried"></i>
                        <span class="text-single custom-color">Single</span></label>
                                    </span>
                                    <!-- end ngRepeat: martial in MartialType --> <br>
                                    <span id="martialError"></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Content Footer Area -->
                <div class="btn-group navbar-btn sw-btn-group" role="group">
                    <div class="prev d-l">
                        <button class="btn btn-default sw-btn-prev  disabled" type="button" onclick="bkFunction()"
                                style="cursor: pointer;">Previous
                        </button>
                    </div>
                    <div class="next d-l">
                        <button class="btn btn-default sw-btn-next  dl" type="button" id="next1"
                                onClick="myFunction1()">Next
                        </button>
                    </div>
                </div>
            </div>

            <div id="contact-info" class="sw-main sw-theme-arrows step-content">
                <div class="contact-info">
                    <form id="contactInfoForm" name="contactInfoForm" class="" novalidate>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="group">

                                    <div class="intl-tel-input allow-dropdown">
                                        <div class="flag-container">
                                            <div class="selected-flag" tabindex="0"
                                                 title="Saudi Arabia (&#8235;المملكة العربية السعودية&#8236;&lrm;): +966">

                                                <div class="iti-flag sa"></div>
                                                <div class="iti-arrow"></div>
                                            </div>
                                            <input type="number"
                                                   style="padding-left: 4px !important;    width: 92% !important;"
                                                   id="mobileNo" name="Mobile" placeholder="051 234 5678">
                                        </div>
                                        <label class="mobileLabel " style="color:#fff; opacity: .6">Mobile No<span
                                                    style="color: red;"> *</span></label>
                                        <div id="mobileNo-error" class="error"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="group">
                                    <input type="text" name="Email" placeholder="john@example.com" id="emailid"
                                           required>
                                    <label style="color:#fff; opacity: .6">Email Address<span
                                                style="color: red;"> *</span></label>
                                    <div id="emailid-error" class="error"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Content Footer Area -->
                <div class="btn-group navbar-btn sw-btn-group" role="group">
                    <div class="prev d-l">
                        <button class="btn btn-default sw-btn-prev  disabled" type="button" onClick="bkFunction1()"
                                style="cursor: pointer;">Previous
                        </button>
                    </div>
                    <div class="next d-l">
                        <button class="btn btn-default sw-btn-next " type="button" onClick="myFunction2()">Next</button>
                    </div>
                </div>
            </div>

            <div id="professional-info" class="sw-main sw-theme-arrows step-content">
                <div class="professional-info">
                    <form id="professionalInfoForm" name="professionalform">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="group">
                                    <input type="text" name="CompanyName">
                                    <label>University / Company Name<span style="color: red;"> *</span></label>
                                    <span id="universityError"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="group">
                                    <input type="text" name="Position">
                                    <label>Grade / Position<span style="color: red;"> *</span></label>

                                    <span id="gradeError"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="vwt-FormGroup">
                                    <label style="display: inline; top:0;" class="vwt-Input custom-color2">City<span
                                                style="color: red;"> *</span></label>

                                    <select name="CityID" style="color: #fff;
    position: static;
    display: block;
    width: 100%;
    padding-top: 42px !important;
    color:grey;
    background: transparent;
    border-width: 0 0 1px;
    border-style: none none solid;
    border-color: transparent transparent rgba(255, 255, 255, 0.5);" id="city1">
                                        <?php
                                        foreach ($cities as $city) { ?>
                                            <option value="<?php echo $city->CityID; ?>"><?php echo $city->Title; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                    <span id="cityError"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="upload-file">
                                    <label style="font-size:17px;color: #fff;opacity: 0.6;" class="custom-color2">Attach
                                        University / Company ID<span style="color: red;"> *</span></label>
                                    <div class="border"></div>
                                    <div class="attach-icon">
                                        <div class="input-file-container">
                                            <input class="input-file"
                                                   accept=".jpg, .jpeg, .png, .JPG, .JPEG, .PNG, .pdf"
                                                   ngf-select="uploadFile($file)" name="IDImage" id="uploadFile"
                                                   type="file" title=" " required data-error="#fileuploadError">

                                            <label tabindex="0" for="uploadFile" class="input-file-trigger"
                                                   style="background-image: url('<?php echo frontend_assets(); ?>images/Capture2.png'); bottom:17px"></label>
                                        </div>
                                    </div>
                                    <span class="filePlaceholder" style="font-style: normal;vertical-align: sub;   ">JPG,JPEG,PNG,PDF
                   </span>
                                    <p class="file-return" style=" border-bottom:1.5px solid grey; margin-bottom:0px">
                                        <span id="file-attachment" style="color:#fff;opacity:0.6;"></span></p>
                                    <span id="fileuploadError"></span>
                                    <div class="cg-busy cg-busy-animation ">
                                        <div class="cg-busy-default-wrapper"
                                             style="position: absolute; top: 0px; left: 0px; right: 0px; bottom: 0px;">

                                            <div class="cg-busy-default-sign">

                                                <div class="cg-busy-default-spinner">
                                                    <div class="bar1"></div>
                                                    <div class="bar2"></div>
                                                    <div class="bar3"></div>
                                                    <div class="bar4"></div>
                                                    <div class="bar5"></div>
                                                    <div class="bar6"></div>
                                                    <div class="bar7"></div>
                                                    <div class="bar8"></div>
                                                    <div class="bar9"></div>
                                                    <div class="bar10"></div>
                                                    <div class="bar11"></div>
                                                    <div class="bar12"></div>
                                                </div>

                                                <div class="cg-busy-default-text"></div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Content Footer Area -->
                <div class="btn-group navbar-btn sw-btn-group" role="group">
                    <div class="prev d-l">
                        <button class="btn btn-default sw-btn-prev  disabled" type="button" onClick="bkFunction2()"
                                style="cursor: pointer;">Previous
                        </button>
                    </div>
                    <div class="next d-l">
                        <button class="btn btn-default sw-btn-next " type="button" onClick="myFunction3()">Next</button>
                    </div>
                </div>
            </div>

            <div id="payment-info" class="sw-main sw-theme-arrows step-content">
                <div class="payment-info">
                    <form id="paymentInfoForm" name="paymentInfoForm" novalidate="novalidate">
                        <div class="row">
                            <!--<div class="col-md-2">
                                <div class="vwt-FormGroup readonly custom-color2">
                                    <input class="vwt-Input " type="text" style="pointer-events: none;" name="cardType"
                                           id="cardType" disabled="disabled">
                                    <label class="vwt-Input ">Card Type</label>
                                </div>
                            </div>-->
                            <!--<div class="col-md-2">
                                <div class="vwt-FormGroup custom-color2">
                                    <input class="vwt-Input hide" type="text" name="validities" id="validities"
                                           data-error="#selectValidity" readonly data-toggle="dropdown">
                                    <label for="" class="vwt-Input ">Validity<span style="color: red;"> *</span></label>
                                    <select style="color: #fff;
    position: static;
    display: block;
    width: 100%;
    color:grey;
    padding-top: 10px !important;
    background: transparent;
    border-width: 0 0 1px;
    border-style: none none solid;
    border-color: transparent transparent rgba(255, 255, 255, 0.5);" name=validity>

                                        <option selected>
                                        </option>
                                        <option>
                                            1 - year
                                        </option>
                                        <option>
                                            2 - year
                                        </option>
                                        <option>
                                            3 - year
                                        </option>
                                        <option>
                                            4 - year
                                        </option>

                                    </select>

                                    <span id="selectValidity"></span>
                                </div>
                            </div>-->
                            <!--<div class="col-md-2">
                                <div class="vwt-FormGroup readonly custom-color2">
                                    <input class="vwt-Input date " style="pointer-events: none;" type="text"
                                           disabled="disabled">
                                    <label class="vwt-Input ">Expiry Date</label>
                                </div>
                            </div>-->
                            <div class="col-md-3">
                                <div class="group">
                                    <input type="text" id="promoCode" style="border:solid 2px white; border-radius: 50px;
    width: 90%;
    height: 32px;
    margin-top: 5px;
    padding: 6px 10px;
    border: 2px solid #fff;
    border-radius: 25px;">
                                    <input type="hidden" name="CouponID" id="CouponID" value="0">
                                    <button type="button" class="btn-apply" name="button" onclick="applyCoupon();"
                                            style=" font-size: 16px;
    position: absolute;
    top: 0;
    right: 21px;
    height: 32px;
    padding: 5px 20px;
    color: #fff;
    color: #333;
    border: none;
    border-top-right-radius: 25px;
    border-bottom-right-radius: 25px;
    outline: none;
    background-color: #fff;
    float: left;
    display: inline;">Apply
                                    </button>
                                    <!-- ngIf: applyPromo==true -->
                                    <label style="top: -28px !important;">Promo Code</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="vwt-FormGroup readonly custom-color2">
                                    <!-- ngIf: applyPromo==true -->

                                    <input class="vwt-Input amountField" name="Amount" type="text" readonly style="">
                                    <!-- end ngIf: applyPromo!=true -->
                                    <!-- ngIf: applyPromo==true -->
                                    <label class="vwt-Input ">Amount</label>
                                </div>
                            </div>
                        </div>

                        <!--<div class="row">
                            <div class="col-md-5">
                                <div class="group" style="margin-bottom: 20px;">
                                    <input type="text" name="holderName" placeholder="(First,Middle,Last)" class="">
                                    <span class="bar"></span>
                                    <label class="">Bank Account Holder's Name<span
                                                style="color: red;"> *</span></label>
                                </div>
                            </div>
                        </div>-->

                        <!--<div class="payment-gateway">
                            <span class="title ">Make Payment<span style="color: red;"> *</span></span>
                            <div class="banks">
                                <div class="items" id="items">
                                    <label for="">
                                        <div class="item  payment-log">
                                            <div class="circle">
                                                <a href="#bankDetailModal" data-toggle="modal" id="NCB">
                                                    <img
                                                            style="line-height: 55px;width: 55px;height: 55px;margin-bottom: 5px;text-align: center;border-radius: 100%;"
                                                            alt=""
                                                            src="http://app.exclusavecard.com:8082/assets/Bank/NCB_1508242342000.png">
                                                </a>
                                            </div>
                                            <input type="radio" style="cursor: pointer;" name="bankTitle"
                                                   data-error="#selectBank">&nbsp;&nbsp;<span
                                                    class=" custom-color">NCB</span>
                                        </div>
                                        <div class="item  payment-log">
                                            <div class="circle">
                                                <a href="#bankDetailModal" data-toggle="modal" id="SAMBA">
                                                    <img style="line-height: 55px;width: 55px;height: 55px;margin-bottom: 5px;text-align: center;border-radius: 100%;"
                                                         alt=""
                                                         src="http://app.exclusavecard.com:8082/assets/Bank/Samba_1508242402000.png">
                                                </a>
                                            </div>
                                            <input type="radio" style="cursor: pointer;" name="bankTitle"
                                                   data-error="#selectBank">&nbsp;&nbsp;<span class=" custom-color">SAMBA</span>
                                        </div>
                                    </label>
                                </div>
                                <span id="selectBank"></span>
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="vwt-CheckboxWrapper">
                                    <input class="vwt-Checkbox" type="checkbox" name="termss" id="terms"
                                           data-error="#termsError" value="yes">
                                    <label class="vwt-CheckboxLabel" for="terms" name="terms">
                                        <span class="circle"></span>
                                        <span class="check"></span>
                                        <span class="box"></span>
                                        <label id="termsText" style="opacity:.6;color:#fff;">I have read and agreed to
                                            the
                                            <a style="color: #fff;text-decoration: underline;"
                                               href="#termsConditionsModal" data-toggle="modal" class="">Terms &amp;
                                                Conditions </a>and
                                            <a style="color: #fff;text-decoration: underline;" href="#privacyModal"
                                               data-toggle="modal" class="">the Privacy Policy</a>
                                        </label>
                                        <span id="termsError"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Content Footer Area -->
                <div class="btn-group navbar-btn sw-btn-group" role="group">
                    <div class="prev d-l">
                        <button class="btn btn-default sw-btn-prev disabled" type="button" onClick="bkFunction3()"
                                style="cursor: pointer;">Previous
                        </button>
                    </div>
                    <div class="next d-l">
                        <button class="btn btn-default sw-btn-next" type="button" onclick="myFunction4()">Submit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo frontend_assets(); ?>js/jquery.min.js"></script>
<script src="<?php echo frontend_assets(); ?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- END T & C Modal -->
<script>
    document.getElementById("basic-info").style.display = "none";
    document.getElementById("contact-info").style.display = "none";
    document.getElementById("professional-info").style.display = "none";
    document.getElementById("payment-info").style.display = "none";

    function myFunction() {
        var hh = document.forms["cardTypeForm"]["CardID"].value;
        if (hh == "" || hh == null) {
            document.getElementById("cardTypeError").innerHTML = "<span style='color: red;'>Card is required</span>";
            return false;
        } else {
            var y = document.getElementById("basic-info");
            var x = document.getElementById("card-type");
            x.style.display = "none";
            y.style.display = "block";
            $('#basic-infoo').siblings('li').removeClass('active');
            $('#basic-infoo').addClass('active');
        }
    }

    function bkFunction() {
        var bb = document.getElementById("basic-info");
        var xx = document.getElementById("card-type");
        bb.style.display = "none";
        xx.style.display = "block";
    }

    function myFunction1() {
        document.getElementById("FirstNameError").innerHTML = " ";
        document.getElementById("LastNameError").innerHTML = " ";
        document.getElementById("dateError").innerHTML = " ";
        document.getElementById("genderError").innerHTML = " ";
        document.getElementById("martialError").innerHTML = " ";
        var hh = document.forms["basicinfoform"]["FirstName"].value;
        var hh1 = document.forms["basicinfoform"]["LastName"].value;
        var dob = document.forms["basicinfoform"]["DateOfBirth"].value;
        var gen = document.forms["basicinfoform"]["Gender"].value;
        var mstatus = document.forms["basicinfoform"]["MaritalStatus"].value;

        if (hh == "" || hh == null) {
            document.getElementById("FirstNameError").innerHTML = "<span style='color: red;'>First name is required</span>";
            return false;

        } else if (hh1 == "" || hh1 == null) {
            document.getElementById("LastNameError").innerHTML = "<span style='color: red;'>Last name is required</span>";
            return false;

        } else if (dob == "" || dob == null) {
            document.getElementById("dateError").innerHTML = "<span style='color: red;'>Date of birth is required</span>";
            return false;

        } else if (gen == "" || gen == null) {
            document.getElementById("genderError").innerHTML = "<span style='color: red;'>Gender is required</span>";
            return false;
        } else if (mstatus == "" || mstatus == null) {
            document.getElementById("martialError").innerHTML = "<span style='color: red;'>Marital status  is required</span>";
            return false;
        } else {
            var e = document.getElementById("contact-info");
            var c = document.getElementById("basic-info");
            c.style.display = "none";
            e.style.display = "block";
            document.getElementById('next1').className = "active";
            var button_class = document.getElementById('next').className;
            $('#contact-infoo').siblings('li').removeClass('active');
            $('#contact-infoo').addClass('active');
        }
    }

    function bkFunction1() {
        var ee = document.getElementById("contact-info");
        var cc = document.getElementById("basic-info");
        ee.style.display = "none";
        cc.style.display = "block";
    }

    function myFunction2() {
        document.getElementById("emailid-error").innerHTML = " ";
        document.getElementById("mobileNo-error").innerHTML = " ";
        var a = document.forms["contactInfoForm"]["Mobile"].value;
        var b = document.forms["contactInfoForm"]["Email"].value;
        var atposition = b.indexOf("@");
        var dotposition = b.lastIndexOf(".");

        if (a == null || a == "") {
            document.getElementById("mobileNo-error").innerHTML = "<span style='color: red;'>Mobile no. is required</span>";
            return false;

        } else if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= b.length) {
            document.getElementById("emailid-error").innerHTML = "<span style='color: red;'>Proper Email is required</span>";
            return false;
        } else if (b == null || b == "") {
            document.getElementById("emailid-error").innerHTML = "<span style='color: red;'>Email is required</span>";
            return false;

        } else {
            var d = document.getElementById("contact-info");
            var h = document.getElementById("professional-info");
            d.style.display = "none";
            h.style.display = "block";

            $('#professional-infoo').siblings('li').removeClass('active');
            $('#professional-infoo').addClass('active');
        }
    }

    function bkFunction2() {
        var dd = document.getElementById("contact-info");
        var hh = document.getElementById("professional-info");
        hh.style.display = "none";
        dd.style.display = "block";

    }

    function myFunction3() {

        document.getElementById("universityError").innerHTML = " ";
        document.getElementById("gradeError").innerHTML = " ";
        document.getElementById("cityError").innerHTML = " ";
        document.getElementById("fileuploadError").innerHTML = " ";
        var uni = document.forms ["professionalInfoForm"]["CompanyName"].value;
        var gradee = document.forms["professionalInfoForm"]["Position"].value;
        var city = document.forms ["professionalInfoForm"]["CityID"].value;

        //var file = document.forms["professionalInfoForm"] ["file"]. value;

        if (uni == "" || uni == null) {
            document.getElementById("universityError").innerHTML = "<span style='color: red;'>This field is required</span>";
            return false;
        } else if (gradee == null || gradee == "") {
            document.getElementById("gradeError").innerHTML = "<span style='color: red;'>This field is required</span>";
            return false;
        }

        /*	else if (document.form.city.selectedIndex==""	 )
    {

                document.getElementById("cityError").innerHTML= "<span style='color: red;'>This field is required</span>";
            //	document.getElementById("fileuploadError").innerHTML= "<span style='color: red;'>This field is required</span>";
                return false;

        }*/

        else {
            var t = document.getElementById("payment-info");
            var u = document.getElementById("professional-info");
            u.style.display = "none";
            t.style.display = "block";

            $('#payment-infoo').siblings('li').removeClass('active');
            $('#payment-infoo').addClass('active');

        }
    }

    function bkFunction3() {

        var tt = document.getElementById("payment-info");
        var uu = document.getElementById("professional-info");
        tt.style.display = "none";
        uu.style.display = "block";

    }

    function myFunction4() {

        /*
            document.getElementsByClassName("bar").innerHTML= " ";
            document.getElementById("selectBank").innerHTML= " ";
            document.getElementById("termsError").innerHTML= " ";*/
        // var hl = document.forms["paymentInfoForm"] ["bankTitle"].value;
        document.getElementById("termsError").innerHTML = " ";
        var hlw = document.forms["paymentInfoForm"]["termss"].value;
        /*if (hl == "") {
            document.getElementById("selectBank").innerHTML = "<span style='color: red;'>This field is required</span>";
            return false;
        } else */
        if ($("#terms").is(':checked')) {
            // Construct data string
            var dataString = $("#cardTypeForm, #basicInfoForm, #contactInfoForm, #professionalInfoForm, #paymentInfoForm").serialize();
            // Log in console so you can see the final serialized data sent to AJAX
            console.log(dataString);
            // Do AJAX
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('account/signUp'); ?>',
                data: dataString,
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                    showMessage(result.message);
                    if (result.status == true) {
                        setTimeout(function () {
                            window.location.href = result.redirect_url;
                        }, 2000);
                    }
                }
            });
        } else {
            document.getElementById("termsError").innerHTML = "<span style='color: red;'>Please accept terms and conditions to proceed with signup.</span>";
            return false;
        }
    }

    function showMessage(message_to_show, type = 'info') {
        $.notify({
            // options
            message: message_to_show
        }, {
            // settings
            type: type,
            placement: {
                from: "top",
                align: align_notify_message
            },
            delay: 7000,
            mouse_over: "pause",
            offset: 20,
            spacing: 10,
            z_index: 1031,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
    }

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    function applyCoupon() {
        var CardID = $('input[name=CardID]:checked').val();
        var CouponCode = $('#promoCode').val();
        var Amount = $('.amountField').val();
        if (CouponCode !== '') {
            showCustomLoader();
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('account/applyCoupon'); ?>',
                data: {'CardID': CardID, 'CouponCode': CouponCode, 'Amount': Amount},
                dataType: 'json',
                success: function (result) {
                    hideCustomLoader();
                    if (result.status == true) {
                        showMessage(result.message, 'success');
                        $('.amountField').val(result.DiscountedAmount);
                        $('#CouponID').val(result.CouponID);
                    } else {
                        showMessage(result.message, 'danger');
                    }
                }
            });
        } else {
            showMessage('Please enter promo code!', 'danger')
        }
    }

    function showCustomLoader() {
        $('.overlaybg').fadeIn();
    }

    function hideCustomLoader() {
        $('.overlaybg').fadeOut();
    }
</script>


<script src="<?php echo frontend_assets(); ?>plugins/bootstrap_notify/bootstrap-notify.js"></script>
<script>
    $('.makeActiveJQ .obj').click(function () {
        $(this).siblings('.obj').removeClass('active');
        $(this).addClass('active');
    });
</script>
<style>
    .obj.active label i,
    .obj.active span.custom-color {
        opacity: 1 !important;
    }

    .nav li a:hover, .nav li a:focus {
        text-decoration: none;
        background-color: #0a040400 !important;
    }

    .nav-tabs li a:hover {
        border-color: #0a040400 !important;
    }

    .nav-tabs li a:hover {
        color: #bbb !important;
    }
</style>
<script>
    $(document).ready(function () {
        hideCustomLoader();
        <?php
        if ($this->session->flashdata('message') && $this->session->flashdata('message') != '')
        { ?>
        showMessage('<?php echo $this->session->flashdata('message'); ?>', 'warning');
        <?php }
        ?>
        <?php
        if ($this->session->userdata('message') && $this->session->userdata('message') != '')
        { ?>
        showMessage('<?php echo $this->session->userdata('message'); ?>', 'warning');
        <?php
        $this->session->unset_userdata('message');
        }
        ?>
    });
</script>
</body>