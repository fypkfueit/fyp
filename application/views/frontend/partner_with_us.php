<section id="advertise-with-us" class="advertise-with-us form-loader custom-min-height" style="background-color:black">
    <div class="container">
        <div class="content">
            <p>Exclusave is an innovative &amp; effective marketing tool that will help your business by increasing
                sales frequency, customers loyalty, and brand awareness.It's the best channel to reach your potential
                customers and build a connection. Our partners are increasing every day and we have lots of creative
                ideas to market for your business. Join us Now.</p>

            <p>Kindly, fill the form below with your business information and we will get in touch with you in maximum 3
                days.</p>
        </div>

        <form name="advertiseForm" id="advertiseForm" novalidate="novalidate">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="icon-company icon"></span>
                        <label>Company Name<span style="color: red;"> *</span></label>
                        <input type="text" name="companyName" id="companyName" class="form-control ">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="icon-industry icon"></span>
                        <label>Industry<span style="color: red;"> *</span></label>
                        <div class="input-group-btn bs-dropdown-to-select-group">
                            <select class="form-control" style="color:grey">
                                <option>Engineering</option>
                                <option>Developer</option>
                                <option>Education</option>
                            </select>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="icon-head-office icon"></span>
                        <label>Head Office Address<span style="color: red;"> *</span></label>
                        <input placeholder="Enter Location" type="text" name="address" id="address"
                               class="form-control " autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="icon-contact-name icon"></span>
                        <label>Contact Person Name<span style="color: red;"> *</span></label>
                        <input type="text" name="contactName" placeholder="(First,Middle,Last)" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="form-group ">
                        <span class="icon-mobile icon"></span>
                        <label>Mobile No<span style="color: red;"> *</span></label>
                        <input type="text" name="contactNumber" class="form-control ">
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="form-group ">
                        <span class="icon-mail-verification icon"></span>
                        <label>Email Address<span style="color: red;"> *</span></label>
                        <input type="email" name="email" placeholder="john@gmail.com" class="form-control ">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="icon-website icon"></span>
                        <label>Website/Facebook Page<span style="color: red;"> *</span></label>
                        <input type="text" name="url" class="form-control ">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="icon-other-information icon"></span>
                        <label>Other Social Media</label>
                        <input type="text" name="info" class="form-control ">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn submit ">Submit</button>
                </div>
            </div>
        </form>
    </div>


    <div class="cg-busy cg-busy-backdrop cg-busy-backdrop-animation "></div>
    <div class="cg-busy cg-busy-animation ">
        <div class="cg-busy-default-wrapper" style="top: 0px; left: 0px; right: 0px; bottom: 0px;">

            <div class="cg-busy-default-sign">

                <div class="cg-busy-default-spinner">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                    <div class="bar4"></div>
                    <div class="bar5"></div>
                    <div class="bar6"></div>
                    <div class="bar7"></div>
                    <div class="bar8"></div>
                    <div class="bar9"></div>
                    <div class="bar10"></div>
                    <div class="bar11"></div>
                    <div class="bar12"></div>
                </div>

                <div class="cg-busy-default-text "></div>

            </div>

        </div>
    </div>
</section>