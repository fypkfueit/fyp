(function($) {
  "use strict";

  //Masonry
  jQuery(window).on('load', function() {
    // jQuery('.blogs-list').masonry({
    //   itemSelector: '.blogs-item',
    // });
  });

  jQuery.fn.is_exists = function() {
    return this.length > 0;
  }

  $(window).on('load', function() {

    // Preloader
    $("#preloader-body").delay(1500).fadeOut(2000);
    $(".preloader-item").fadeOut(2000);

  }); // end on.load event

  // slimScroll
  $('#faqScroller').slimScroll({
    height: 'auto',
    position: 'right',
    size: "5px",
    color: '#333',
    alwaysVisible: false,
    distance: '0',
    railVisible: false,
    railColor: '#222',
    railOpacity: 0.3,
    wheelStep: 10,
    allowPageScroll: false,
    disableFadeOut: false
  });

  $('#sidebarScroller').slimScroll({
    height: 'auto',
    position: 'right',
    size: "5px",
    color: '#333',
    alwaysVisible: false,
    distance: '0',
    railVisible: false,
    railColor: '#222',
    railOpacity: 0.3,
    wheelStep: 10,
    allowPageScroll: false,
    disableFadeOut: false
  });

  // Animate panel function
  $.fn['animatePanel'] = function() {
    var element = $(this);
    var effect = $(this).data('effect');
    var delay = $(this).data('delay');
    var child = $(this).data('child');
    // Set default values for attrs
    if (!effect) {
      effect = 'zoomIn'
    }
    if (!delay) {
      delay = 0.3
    } else {
      delay = delay / 10
    }
    if (!child) {
      child = '.animated > div'
    } else {
      child = "." + child
    }
    //Set defaul values for start animation and delay
    var startAnimation = 0;
    var start = Math.abs(delay) + startAnimation;
    // Get all visible element and set opacity to 0
    var panel = element.find(child);
    panel.addClass('opacity-0');
    // Get all elements and add effect class
    panel = element.find(child);
    panel.addClass('stagger').addClass('animated-panel').addClass(effect);
    var panelsCount = panel.length + 10;
    var animateTime = (panelsCount * delay * 10000) / 10;
    // Add delay for each child elements
    panel.each(function(i, elm) {
      start += delay;
      var rounded = Math.round(start * 10) / 10;
      $(elm).css('animation-delay', rounded + 's');
      // Remove opacity 0 after finish
      $(elm).removeClass('opacity-0');
    });
    // Clear animation after finish
    setTimeout(function() {
      $('.stagger').css('animation', '');
      $('.stagger').removeClass(effect).removeClass('animated-panel').removeClass('stagger');
    }, animateTime)
  };

  // Selectpicker
  $(function() {
    $('.selectpicker').selectpicker();
  });

  // Floating chat
  var element = $('.floating-chat');
  var myStorage = localStorage;

  // if (!myStorage.getItem('chatID')) {
  //   myStorage.setItem('chatID', createUUID());
  // }

  setTimeout(function() {
    element.addClass('enter');
  }, 4000);

  element.click(openElement);

  function openElement() {
    var messages = element.find('.messages');
    var textInput = element.find('.text-box');
    element.find('>i').hide();
    element.addClass('expand');
    element.find('.chat').addClass('enter');
    //var strLength = textInput.val().length * 2;
    //textInput.keydown(onMetaAndEnter).prop("disabled", false).focus();
    element.off('click', openElement);
    element.find('.header .close').click(closeElement);
    //element.find('#sendMessage').click(sendNewMessage);
    messages.scrollTop(messages.prop("scrollHeight"));
  }

  function closeElement() {
    element.find('.chat').removeClass('enter').hide();
    element.find('>i').show();
    element.removeClass('expand');
    element.find('.header .close').off('click', closeElement);
    //element.find('#sendMessage').off('click', sendNewMessage);
    //element.find('.text-box').off('keydown', onMetaAndEnter).prop("disabled", true).blur();
    setTimeout(function() {
      element.find('.chat').removeClass('enter').show()
      element.click(openElement);
    }, 500);
  }

  // function sendNewMessage() {
  //   var userInput = $('.text-box');
  //   var newMessage = userInput.html().replace(/\<div\>|\<br.*?\>/ig, '\n').replace(/\<\/div\>/g, '').trim().replace(/\n/g, '<br>');
  //
  //   if (!newMessage) return;
  //
  //   var messagesContainer = $('.messages');
  //
  //   messagesContainer.append([
  //     '<li class="self"><div class="chat-start"><div class="list-action-left"><img src="assets/images/faces/self.jpg" alt=""></div><div class="list-content"><span class="caption">',
  //     newMessage,
  //     '</span></div></div></li>'
  //   ].join(''));
  //
  //   // clean out old message
  //   userInput.html('');
  //   // focus on input
  //   userInput.focus();
  //
  //   messagesContainer.finish().animate({
  //     scrollTop: messagesContainer.prop("scrollHeight")
  //   }, 250);
  // }

  // function onMetaAndEnter(event) {
  //   if ((event.metaKey || event.ctrlKey) && event.keyCode == 13) {
  //     sendNewMessage();
  //   }
  // }

  // Init vwt-List selection
  $(document).on("click", ".vwt-List li", function(e) {
    var items, raw, mid, pro;
    e.preventDefault();
    items = $(this).parent("ul").children("li");
    raw = $(this).html();
    mid = $("<div>").html(raw);
    mid.find("span").remove();
    pro = $.trim(mid.text());
    if (!$(this).hasClass("vwt-List-selected")) {
      items.removeClass("vwt-List-selected");
      $(this).addClass("vwt-List-selected");
      $(this).parents(".vwt-FormGroup").find("input.vwt-Input").val(pro).blur()
    } else $(this).parents(".vwt-FormGroup").find("input.vwt-Input").val(pro).blur()
  });

  // Init & Activate vwt-Input Blur Event
  $(document).on("blur", ".vwt-Input", function() {
    if ($.trim($(this).val())) {
      if (!$(this).siblings("label.vwt-Input").hasClass("vwt-InputHasValue")) $(this).siblings("label").addClass("vwt-InputHasValue")
    } else $(this).siblings("label").removeClass("vwt-InputHasValue")
  });
  $('.vwt-Input').blur();


  // Animation
  var wow = new WOW({
    boxClass: 'wow', // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 0, // distance to the element when triggering the animation (default is 0)
    mobile: false, // trigger animations on mobile devices (default is true)
    live: true // act on asynchronously loaded content (default is true)
  });
  wow.init();

})(jQuery);

$('#carouselStudent, #carouselcop').carousel({
  interval: 999900
});