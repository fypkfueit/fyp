    
$( document ).ready(function() {
   $('#DepartmentID').on('change',function(){
           
            $('#ClassID').html('<option value="">Select Class</option>');
            var DepartmentID = $('#DepartmentID').val();
            //alert(DepartmentID);
            $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/department/getPrograms',
                    data: {
                        'DepartmentID': DepartmentID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#ProgramID').html(result.html);
                       $("#ProgramID").selectpicker('refresh');

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
           
       });


   $('#ProgramID').on('change',function(){
           
            
            var ProgramID = $("#ProgramID option:selected").val();
            //alert(ProgramID);
            $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/program/getClasses',
                    data: {
                        'ProgramID': ProgramID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#ClassID').html(result.html);
                       $("#ClassID").selectpicker('refresh');

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
           
       });


   $('#HasPartner').on('click',function(){

        if($(this).prop("checked") == true){
                $('.Partner').show();
        }else if($(this).prop("checked") == false){
                $('.Partner').hide();
        }

   });


    
});