$(document).ready(function () {
    tinymce.init({
        forced_root_block: "",
        selector: 'textarea.textarea',
        plugins: "code,textcolor,directionality,table,link",
        theme: "modern",
        mode: "exact",
        toolbar: "fontsizeselect,forecolor,backcolor, undo,redo,cleanup,bold,italic,underline,strikethrough,separator,"
            + "formatselect,"
            + "alignleft aligncenter alignright alignjustify | bullist,numlist,outdent,indent,table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol|ltr rtl",
        theme_advanced_toolbar_location: "top",
        theme_advanced_buttons1: "bold,italic,underline,strikethrough,separator,"
            + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
            + "bullist,numlist,outdent,indent",
        theme_advanced_buttons2: "link,unlink,anchor,image,separator,"
            + "undo,redo,cleanup,code,separator,sub,sup,charmap",
        theme_advanced_buttons3: "",
        height: "250px",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });

    tinymce.init({
        forced_root_block: "",
        selector: '#textarea_ar',
        plugins: "code,textcolor,directionality,table,link",
        theme: "modern",
        mode: "exact",
        directionality: "rtl",
        toolbar: "fontsizeselect,forecolor,backcolor, undo,redo,cleanup,bold,italic,underline,strikethrough,separator,"
            + "formatselect,"
            + "alignleft aligncenter alignright alignjustify | bullist,numlist,outdent,indent,table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol|ltr rtl",
        theme_advanced_toolbar_location: "top",
        theme_advanced_buttons1: "bold,italic,underline,strikethrough,separator,"
            + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
            + "bullist,numlist,outdent,indent",
        theme_advanced_buttons2: "link,unlink,anchor,image,separator,"
            + "undo,redo,cleanup,code,separator,sub,sup,charmap",
        theme_advanced_buttons3: "",
        height: "250px",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });
});
/*
$(document).ready(function () {
    tinyMCE.init({
        //mode : "textareas",
        mode : "specific_textareas",
        editor_selector : "textarea",
        theme : "simple"
    });
});*/
